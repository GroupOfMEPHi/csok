﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using CSOK.Models;
using System.Data.Entity;
using System.Data;

namespace CSOK.Controllers
{
    public class Entrie
    {
        public int Num_of_news { get; set; }
        public DateTime DateTimeOfCreation { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Name { get; set; }

    }
    [Authorize]
    public class NewsController : Controller
    {
        private MyDbContext db = new MyDbContext();

        public ActionResult Index()
        {
            ViewBag.message_success = TempData["message_success"];
            var Names =
                (from nameA in db.Admin select new { ID = nameA.UserID, Name = nameA.Name }).Union(from nameT in db.Teachers select new { ID = nameT.UserId, Name = nameT.Name });

            var mostRecentEntries = (from entry in db.News
                                     join usr in Names on entry.Author.UserId equals usr.ID
                                     select new Entrie() { Num_of_news = entry.Num_of_news, DateTimeOfCreation = entry.DateTimeOfCreation, Title = entry.Title, Content = entry.Content, Name = usr.Name }).Distinct().OrderByDescending(x => x.DateTimeOfCreation).ToList();
            ViewBag.Entries = mostRecentEntries;
            try
            {
                if (User.IsInRole("Admin") || User.IsInRole("Teacher"))
                {
                    ViewBag.Role = "~/Views/News/Menu_Writer.cshtml";
                }
                return View();
            }
            catch
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Teacher")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        public ActionResult Create(News entry)
        {
            News neww = new News();
            neww.Title = entry.Title;
            neww.Content = entry.Content;
            neww.DateTimeOfCreation = DateTime.Now;
            int id = WebSecurity.GetUserId(User.Identity.Name);
            User usr = (from Userr in db.User
                        where Userr.UserId == id
                        select Userr).First();
            neww.Author = usr;
            db.News.Add(neww);
            TempData["message_success"] = "Новость добавлена!";
            db.SaveChanges();
            return RedirectToAction("Index", "News");
        }

        public ActionResult Details(int Num_of_news)
        {
            var user = (from us in db.News
                        where (us.Num_of_news == Num_of_news)
                        select us).First();
            var Names =
                (from nameA in db.Admin select new { ID = nameA.UserID, Name = nameA.Name }).Union(from nameT in db.Teachers select new { ID = nameT.UserId, Name = nameT.Name });
            var entry = (from res in db.News
                         join usr in Names on res.Author.UserId equals usr.ID
                         where (res.Num_of_news == Num_of_news)
                         select new Entrie() { Num_of_news = res.Num_of_news, DateTimeOfCreation = res.DateTimeOfCreation, Title = res.Title, Content = res.Content, Name = usr.Name }).Distinct().ToList().First();
            try
            {
                if (User.IsInRole("Admin"))
                {
                    ViewBag.Role = "~/Views/News/Menu_Admin.cshtml";
                }
                if ((User.IsInRole("Teacher")) && (WebSecurity.GetUserId(User.Identity.Name) == user.Author.UserId))
                {
                    ViewBag.Role = "~/Views/News/Menu_Admin.cshtml";
                }
                return View(entry);
            }
            catch
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult Edit(int Num_of_news)
        {
            News entry = (from ent in db.News
                          where ent.Num_of_news == Num_of_news
                          select ent).First();
            if (entry == null)
            {
                return HttpNotFound();
            }
            return View(entry);
        }

        [HttpPost]
        public ActionResult Edit(News entry)
        {
            News dbEntry = (from ent in db.News
                            where ent.Num_of_news == entry.Num_of_news
                            select ent).First();
            dbEntry.Title = entry.Title;
            dbEntry.Content = entry.Content;
            db.SaveChanges();
            TempData["message_success"] = "Новость изменена!";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int Num_of_news)
        {
            News entry = (from ent in db.News
                          where ent.Num_of_news == Num_of_news
                          select ent).First();
            if (entry == null)
            {
                return HttpNotFound();
            }
            return View(entry);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int Num_of_news)
        {
            News dbEntry = (from ent in db.News
                            where ent.Num_of_news == Num_of_news
                            select ent).First();
            db.News.Remove(dbEntry);
            db.SaveChanges();
            TempData["message_success"] = "Новость удалена!";
            return RedirectToAction("Index");
        }
    }
}
