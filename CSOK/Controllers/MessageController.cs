﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using CSOK.Core;
using CSOK.Filters;
using CSOK.Models;
using WebGrease.Css.Extensions;
using WebMatrix.WebData;

namespace CSOK.Controllers
{
    [Authorize]
    public class MessageController : Controller
    {
        private MessageCore _core;
        public ActionResult Inbox()
        {
            ViewBag.Title = "Входящие";
            ViewBag.Type = "inbox";
            return View("MessageList");
        }

        public ActionResult Sending()
        {
            ViewBag.Title = "Исходящие";
            ViewBag.Type = "sending";
            return View("MessageList");
        }

        public ActionResult Removed(string searchString = "", int start = 0, int count = 10)
        {
            ViewBag.Title = "Удаленные";
            ViewBag.Type = "removed";
            return View("MessageList");
        }

        public ActionResult MessageListPartial(string type, string searchString = "", int start = 0, int count = 10, 
            MessageSortOrder sortOrder = MessageSortOrder.DateDesc)
        {
            _core = new MessageCore(User.Identity.Name, (new UsersContext()).Users);
            var filter = new MessageFilter();
            if (type.Equals("inbox", StringComparison.InvariantCultureIgnoreCase))
            {
                filter = new MessageFilter
                {
                    WithInbox = true,
                    WithOutbox = false,
                    AreDeleted = false
                };
            }
            if (type.Equals("sending", StringComparison.InvariantCultureIgnoreCase))
            {
                filter = new MessageFilter
                {
                    WithInbox = false,
                    WithOutbox = true,
                    AreDeleted = false
                };
            }
            if (type.Equals("removed", StringComparison.InvariantCultureIgnoreCase))
            {
                filter = new MessageFilter
                {
                    WithInbox = true,
                    WithOutbox = true,
                    AreDeleted = true
                };
            }
            filter.SearchString = searchString;
            filter.StartIndex = start;
            filter.ItemCount = count;
            filter.SortOrder = sortOrder;
            return View("pMessageList", _core.GetFilteredMessageList(filter).ToList());
        }
        public JsonResult GetPeopleVariant(string query)
        {
            _core = new MessageCore(User.Identity.Name, (new UsersContext()).Users);
            var suggestions = _core.SearchUser(query);
            if (suggestions.Count == 0)
                suggestions.Add("Нет таких пользователей");
            var q = new JsonResult
            {
                Data = new
                {
                    suggestions
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            return q;
        }
        public ActionResult Send(int? idRe)
        {
            if (idRe == null)
            {
                return View(new SendMailModel
                {
                    AttachmentUrls = new List<HttpPostedFileBase>(),
                    Subject = "",
                    Text = "",
                    To = ""
                });
            }
            _core = new MessageCore(User.Identity.Name, (new UsersContext()).Users);
            return View(_core.GetRe(idRe.Value));
        }
        [HttpPost]
        public JsonResult CheckValidName(string names)
        {
            _core = new MessageCore(User.Identity.Name, (new UsersContext()).Users);
            var badNames = _core.GetBadNames(names);
            return new JsonResult { Data = new { badNames = badNames } };
        }
        [HttpPost]
        public ActionResult SendMessage(SendMailModel model)
        {
            _core = new MessageCore(User.Identity.Name, (new UsersContext()).Users);
            _core.SendMessage(model, Server.MapPath(ConfigurationManager.AppSettings["UploadUrl"]));
            return View("SendSuccesfull");
        }

        public ActionResult Read(int id = -1)
        {
            _core = new MessageCore(User.Identity.Name, (new UsersContext()).Users);
            try
            {
                return View(_core.GetMessageById(id));
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }
        [HttpPost]
        public JsonResult Delete(int id)
        {
            _core = new MessageCore(User.Identity.Name, (new UsersContext()).Users);
            _core.RemoveMessage(id);
            return new JsonResult{Data = new{ ok="ok"}};
        }

        [HttpPost]
        public JsonResult Restore(int id)
        {
            _core = new MessageCore(User.Identity.Name, (new UsersContext()).Users);
            _core.RestoreMessage(id);
            return new JsonResult { Data = new { ok = "ok" } };
        }
        ////я не смог пробиться сквозь валидацию регистрации, считерил
        private void AddUser(RegisterModel model)
        {
            var _dbContext = new MyDbContext();
            WebSecurity.CreateUserAndAccount(model.UserName, model.Password, new { Email = model.Email });
            WebSecurity.Login(model.UserName, model.Password);
            int id = WebSecurity.GetUserId(model.UserName);
            if (model.Num_of_group == null)
            {
                Teacher teacher = new Teacher();
                teacher.Name = model.Name;
                int num = Convert.ToInt32(model.Num_of_sub_faculty);
                //SubFaculty sf = (from subf in _dbContext.SubFaculties
                //                 where subf.Id == num
                //                 select subf).First();
                //teacher.SubFaculty = sf; 
                teacher.Post = model.Post;
                teacher.Rank = model.Rank;
                teacher.YOB = model.YOB;
                teacher.UserId = id;
                _dbContext.Teachers.Add(teacher);
                _dbContext.SaveChanges();
            }
            else
            {
                Student student = new Student();
                student.Num_of_record_book = model.Num_of_record_book;
                student.Name = model.Name;
                student.YOB = model.YOB.Date;
                //student.Num_of_sub_faculty = Convert.ToInt32(model.Num_of_sub_faculty);
                //CSOK.Models.Group gr = (from groupp in _dbContext.Groups
                //            where groupp.GroupName == model.Num_of_group
                //            select groupp).First();
                student.Group = _dbContext.Groups.FirstOrDefault();
                student.UserId = id;
                _dbContext.Students.Add(student);
                _dbContext.SaveChanges();
            }
            _dbContext.Dispose();
        }
        [AllowAnonymous]
        public ActionResult AddTestUser()
        {
            AddUser(new RegisterModel
            {
                UserName = "student4",
                Password = "qwerty123",
                Email = "mailStudent1@mail.ru",
                Name = "Петров Иван Сидорович",
                Num_of_record_book = "3",
                Num_of_sub_faculty = "33",
                Num_of_group = "K8-221",
                Post = "PostStudent1",
                Rank = "RankStudent1",
                YOB = new DateTime(2010, 01, 01)
            });
            //AddUser(new RegisterModel
            //{
            //    UserName = "prepod2",
            //    Password = "qwerty123",
            //    Email = "mailStudent2@mail.ru",
            //    Name = "NamePrepod2",
            //    Num_of_sub_faculty = "3",
            //    Post = "PostPrepod2",
            //    Rank = "RankPrepod2",
            //    YOB = new DateTime(2010, 01, 01)
            //});
            return View("Inbox");
        }
    }
}
