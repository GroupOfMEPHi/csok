﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSOK.Models;
using System.IO;
using System.Data.Entity;
using System.Data;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using WebMatrix.WebData;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;


namespace CSOK.Controllers
{
    [Authorize(Roles = "Teacher, Student, Admin")]

    public class TestsController : Controller
    {
        private MyDbContext db = new MyDbContext();
        //private int pageSize = 1;
        private UsersContext dbu = new UsersContext();

        //
        // GET: /Tests/
        [Authorize(Roles = "Teacher, Student")]
        public ActionResult Index()
        {
            if (User.IsInRole("Student"))
            {
                ViewBag.Role = "~/Views/Tests/Menu_student.cshtml";
            }
            if (User.IsInRole("Teacher"))
            {
                ViewBag.Role = "~/Views/Tests/Menu_teacher.cshtml";
            }
            ViewBag.message = TempData["message_success"];
                //return View(db.Testitems.ToList());
            
                return View();
            
        }

        //
        // GET: /Tests/Details/5
        [Authorize(Roles = "Teacher,Student")]
        public ActionResult Details(int id = 0)
        {
            if (User.IsInRole("Student"))
            {
                List<TestItemAnswer> testitem = db.Testitems.First(q => q.ID == id).TestItemAnswers.Where(q => q.Author.UserId==WebSecurity.CurrentUserId).ToList();
                //List<TestItemAnswer> testitem = db.TestItemAnswers.DefaultIfEmpty(q => q.AuthorID == WebSecurity.CurrentUserId && q.TestID=id);
                if (testitem == null)
                {
                    return HttpNotFound();
                }
                //ViewBag.Role = "~/Views/Tests/Details_student.cshtml";
                return View("Details_student",testitem);
            }
            else
            {
                TestItem testitem = db.Testitems.Find(id);
                if (testitem == null)
                {
                    return HttpNotFound();
                }
                //ViewBag.Role = "~/Views/Tests/Details_teacher.cshtml";
                return View(testitem);

            }
        }


        //
        // GET: /Tests/Create
        [HttpGet]
        [Authorize(Roles = "Teacher")]
        public ActionResult Create()
        {
            if (TempData["Err"] != null)
            {
                ViewBag.Err = TempData["Err"];
            }
            List<Question> qq = db.Questions.ToList();
            TestItem ti = new TestItem { Questions = qq };
            ti.Questions.ForEach(t => t.Body = Regex.Replace(HttpUtility.HtmlDecode(t.Body), "<.+?>", string.Empty));
            ti.ClosingDateTime = System.DateTime.Now;
            ti.OpeningDateTime = System.DateTime.Now;
            IEnumerable<Estimation> Estimations = db.Estimations.ToList();
            ViewBag.Est = Estimations;
            ViewBag.message = TempData["message_success"];

            return View(ti);
        }

        //
        // POST: /Tests/Create

        [HttpPost]
        [ValidateInput(false)]
        [Authorize(Roles = "Teacher")]
        public ActionResult Create(TestItem testitem, bool Timer, bool Paging, bool PagingType, int Estimation, bool TestMode, bool TestMode_1, bool TestMode_2)
        {
            
            if (!Timer) { testitem.Time = 0; }
            if (Paging)
            {if (PagingType){testitem.Paging = 2;}else{testitem.Paging = 1;}}
            else { testitem.Paging = 0; }
            if (TestMode) { if (TestMode_1) { testitem.TestMode = 1; } if (TestMode_2) { testitem.TestMode = 2; } }
            else { testitem.TestMode = 0; }
            TestItem t_i = new TestItem { Questions = new List<Question>() };
            List<Question> qq = db.Questions.ToList();


            List<Question> QE = testitem.Questions.FindAll(y => y.isSelected == true);
            Question QE2 = new Question();
            List<int> ids = QE.Select(id => id.ID).ToList();


            foreach (var id in ids)
            {
                QE2 = qq.Find(y => y.ID == id);

                t_i.Questions.Add(QE2);
            }

            //Заглушка для вопросов с блоками
          
            if (TestMode_2)
            {
                foreach (var t in t_i.Questions)
                {
                    if ((t.Blocks.Count != 0) || (t.Excess_Blocks.Count != 0))
                    { 
                        TempData["Err"] = "В режиме обучения с контролем вопроса недоступны блоки";
                        return RedirectToAction("Create"); 
                    }
                }

            }

            testitem.Questions.Clear();
            testitem.CreationDateTime = System.DateTime.Now;
            testitem.Author=db.User.Find(WebSecurity.CurrentUserId);
            testitem.Questions = t_i.Questions;
            testitem.Estimation = db.Estimations.Find(Estimation);
            testitem.Description = HttpUtility.HtmlEncode(testitem.Description);
            // if (ModelState.IsValid)
            // {
            //if (testitem.Subject == null)
            //    ViewBag.Message2 = "Введите название предмета";
            // else
            // {
            if (testitem.Questions.Count > 0)
            {
                TempData["message_success"] = "Тест успешно создан!";
                db.Testitems.Add(testitem);
                db.SaveChanges();
                ViewBag.message = TempData["message_success"];
                return RedirectToAction("Index");
            }
            else ViewBag.Message_1 = "Минимум 1 вопрос!";
            //  }

            List<Question> qqq = db.Questions.ToList();
            TestItem ti = new TestItem { Questions = qqq };


            
            return View(ti);



        }

        //
        // GET: /Tests/Edit/5
        [Authorize(Roles = "Teacher")]
        public ActionResult Edit(int id = 0)
        {
            TestItem testitem = db.Testitems.Find(id);
            if (testitem == null)
            {
                return HttpNotFound();
            }
            return View(testitem);
        }

        //
        // POST: /Tests/Edit/5

        [HttpPost]
        [Authorize(Roles = "Teacher")]
        public ActionResult Edit(TestItem testitem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(testitem).State = EntityState.Modified;
                db.SaveChanges();
                TempData["message_success"] = "Тест изменен!";
                return RedirectToAction("IndexPass", "Tests");
            }
            return View(testitem);
        }

        //
        // GET: /Tests/Delete/5
        [Authorize(Roles = "Teacher")]
        public ActionResult Delete(int id = 0)
        {
            TestItem testitem = db.Testitems.Find(id);
            if (testitem == null)
            {
                return HttpNotFound();
            }
            return View(testitem);
        }

        //
        // POST: /Tests/Delete/5
        [Authorize(Roles = "Teacher")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TestItem testitem = db.Testitems.Find(id);
            db.Testitems.Remove(testitem);
            db.SaveChanges();
            TempData["message_success"] = "Тест удален!";
            return RedirectToAction("IndexPass", "Tests");
        }

        /*protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }*/
        public ActionResult IndexPass()
        {
            if ((string)TempData["FailAttemp"]=="true")
            {
                ViewBag.FailAttemp = "Попыток больше нет!";
            }
            if (TempData["message_success"] != null)
            {
                ViewBag.message_success = TempData["message_success"];
            }
            //if ((string)TempData["TestPass"] == "true")
            //{
            //    ViewBag.TestPass = "Вы уже прошли тест!";
            //    //TempData["TestPass"] = "true";
            //    //return View(db.Testitems.ToList());
            //}
            if ((string)TempData["Err"] != "")
            {
                ViewBag.Err = TempData["Err"];
            }
            TempData["Err"]="";
            TempData["FailAttemp"] = "false";
            TempData["TestPass"] = "false";
            TempData["IndexPass"]="true";

            List<int> lastAttempt = new List<int>();
            List<TestItem> testitem = db.Testitems.ToList();
            IDictionary<int,int> resultAtt = new Dictionary<int,int>();
            IDictionary<int, int> resultScore = new Dictionary<int, int>();
          
            foreach (var res in testitem)
            {
                int last = 0;
                int score = 0;
                if (res.TestItemAnswers.Where(q => q.Author.UserId == WebSecurity.CurrentUserId).Count() != 0) { last = res.TestItemAnswers.Where(q => q.Author.UserId == WebSecurity.CurrentUserId).Last().CurrentAttempt; score = res.TestItemAnswers.Where(q => q.Author.UserId == WebSecurity.CurrentUserId).Max(q => q.TestScore); }
                resultAtt.Add(res.ID,last );
                resultScore.Add(res.ID, score);

            }

            ViewBag.LasAtt = resultAtt;
            ViewBag.Score = resultScore;


            return View(db.Testitems.ToList());
        }

        public ActionResult PassTestView(int id = 0)
        {
            
            TestItem testitemm = db.Testitems.First(q => q.ID == id);
            testitemm.Description = HttpUtility.HtmlDecode(testitemm.Description);
            List<TestItemAnswer> t = testitemm.TestItemAnswers.Where(q => q.Author.UserId == WebSecurity.CurrentUserId).ToList();
            int res = 0;
            if (t.Count() != 0)
            {
                if (testitemm.Estimation.EstID == 1)
                {
                    res = t.Max(e => e.TestScore);
                }
                else if (testitemm.Estimation.EstID == 2) { res = t.Sum(e => e.TestScore) / testitemm.Attempts; }
                else if (testitemm.Estimation.EstID == 3) { res = t.First().TestScore; }
                else if (testitemm.Estimation.EstID == 4) { res = t.Last().TestScore; }
                ViewBag.Score = res;
            }
            else {ViewBag.Score = 0; }

            return View(testitemm);
        }

        [HttpGet]
        [OutputCache(Duration=0, NoStore=false, Location=System.Web.UI.OutputCacheLocation.None)]
        public ActionResult PassTest(int id = 0)
        {
            
            try
            {
            var lst = new List<object>();
                
                TempData["TestPass"] = "false";
                TempData["IndexPass"] = "false";
                TestItem testitem = db.Testitems.First(q => q.ID == id);
                if (testitem.TestMode==2)
                {
                    TempData["TIDT"] = id;
                    return RedirectToAction("PassTestTraining", "Tests", new {id=id});  
                }
                TestItemAnswer testitemanswer = new TestItemAnswer();
                if (db.Testitems.First(q => q.ID == id).TestItemAnswers.Count(q => q.Author.UserId == WebSecurity.CurrentUserId && (q.CurrentAttempt == db.Testitems.First(t => t.ID == id).TestItemAnswers.Count(t => t.Author.UserId == WebSecurity.CurrentUserId))) != 0)
                {
                    int cnt = db.Testitems.First(q => q.ID == id).TestItemAnswers.Count(q => q.Author.UserId == WebSecurity.CurrentUserId);
                    testitemanswer = db.Testitems.First(q => q.ID == id).TestItemAnswers.First(q => q.Author.UserId == WebSecurity.CurrentUserId && q.CurrentAttempt == cnt);

                }


                if (db.Testitems.First(q => q.ID == id).TestItemAnswers.Count(q => q.Author.UserId == WebSecurity.CurrentUserId) == 0)
                {

                    //testitemanswer.TestID = id;
                    testitemanswer.Author = db.User.Find(WebSecurity.CurrentUserId);
                    //testitemanswer.ClosingDateTime = testitem.ClosingDateTime;
                    //testitemanswer.CreationDateTime = testitem.CreationDateTime;
                    //testitemanswer.OpeningDateTime = testitem.OpeningDateTime;
                    testitemanswer.StartDateTime = System.DateTime.Now;
                    testitemanswer.EndtDateTime = System.DateTime.Now;
                    testitemanswer.CurrentAttempt = 1;
                    testitemanswer.CurrentPasses = true;
                    testitemanswer.Discipline = testitem.Discipline;
                    testitemanswer.Paging = testitem.Paging;
                    testitemanswer.Questions = testitem.Questions;
                    testitemanswer.Subject = testitem.Subject;
                    testitemanswer.Time = testitem.Time;

                    db.Testitems.First(q => q.ID == id).TestItemAnswers.Add(testitemanswer);
                    db.SaveChanges();
                    foreach (var q in testitemanswer.Questions)
                    {
                        q.Body = HttpUtility.HtmlDecode(q.Body);

                        if (q.Matrices.Count() != 0)
                        {

                            lst.Add(OutputMatrix(q.Matrices.Single().Matrix_Id) as List<List<string>>);

                        }
                        if (q.HasPossibleAnswer == true)
                        {
                            foreach (var p in q.PossibleAnswers)
                            {
                                p.AnswerPossibleAnswers.Insert(0, new AnswerPossibleAnswer());

                            }
                        }
                        if (q.CorrectAnswer != null)
                        {
                            q.CorrectAnswer.AnswerCorrectAnswers.Insert(0, new AnswerCorrectAnswer());
                        }
                        //blocks
                        if (q.Blocks.Count() != 0)
                        {
                            foreach (var b in q.Blocks)
                            {
                                b.AnswerBlocks.Insert(0, new AnswerBlock());
                            }
                        }
                        //excess_blocks
                        if (q.Excess_Blocks.Count() != 0)
                        {
                            foreach (var b in q.Excess_Blocks)
                            {
                                b.AnswerExcessBlocks.Insert(0, new AnswerExcessBlock());
                            }
                        }
                    }
                    ViewBag.matrices = lst;
                    TempData["TID"] = testitemanswer.ID;
                    testitemanswer.Time = testitemanswer.Time * 60;
                    return View(testitemanswer);
                }
                if (db.Testitems.First(q => q.ID == id).TestItemAnswers.Count(q => q.Author.UserId == WebSecurity.CurrentUserId) != 0)
                {
                    
                    int cnt = db.Testitems.First(q => q.ID == id).TestItemAnswers.Count(q => q.Author.UserId == WebSecurity.CurrentUserId);
                    TestItemAnswer lasttestitem = db.Testitems.First(q => q.ID == id).TestItemAnswers.First(q => q.Author.UserId == WebSecurity.CurrentUserId && q.CurrentAttempt == cnt);
                    //bool curpass = db.TestItemAnswers.Last(q => q.TestID == id && q.AuthorID == WebSecurity.CurrentUserId).CurrentPasses;
                    if ((lasttestitem.CurrentPasses == false))
                    //if ((db.TestItemAnswers.Last(q => q.TestID == id && q.AuthorID == WebSecurity.CurrentUserId).CurrentPasses == false))
                    {
                        testitemanswer = new TestItemAnswer();
                        //testitemanswer.TestID = id;
                        testitemanswer.Author = db.User.Find(WebSecurity.CurrentUserId);
                        //testitemanswer.ClosingDateTime = testitem.ClosingDateTime;
                        //testitemanswer.CreationDateTime = testitem.CreationDateTime;
                        //testitemanswer.OpeningDateTime = testitem.OpeningDateTime;
                        testitemanswer.StartDateTime = System.DateTime.Now;
                        testitemanswer.EndtDateTime = System.DateTime.Now;
                        testitemanswer.CurrentAttempt = db.Testitems.First(q => q.ID == id).TestItemAnswers.First(q => q.Author.UserId == WebSecurity.CurrentUserId && q.CurrentAttempt == cnt).CurrentAttempt + 1;
                        if (testitemanswer.CurrentAttempt > testitem.Attempts)
                        {
                            TempData["FailAttemp"] = "true";
                            return RedirectToAction("IndexPass", "Tests");
                        }
                        testitemanswer.CurrentPasses = true;
                        testitemanswer.Discipline = testitem.Discipline;
                        testitemanswer.Paging = testitem.Paging;
                        testitemanswer.Questions = testitem.Questions;
                        testitemanswer.Subject = testitem.Subject;
                        testitemanswer.Time = testitem.Time;
                        db.Testitems.First(q => q.ID == id).TestItemAnswers.Add(testitemanswer);
                        db.SaveChanges();
                        foreach (var q in testitemanswer.Questions)
                        {
                            q.Body = HttpUtility.HtmlDecode(q.Body);

                            if (q.Matrices.Count() != 0)
                            {

                                lst.Add(OutputMatrix(q.Matrices.Single().Matrix_Id) as List<List<string>>);

                            }
                            if (q.HasPossibleAnswer == true)
                            {
                                foreach (var p in q.PossibleAnswers)
                                {
                                    p.AnswerPossibleAnswers.Insert(0, new AnswerPossibleAnswer());

                                }
                            }
                            if (q.CorrectAnswer != null)
                            {
                                q.CorrectAnswer.AnswerCorrectAnswers.Insert(0, new AnswerCorrectAnswer());
                            }
                            if (q.Blocks.Count() != 0)
                            {
                                foreach (var b in q.Blocks)
                                {
                                    b.AnswerBlocks.Insert(0, new AnswerBlock());
                                }
                            }
                            if (q.Excess_Blocks.Count() != 0)
                            {
                                foreach (var b in q.Excess_Blocks)
                                {
                                    b.AnswerExcessBlocks.Insert(0, new AnswerExcessBlock());
                                }
                            }
                        }
                        ViewBag.matrices = lst;
                        TempData["TID"] = testitemanswer.ID;
                        testitemanswer.Time = testitemanswer.Time * 60;
                        return View(testitemanswer);
                    }
                }


                foreach (var q in testitemanswer.Questions)
                {
                    q.Body = HttpUtility.HtmlDecode(q.Body);

                    if (q.Matrices.Count() != 0)
                    {
                        
                        lst.Add(OutputMatrix(q.Matrices.Single().Matrix_Id) as List<List<string>>);
                        
                    }
                    if (q.HasPossibleAnswer == true)
                    {
                        foreach (var p in q.PossibleAnswers)
                        {
                            p.AnswerPossibleAnswers.Insert(0, new AnswerPossibleAnswer());

                        }
                    }
                    if (q.CorrectAnswer != null)
                    {
                        q.CorrectAnswer.AnswerCorrectAnswers.Insert(0, new AnswerCorrectAnswer());
                    }
                    if (q.Blocks.Count() != 0)
                    {
                        foreach (var b in q.Blocks)
                        {
                            b.AnswerBlocks.Insert(0, new AnswerBlock());
                        }
                    }
                    if (q.Excess_Blocks.Count() != 0)
                    {
                        foreach (var b in q.Excess_Blocks)
                        {
                            b.AnswerExcessBlocks.Insert(0, new AnswerExcessBlock());
                        }
                    }
                }
                if (testitem.Time != 0)
                {
                    int minutStart = 0;
                    int secondStart = 59;
                    int minutPass = 0;
                    int secondPass = 0;
                    int minutTime = 0;
                    int secondTime = 0;
                    int restime = 0;
                    //string m = "";

                    TimeSpan diftime = new TimeSpan();
                    DateTime currenttime = System.DateTime.Now;
                    diftime = (currenttime - testitemanswer.StartDateTime);
                    minutStart = testitemanswer.Time;
                    minutPass = diftime.Minutes;
                    secondPass = diftime.Seconds;
                    minutTime = minutStart - minutPass - 1;
                    secondTime = secondStart - secondPass;
                    restime = minutTime * 60 + secondTime;
                    testitemanswer.Time = restime;
                }
                //ViewBag.Time = restime;
                ViewBag.matrices = lst;

                //if (minutTime < 10)
                //{
                //    m = "0" + minutTime;
                //}

                //else
                //{
                //    m = minutTime.ToString();
                //}
                //if (minutTime <= 0)
                //{
                //    m = "00";

                //}

                //ViewBag.minute = minutTime;
                //ViewBag.Time = m + ":" + secondTime;

                TempData["TID"] = testitemanswer.ID;
                return View(testitemanswer);
           }
            catch {
                TempData["Err"] = "Ошибка сервера";
                return RedirectToAction("IndexPass", "Tests");           
            }
        }



        [HttpGet]
        public ActionResult PassTestTraining(int id = 0, int flag=0, int step=0, int rez=0, int num=0)
        {
            // try
            //{
            ViewBag.Num = num;
            TempData["step"] = step;
            var lst = new List<object>();

                TempData["TestPass"] = "false";
                TempData["IndexPass"] = "false";
                TestItem testitem = db.Testitems.First(q => q.ID == id);
            Question question=new Question();
            if (flag == 0)
                {
                    ViewBag.Num = 1;
                question = testitem.Questions.First();
                }
            else
                {
                    if (rez == 0) { @ViewBag.Wrong = "Вы ответили неправильно!"; }
                    question = testitem.Questions.Skip(step).FirstOrDefault();
               
                    if (question == null) { TempData["End_Training"] = "true"; return View("ResultTraining"); }
                    if ((testitem.Questions.Skip(step + 1).FirstOrDefault()) == null) { TempData["End_Question"] = "true"; }
                  
            }

                question.Body = HttpUtility.HtmlDecode(question.Body);

                if (question.Matrices.Count() != 0)
                    {

                    lst.Add(OutputMatrix(question.Matrices.Single().Matrix_Id) as List<List<string>>);
                        
                        }
                if (question.HasPossibleAnswer == true)
                        {
                    foreach (var p in question.PossibleAnswers)
                            {
                                    p.AnswerPossibleAnswers.Insert(0, new AnswerPossibleAnswer());

                                }
                            }
                if (question.CorrectAnswer != null)
                            {
                    question.CorrectAnswer.AnswerCorrectAnswers.Insert(0, new AnswerCorrectAnswer());
                            }
                if (question.Blocks.Count() != 0)
                            {
                    foreach (var b in question.Blocks)
                                {
                                    b.AnswerBlocks.Insert(0, new AnswerBlock());
                                }
                            }
                if (question.Excess_Blocks.Count() != 0)
                    {
                    foreach (var b in question.Excess_Blocks)
                        {
                        b.AnswerExcessBlocks.Insert(0, new AnswerExcessBlock());
                        }
                    }
            
            ViewBag.matrices = lst;
            ViewBag.Subject = testitem.Subject;
            //TempData["TID"] = testitem.ID;
            if (flag == 0)
                    {
                return View(question);
                    }
            else
                    {

                return View("_PassTestTraining", question);
            }
            // }
            //catch {
            //   TempData["Err"] = "Ошибка сервера";
            //  return RedirectToAction("IndexPass", "Tests");           
            //}
            }

        //ouput matrix
        public List<List<string>> OutputMatrix(int id)
        {

            byte[] array = db.Matrices.SingleOrDefault(m => m.Matrix_Id == id).matr_bin;
            MemoryStream ms = new MemoryStream(array, 0, array.Length);
            BinaryFormatter formatter = new BinaryFormatter();
            List<List<string>> output_matrix = (List<List<string>>)formatter.Deserialize(ms);
            return output_matrix;

        }
        public static bool matrix_equals(byte[] a1, byte[] a2)
        {
            if (a1 == a2)
            {
                return true;
            }
            if ((a1 != null) && (a2 != null))
            {
                if (a1.Length != a2.Length)
                {
                    return false;
                }
                for (int i = 0; i < a1.Length; i++)
                {
                    if (a1[i] != a2[i])
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        [HttpPost]
        public ActionResult PassTest(TestItemAnswer testitemnew, List<int> ids, List<int> Excess_ids, List<List<List<string>>> elements)
        {
            //try{
            if ((string)TempData["TestPass"] == "true")
            {

                TempData["TestPassMassage"] = "Вы уже прошли тест!";
                return RedirectToAction("IndexPass", "Tests");
                
            }

            if ((string)TempData["IndexPass"] == "true")
            {

                TempData["TestPassMassage"] = "Вы уже прошли тест!";
                return RedirectToAction("IndexPass", "Tests");

            }

           
            //int id = 0;
            //TestItemAnswer testitemnew = new TestItemAnswer();
            //int id = (int)TempData["TID"];
            int id = testitemnew.ID;
            TestItemAnswer testitem = db.TestItemAnswers.First(q => q.ID == id);
            testitem.EndtDateTime = System.DateTime.Now;

            
           
            //if (testitem.Time != 0)
            //{
            //    int minutStart = 0;
            //    int minutPass = 0;
            //    int secondPass = 0;
            //    TimeSpan diftime = new TimeSpan();
            //    DateTime currenttime = System.DateTime.Now;
            //    diftime = (currenttime - testitem.StartDateTime);
            //    minutStart = testitem.Time;
            //    minutPass = diftime.Minutes;
            //    secondPass = diftime.Seconds;
            //    if (minutPass > testitem.Time) { minutPass = testitem.Time; secondPass = 00; }
            //    testitem.SpentTime = minutPass + ":" + secondPass;
            //}
           // TestItem testitem = db.Testitems.First(q => q.ID == id);
            // TestItem testitem = new TestItem();
            int cq = testitem.Questions.Count();
            int rez = 0;
            int qust = 0;
            int tempq = 0;
            int tempb = 0;
            int tempc = 0;
            int tempctrash = 0;
            int tempbid = 0;
            int mtx = 0;
            foreach (var q in testitem.Questions)
            {
                int poss = 0;
                int bi = 0;
                
                if ((q.Matrices.Count() != 0) && (q.MatrixAnswer == true))
                {
                    var an_m = elements[mtx];
                    MemoryStream ms = new MemoryStream();
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(ms, an_m);
                    q.Matrices.Single().AnswerMatrices.Insert(0, new AnswerMatrix { TestItemAnswer=db.TestItemAnswers.First(t=>t.ID==testitem.ID), matr_bin = ms.ToArray(), nRows = elements.ElementAt(mtx).Count(), nCols = elements.ElementAt(mtx).ElementAt(0).Count() });
                    
                        var equal = matrix_equals(ms.ToArray(), q.Matrices.Single().correct_matr_bin);
                        if (equal == true)
                        { 
                            rez++;
                            db.TestResults.Add(new TestResults { Question = q, IsRight = true, TestItemAnswer = testitem });
                        }
                        else db.TestResults.Add(new TestResults { Question = q, IsRight = false, TestItemAnswer = testitem });

                        mtx++;
                }
                if (q.HasPossibleAnswer == true)
                {
                    if (testitem.Questions[qust].RadioAnswer == true)
                    {

                        foreach (var p in testitemnew.Questions[qust].PossibleAnswers)
                        {
                            p.AnswerPossibleAnswers.Insert(0, new AnswerPossibleAnswer());

                        }
                        if (testitemnew.Questions[qust].SelectedRadio != null)
                        {
                            int a = Convert.ToInt32(testitemnew.Questions[qust].SelectedRadio);
                            testitemnew.Questions[qust].PossibleAnswers[a].AnswerPossibleAnswers[0].IsRight = true;
                        }
                    }
                    int cp = q.PossibleAnswers.Count();
                    int posansw = 0;
                    foreach (var p in q.PossibleAnswers)
                    {

                        p.AnswerPossibleAnswers.Insert(0, new AnswerPossibleAnswer { TestItemAnswer = db.TestItemAnswers.First(t => t.ID == testitem.ID), IsRight = testitemnew.Questions[qust].PossibleAnswers[poss].AnswerPossibleAnswers[0].IsRight });
                        if (p.IsRight == testitemnew.Questions[qust].PossibleAnswers[poss].AnswerPossibleAnswers[0].IsRight)
                        {
                            posansw = posansw + 1;
                        }
                        poss = poss + 1;
                    }
                    if (posansw == cp)
                    {
                        rez = rez + 1;
                        db.TestResults.Add(new TestResults { Question = q, IsRight = true, TestItemAnswer = testitem });
						//db.TestResults.Add(new TestResults {  Question_ID = q.ID, Student_ID = WebSecurity.CurrentUserId, IsRight = true, TestItem_ID = id, DatePass = System.DateTime.Now });
                    }
					else
                    {
                        db.TestResults.Add(new TestResults { Question = q, IsRight = false, TestItemAnswer = testitem });
                        //db.TestResults.Add(new TestResults {  Question_ID = q.ID, Student_ID = WebSecurity.CurrentUserId, IsRight = false, TestItem_ID = id, DatePass = System.DateTime.Now });
                    }
                }
                if (q.CorrectAnswer != null)
                {
                    q.CorrectAnswer.AnswerCorrectAnswers.Insert(0, new AnswerCorrectAnswer { TestItemAnswer=db.TestItemAnswers.First(t=>t.ID==testitem.ID), Body = testitemnew.Questions[qust].CorrectAnswer.AnswerCorrectAnswers[0].Body });
                    if (q.CorrectAnswer.Body == testitemnew.Questions[qust].CorrectAnswer.AnswerCorrectAnswers[0].Body)
                    {
                        rez = rez + 1;
                        db.TestResults.Add(new TestResults { Question = q, IsRight = true, TestItemAnswer = testitem });
						//db.TestResults.Add(new TestResults { Question_ID = q.ID, Student_ID = WebSecurity.CurrentUserId, IsRight = true, TestItem_ID = id, DatePass = System.DateTime.Now });
                    }
					else
                    {
                        db.TestResults.Add(new TestResults { Question = q, IsRight = false, TestItemAnswer = testitem });
                        //db.TestResults.Add(new TestResults { Question_ID = q.ID, Student_ID = WebSecurity.CurrentUserId, IsRight = false, TestItem_ID = id, DatePass = System.DateTime.Now });
                    }
                }
                if (q.Blocks.Count() != 0)
                {
                    
                    int cb = q.Blocks.Count();
                    int blockrez = 0;
                    tempbid = q.Blocks[0].ID;
                    int[] ansbody=new int[cb];
                    //Конвертируем порядок блоков(ответы) в id ответов
                    foreach (var b in testitemnew.Questions[qust].Blocks)
                    {
                        testitemnew.Questions[qust].Blocks[b.AnswerBlocks[0].Order-tempc].AnswerBlocks[0].Block=db.Blocks.Find(tempbid+bi);
                        bi++;
                    }
                    bi=0;
                    foreach (var b in q.Blocks)
                    {

                        if (b.ID == (testitemnew.Questions[qust].Blocks[bi].AnswerBlocks[0].Block.ID))
                        {
                           
                            blockrez = blockrez + 1;
                        }

                     
                        
                        b.AnswerBlocks.Insert(0, new AnswerBlock { TestItemAnswer = db.TestItemAnswers.First(t => t.ID == testitem.ID), Body = testitemnew.Questions[qust].Blocks[bi].AnswerBlocks[0].Block.Body});
                        //b.AnswerBlocks[0].Block = testitemnew.Questions[qust].Blocks[bi];
                        bi = bi + 1;
                    }
                    if (blockrez == cb)
                    
                    {
                        rez++;
                        db.TestResults.Add(new TestResults { Question = q, IsRight = true, TestItemAnswer = testitem });
						//db.TestResults.Add(new TestResults { Question_ID = q.ID, Student_ID = WebSecurity.CurrentUserId, IsRight = true, TestItem_ID = id, DatePass = System.DateTime.Now });
                    }
					else
                    {
                        db.TestResults.Add(new TestResults { Question = q, IsRight = false, TestItemAnswer = testitem });
                        //db.TestResults.Add(new TestResults { Question_ID = q.ID, Student_ID = WebSecurity.CurrentUserId, IsRight = false, TestItem_ID = id, DatePass = System.DateTime.Now });
                    }
                    
                    tempc = tempc+q.Blocks.Count();
                    tempb++;
                }
                if (q.Excess_Blocks.Count() != 0)
                {
                    int cb = q.Excess_Blocks.Count();
                    int blockrez = 0;
                    tempbid = q.Excess_Blocks[0].ID;
                    int[] ansbody = new int[cb];
                    int nottrash = testitemnew.Questions[qust].Excess_Blocks.Where(t => t.AnswerExcessBlocks[0].Order < 100500).Count();
                    //Конвертируем порядок блоков(ответы) в id ответов
                    foreach (var b in testitemnew.Questions[qust].Excess_Blocks)
                    {
                        if (testitemnew.Questions[qust].Excess_Blocks[bi].AnswerExcessBlocks[0].Order >= 100500)
                        {
                            testitemnew.Questions[qust].Excess_Blocks[b.AnswerExcessBlocks[0].Order - tempctrash - 100500 + nottrash].AnswerExcessBlocks[0].Excess_Block = db.Excess_Blocks.Find(tempbid + bi);
                            testitemnew.Questions[qust].Excess_Blocks[b.AnswerExcessBlocks[0].Order - tempctrash - 100500 + nottrash].AnswerExcessBlocks[0].IsRight = false;
                            bi++;
                        }
                        else
                        {
                            testitemnew.Questions[qust].Excess_Blocks[b.AnswerExcessBlocks[0].Order - tempc].AnswerExcessBlocks[0].Excess_Block = db.Excess_Blocks.Find(tempbid + bi);
                            testitemnew.Questions[qust].Excess_Blocks[b.AnswerExcessBlocks[0].Order - tempc].AnswerExcessBlocks[0].IsRight = true;
                            bi++;
                        }
                    }
                    bi = 0;
                    foreach (var b in q.Excess_Blocks)
                    {

                        if (testitemnew.Questions[qust].Excess_Blocks[bi].AnswerExcessBlocks[0].Order >= 100500)
                        {
                            if (b.IsRight == testitemnew.Questions[qust].Excess_Blocks[bi].AnswerExcessBlocks[0].IsRight) 
                            { 
                                blockrez = blockrez + 1; 
                            }
                            b.AnswerExcessBlocks.Insert(0, new AnswerExcessBlock { TestItemAnswer = db.TestItemAnswers.First(t => t.ID == testitem.ID), Body = testitemnew.Questions[qust].Excess_Blocks[bi].AnswerExcessBlocks[0].Excess_Block.Body, IsRight = testitemnew.Questions[qust].Excess_Blocks[bi].AnswerExcessBlocks[0].IsRight });
                        }
                        else
                        {
                        if (b.ID == (testitemnew.Questions[qust].Excess_Blocks[bi].AnswerExcessBlocks[0].Excess_Block.ID))
                            {
                                blockrez = blockrez + 1;
                               
                            }
                            b.AnswerExcessBlocks.Insert(0, new AnswerExcessBlock { TestItemAnswer = db.TestItemAnswers.First(t => t.ID == testitem.ID), Body = testitemnew.Questions[qust].Excess_Blocks[bi].AnswerExcessBlocks[0].Excess_Block.Body, IsRight = testitemnew.Questions[qust].Excess_Blocks[bi].AnswerExcessBlocks[0].IsRight });
                        }

                      
                        bi = bi + 1;
                    }
                    
                    if (blockrez == cb)
                    {
                        rez++;
                        //db.TestResults.Add(new TestResults { Question_ID = q.ID, Student_ID = WebSecurity.CurrentUserId, IsRight = true, TestItem_ID = id, DatePass = System.DateTime.Now });
                        db.TestResults.Add(new TestResults { Question = q, IsRight = true, TestItemAnswer = testitem});
                    }
                    else
                    {
                        //db.TestResults.Add(new TestResults { Question_ID = q.ID, Student_ID = WebSecurity.CurrentUserId, IsRight = false, TestItem_ID = id, DatePass = System.DateTime.Now });
                        db.TestResults.Add(new TestResults { Question = q, IsRight = false, TestItemAnswer = testitem });
                    }

                    tempc = tempc + nottrash;
                    tempctrash = q.Excess_Blocks.Count() - nottrash;
                    tempb++;
                }
                qust = qust + 1;
                tempq = tempq + 1;
            }
            string kon = "";
            if (rez == 1) {
                kon = " вопрос";
            }
            else if (rez == 2 || rez == 3 || rez == 4)
            {
                kon = " вопроса";
            }
            else {
                kon = " вопросов";

            }


            //ViewBag.Message = "Вы ответили правильно на " + rez + kon + " из " + cq;
            testitem.TestScore = rez;
            testitem.CurrentPasses = false;
            db.Entry(testitem).State = EntityState.Modified;
            db.SaveChanges();
            TempData["TestPass"] = "true";
            TempData["TestResult"] = "Вы ответили правильно на " + rez + kon + " из " + cq;
            TempData["TIDT"] = testitemnew.ID;
            return RedirectToAction("Result", "Tests");  
            //return View("Result");
        //}
            //catch {
            //    TempData["Err"] = "Ошибка сервера";
                
            //    return RedirectToAction("IndexPass", "Tests");           
            //}


        }


        [HttpPost]
        public ActionResult PassTestTraining(Question question, List<int> ids, List<int> Excess_ids, List<List<List<string>>> elements, int num)
        {
            //try{
            //if ((string)TempData["TestPass"] == "true")
            //{

            //    TempData["TestPassMassage"] = "Вы уже прошли тест!";
            //    return RedirectToAction("IndexPass", "Tests");

            //}

            //if ((string)TempData["IndexPass"] == "true")
            //{

            //    TempData["TestPassMassage"] = "Вы уже прошли тест!";
            //    return RedirectToAction("IndexPass", "Tests");

            //}


            //int id = 0;
            //TestItemAnswer testitemnew = new TestItemAnswer();
            //int id = (int)TempData["TID"];
            int id = question.ID;
            int tid = question.TestItems[0].ID;
            //int tid = (int)TempData["TID"];
            TestItem testitem = db.Testitems.First(q => q.ID == tid);
            Question _question = testitem.Questions.First(q=>q.ID==question.ID);
            int cq = testitem.Questions.Count();
            int rez = 0;
            int qust = 0;
            int tempq = 0;
            int tempb = 0;
            int tempc = 0;
            int tempctrash = 0;
            int tempbid = 0;

                int poss = 0;
                int bi = 0;
                if ((_question.Matrices.Count != 0) && (_question.MatrixAnswer == true))
                {
                    var an_m = elements[0];
                    MemoryStream ms = new MemoryStream();
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(ms, an_m);
                    
                    if (question.MatrixAnswer == true)
                    {
                        var equal = matrix_equals(ms.ToArray(), _question.Matrices.Single().correct_matr_bin);
                        if (equal == true) rez++;
                    }
                }
                if (_question.HasPossibleAnswer == true)
                {
                    if (_question.RadioAnswer == true)
                    {
                        foreach (var p in question.PossibleAnswers)
                        {
                            p.AnswerPossibleAnswers.Insert(0, new AnswerPossibleAnswer());

                        }
                      
                        if (question.SelectedRadio != null)
                        {
                            int a = Convert.ToInt32(question.SelectedRadio);
                            question.PossibleAnswers[a].AnswerPossibleAnswers[0].IsRight = true;
                        }
                    }
                    int cp = _question.PossibleAnswers.Count();
                    int posansw = 0;
                    foreach (var p in _question.PossibleAnswers)
                    {

                        
                        if (p.IsRight == question.PossibleAnswers[poss].AnswerPossibleAnswers[0].IsRight)
                        {
                            posansw = posansw + 1;
                        }
                        poss = poss + 1;
                    }
                    if (posansw == cp)
                    {
                        rez = rez + 1;
                        
                    }
                    
                }
                if (_question.CorrectAnswer != null)
                {
                    
                    if (_question.CorrectAnswer.Body == question.CorrectAnswer.AnswerCorrectAnswers[0].Body)
                    {
                        rez = rez + 1;
                    }
                    
                }
                if (_question.Blocks.Count() != 0)
                {

                    int cb = _question.Blocks.Count();
                    int blockrez = 0;
                    tempbid = _question.Blocks[0].ID;
                    int[] ansbody = new int[cb];
                    //Конвертируем порядок блоков(ответы) в id ответов
                    foreach (var b in question.Blocks)
                    {
                        question.Blocks[b.AnswerBlocks[0].Order - tempc].AnswerBlocks[0].Block = db.Blocks.Find(tempbid + bi);
                        bi++;
                    }
                    bi = 0;
                    foreach (var b in _question.Blocks)
                    {

                        if (b.ID == (question.Blocks[bi].AnswerBlocks[0].Block.ID))
                        {

                            blockrez = blockrez + 1;
                        }

                        bi = bi + 1;
                    }
                    if (blockrez == cb)
                    {
                        rez++;
                        
                    }
                   

                    tempc = tempc + _question.Blocks.Count();
                    tempb++;
                }
                if (_question.Excess_Blocks.Count() != 0)
                {
                    int cb = _question.Excess_Blocks.Count();
                    int blockrez = 0;
                    tempbid = _question.Excess_Blocks[0].ID;
                    int[] ansbody = new int[cb];
                    int nottrash = question.Excess_Blocks.Where(t => t.AnswerExcessBlocks[0].Order < 100500).Count();
                    //Конвертируем порядок блоков(ответы) в id ответов
                    foreach (var b in question.Excess_Blocks)
                    {
                        if (question.Excess_Blocks[bi].AnswerExcessBlocks[0].Order >= 100500)
                        {
                            question.Excess_Blocks[b.AnswerExcessBlocks[0].Order - tempctrash - 100500 + nottrash].AnswerExcessBlocks[0].Excess_Block = db.Excess_Blocks.Find(tempbid + bi);
                            question.Excess_Blocks[b.AnswerExcessBlocks[0].Order - tempctrash - 100500 + nottrash].AnswerExcessBlocks[0].IsRight = false;
                            bi++;
                        }
                        else
                        {
                            question.Excess_Blocks[b.AnswerExcessBlocks[0].Order - tempc].AnswerExcessBlocks[0].Excess_Block = db.Excess_Blocks.Find(tempbid + bi);
                            question.Excess_Blocks[b.AnswerExcessBlocks[0].Order - tempc].AnswerExcessBlocks[0].IsRight = true;
                            bi++;
                        }
                    }
                    bi = 0;
                    foreach (var b in _question.Excess_Blocks)
                    {

                        if (question.Excess_Blocks[bi].AnswerExcessBlocks[0].Order >= 100500)
                        {
                            if (b.IsRight == question.Excess_Blocks[bi].AnswerExcessBlocks[0].IsRight)
                            {
                                blockrez = blockrez + 1;
                            }
                        }
                        else
                        {
                            if (b.ID == (question.Excess_Blocks[bi].AnswerExcessBlocks[0].Excess_Block.ID))
                            {
                                blockrez = blockrez + 1;

                            }
                        }


                        bi = bi + 1;
                    }

                    if (blockrez == cb)
                    {
                        rez++;
                        
                    }
                    
                    tempc = tempc + nottrash;
                    tempctrash = _question.Excess_Blocks.Count() - nottrash;
                    tempb++;
                }
                qust = qust + 1;
                tempq = tempq + 1;
         
            
                if (rez == 1) { TempData["step"] = (int)TempData["step"] + 1; num++; }

            return RedirectToAction("PassTestTraining", "Tests", new {id=tid ,flag = 1, step = TempData["step"], rez=rez, num=num });

            //string kon = "";
            //if (rez == 1)
            //{
            //    kon = " вопрос";
            //}
            //else if (rez == 2 || rez == 3 || rez == 4)
            //{
            //    kon = " вопроса";
            //}
            //else
            //{
            //    kon = " вопросов";

            //}


            //TempData["TestPass"] = "true";
            //TempData["TestResult"] = "Вы ответили правильно на " + rez + kon + " из " + cq;
            ////TempData["TIDT"] = testitemnew.ID;
            //return RedirectToAction("Result", "Tests");
            
            //catch {
            //    TempData["Err"] = "Ошибка сервера";

            //    return RedirectToAction("IndexPass", "Tests");           
            //}


        }
        
        
        public ActionResult Result()
        {
            if (TempData["TestResult"] == null || (string)TempData["TestResult"] == "")
            {
                return RedirectToAction("IndexPass", "Tests");   
            }
            ViewBag.Message = TempData["TestResult"];
            TempData["TestResult"]="";
            int id=(int)TempData["TIDT"];
            if (db.TestItemAnswers.First(q => q.ID == id).TestItem.TestMode == 1) { return RedirectToAction("TeacherStudentResultView", "MyCourses", new {id=id }); }
            return View();
        }

        public ActionResult CreateQuestionAndTests()
        {
            ViewBag.message = TempData["message_success"];
            return View();
        }

    }
}