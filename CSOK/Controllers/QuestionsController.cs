﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSOK.Models;
using System.IO;
using System.Data.Entity;
using System.Data;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using WebMatrix.WebData;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;


namespace CSOK.Controllers
{
    [Authorize]
    [Authorize(Roles = "Teacher")]
    public class QuestionsController : Controller
    {
        private MyDbContext db = new MyDbContext();
        private UsersContext dbu = new UsersContext();
        //
        // GET: /Question/

        public ActionResult Index()
        {
            //List<int> guids = db.Images.Select(img => img.Question.ID).ToList();
            IEnumerable<QuestionsIndex> query = (from q in db.Questions
                                                 select new QuestionsIndex { ID = q.ID, Title = q.Title, Body = q.Body, CreationDataTime = q.CreationDataTime, ImageId = q.Images.Select(ids => ids.ImageId) }).ToList();

            /*foreach (var q in query)
            { 
                q.AnswCount = (from a in db.Answers
                               where (a.Question.ID==q.ID)
                               select a).Count();
                 
            }*/
            return View(query);
        }

        //
        // GET: /Questions/Details/5

        public PartialViewResult Details(int id = 0)
        {
            Question _question = db.Questions.First(q => q.ID == id);
            // _question.Author = db.UserProfiles.First(a => a.UserId == _question.Author.UserId);
            //logger.Info(db.Questions.First(q => q.ID == id).Author.UserName);
            //  List<Answer> _answers = (from a in db.Answers
            //                      where (a.Question.ID == id)
            //                select a).ToList();
            /*  if (_question == null)
              {
                  return HttpNotFound();
              }*/

            // QuestionAndAnswers qa = new QuestionAndAnswers { Question = _question, Answers = _answers };
            QuestionsIndex QI = new QuestionsIndex
            {
                ID = _question.ID,
                Title = _question.Title,
                Body = HttpUtility.HtmlDecode(_question.Body),
                ImageId = _question.Images.Select(ids => ids.ImageId),
                PossibleAnswers = _question.PossibleAnswers,
                CheckBoxAnswer = _question.CheckBoxAnswer,
                RadioAnswer = _question.RadioAnswer,
                HasPossibleAnswer = _question.HasPossibleAnswer,
                CorrectAnswer = _question.CorrectAnswer,
                BlockBody = _question.Blocks.Select(b => b.Body),
                ExcessBlockBody = _question.Excess_Blocks.Select(b=>b.Body),
                Matrix = _question.Matrices.SingleOrDefault(m => m.Question.ID == id)
            };
            if (QI.Matrix != null)
            {
                ViewBag.result = OutputMatrix(QI.Matrix.Matrix_Id) as List<List<string>>;
            }

            return PartialView("Details", QI);
        }
        [HttpGet]
        public JsonResult GetThemes()
        {
            var lst = (from t in db.Themes select new { t.Id, t.Name, StructureId = t.Structure.Id });
            List<object> themes = new List<object>();
            foreach (var th in lst)
            {
                themes.Add(th);
            }
            return Json(themes, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Create()
        {
            //ViewBag.structs = new SelectList(db.Structures.ToList(), "Id", "Name");
            if(db.Structures.ToList().Count() != 0 ) ViewBag.structs = db.Structures.ToList();
            return View();
        }

        //
        // POST: /Question/Create
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Create(Question question, List<string> answ, string answrad, HttpPostedFileBase[] files, List<bool> CorrAnsw, string[] checkedValues, int theme_id)
        {
            bool ch = ((CorrAnsw[0] == true) == (question.CorrectAnswer.Body != null)) && ((CorrAnsw[0] == false) == (question.CorrectAnswer.Body == null));
            bool chform = (question.HasPossibleAnswer != false) || (CorrAnsw[0] != false);
            bool chradORbox = (question.HasPossibleAnswer == false) || ((question.HasPossibleAnswer == true) && ((question.RadioAnswer != false) || (question.CheckBoxAnswer != false)));
            //question.Body = str_body.Replace(@"&amp;", "&").Replace(@"&gt;", ">").Replace(@"&lt;", "<").Replace(@"&#39;", "'").Replace(@"&quot;", "\"");
            string description = HttpUtility.HtmlEncode(question.Body);
            question.Body = description;
            if (db.Structures.ToList().Count() != 0) ViewBag.structs = db.Structures.ToList();
            var th = db.Themes.Find(theme_id);
            question.Title = th.Name;
            question.Theme = th;
            if (ModelState.IsValid && ch && chform && chradORbox)
            {

                //int currentID = WebSecurity.CurrentUserId;
                //User currentAuthor = dbu.Users.Find(WebSecurity.CurrentUserId);
                question.CreationDataTime = System.DateTime.Now;
                question.Author = db.User.Find(WebSecurity.CurrentUserId);
                if (!question.HasPossibleAnswer)
                {
                    question.PossibleAnswers.Clear();
                }
                else
                {
                    question.CorrectAnswer = null;
                    int l = answ.Count;
                    bool[] answRight = new bool[l];

                    if (answrad != null)//занесение в массив правильного номера raduobutton
                    {
                        for (int i = 0; i < l; i++)
                        {
                            answRight[i] = false;
                        }
                        int a = Convert.ToInt32(answrad);
                        answRight[a] = true;
                    }

                    if (checkedValues != null)//занесение в массив отмеченных checkbox
                    {
                        for (int i = 0; i < l; i++)
                        {
                            answRight[i] = false;
                        }
                        foreach (string checkedValue in checkedValues)
                        {
                            int a = Convert.ToInt32(checkedValue);
                            answRight[a] = true;

                        }

                    }
                    for (int i = 0; i < answ.Count; i++)
                    {
                        //Answer a = new Answer();
                        question.PossibleAnswers.Add(new PossibleAnswer { Body = answ[i], IsRight = answRight[i] });

                    }
                }
                if (files != null)
                {
                    foreach (var file in files)
                    {
                        if (file != null)
                        {
                            MemoryStream ms = new MemoryStream();
                            file.InputStream.CopyTo(ms);
                            question.Images.Add(new Image
                            {
                                //ImageId = Guid.NewGuid(),
                                ImageBinary = ms.GetBuffer(),
                                ImageLength = file.ContentLength,
                                ImageName = file.FileName,
                                ImageType = file.ContentType
                            });

                        }
                    }
                }

                //foreach (var p in question.PossibleAnswers)
                //{
                //    p.Answers.Add(new Answer());
                //}
                TempData["message_success"] = "Вопрос успешно создан!";
                db.Questions.Add(question);
                //ans_cnt.AnswCount = question.PossibleAnswers.Count;
                db.SaveChanges();
                return RedirectToAction("CreateQuestionAndTests", "Tests");
            }
            //if (ch == false)
            //{
            //    ViewBag.Message = "Введите правильный вариант свободного ответа!";
            //}
            return View(question);
        }



        public FileContentResult OutputImage(int id)
        {

            Image image = db.Images.SingleOrDefault(f => f.ImageId == id);
            return File(image.ImageBinary, image.ImageType);

        }
        //Matrix
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Create_with_matrix(Matrix matrix, List<List<string>> elements, List<List<string>> correct_elements, List<string> answ, string answrad, List<bool> CorrAnsw, string[] checkedValues, int theme_id)
        {
            bool ch = ((CorrAnsw[0] == true) == (matrix.Question.CorrectAnswer.Body != null)) && ((CorrAnsw[0] == false) == (matrix.Question.CorrectAnswer.Body == null));
            bool chform = (matrix.Question.HasPossibleAnswer != false) || (CorrAnsw[0] != false || (matrix.Question.MatrixAnswer != false));
            bool chradORbox = (matrix.Question.HasPossibleAnswer == false) || ((matrix.Question.HasPossibleAnswer == true) && ((matrix.Question.RadioAnswer != false) || (matrix.Question.CheckBoxAnswer != false)));
            if (db.Structures.ToList().Count() != 0) ViewBag.structs = db.Structures.ToList();
            var th = db.Themes.Find(theme_id);
            matrix.Question.Theme = th;
            matrix.Question.Title = th.Name;
            if (ModelState.IsValid && ch && chform && chradORbox)
            {
                MemoryStream ms = new MemoryStream();
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(ms, elements);
                if (matrix.Question.MatrixAnswer == true)
                {
                    matrix.Question.PossibleAnswers.Clear();
                    matrix.Question.CorrectAnswer = null;
                    MemoryStream ms2 = new MemoryStream();
                    BinaryFormatter formatter2 = new BinaryFormatter();
                    formatter.Serialize(ms2, correct_elements);
                    matrix.correct_matr_bin = ms2.ToArray();
                }
                else
                {
                    if (!matrix.Question.HasPossibleAnswer)
                    {
                        matrix.Question.PossibleAnswers.Clear();
                    }
                    else
                    {
                        matrix.Question.CorrectAnswer = null;
                        int l = answ.Count;
                        bool[] answRight = new bool[l];

                        if (answrad != null)//занесение в массив правильного номера raduobutton
                        {
                            for (int i = 0; i < l; i++)
                            {
                                answRight[i] = false;
                            }
                            int a = Convert.ToInt32(answrad);
                            answRight[a] = true;
                        }

                        if (checkedValues != null)//занесение в массив отмеченных checkbox
                        {
                            for (int i = 0; i < l; i++)
                            {
                                answRight[i] = false;
                            }
                            foreach (string checkedValue in checkedValues)
                            {
                                int a = Convert.ToInt32(checkedValue);
                                answRight[a] = true;

                            }

                        }
                        for (int i = 0; i < answ.Count; i++)
                        {
                            //Answer a = new Answer();
                            matrix.Question.PossibleAnswers.Add(new PossibleAnswer { Body = answ[i], IsRight = answRight[i] });

                        }
                    }
                }
            
                matrix.Question.Body = HttpUtility.HtmlEncode(matrix.Question.Body);
                matrix.Question.Author = db.User.Find(WebSecurity.CurrentUserId);
                matrix.matr_bin = ms.ToArray();
                
                //matrix.binary = ms.GetBuffer();
                //_question.PossibleAnswers.Clear();
                matrix.Question.CreationDataTime = System.DateTime.Now;
                TempData["message_success"] = "Вопрос успешно создан!";
                db.Matrices.Add(matrix);
                db.SaveChanges();
                return RedirectToAction("CreateQuestionAndTests", "Tests");
            }
            return View(matrix);
        }
        [HttpGet]
        public ActionResult Create_with_matrix()
        {
            //ViewBag.structs = new SelectList(db.Structures.ToList(), "Id", "Name");
            if (db.Structures.ToList().Count() != 0) ViewBag.structs = db.Structures.ToList();
            return View();
        }
        public List<List<string>> OutputMatrix(int id)
        {

            byte[] array = db.Matrices.SingleOrDefault(m => m.Matrix_Id == id).matr_bin;
            MemoryStream ms = new MemoryStream(array, 0, array.Length);
            BinaryFormatter formatter = new BinaryFormatter();
            List<List<string>> output_matrix = (List<List<string>>)formatter.Deserialize(ms);
            return output_matrix;

        }

        //Matrix
        [HttpGet]
        public ActionResult CreateBlock()
        {
            //ViewBag.structs = new SelectList(db.Structures.ToList(), "Id", "Name");
            if (db.Structures.ToList().Count() != 0) ViewBag.structs = db.Structures.ToList();
            return View();
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateBlock(Question question, List<string> block, string[] right_block, int theme_id)
        {
            if (db.Structures.ToList().Count() != 0) ViewBag.structs = db.Structures.ToList();
            var th = db.Themes.Find(theme_id);
            question.Theme = th;
            question.Title = th.Name;
            if (ModelState.IsValid)
            {
                question.Body = HttpUtility.HtmlEncode(question.Body);
                question.CreationDataTime = System.DateTime.Now;
                question.Author = db.User.Find(WebSecurity.CurrentUserId);
                

                if (question.HasExcessBlocks != true)
                {
                    for (int i = 0; i < block.Count; i++)
                    {
                        question.Blocks.Add(new Block { Body = block[i] });
                    }
                }
                else
                {
                    bool[] _Right_answers = new bool[block.Count];
                    for (int i = 0; i < block.Count; i++)
                    {
                        _Right_answers[i] = false;
                    }
                    if (right_block != null)
                    {
                        foreach (var _right in right_block)
                        {
                            int a = Convert.ToInt32(_right);
                            _Right_answers[a] = true;
                        }
                    }
                    for(int i = 0; i< block.Count; i++)
                    {
                        question.Excess_Blocks.Add(new Excess_Block { Body = block[i], IsRight = _Right_answers[i] });
                    }
                }

                TempData["message_success"] = "Вопрос успешно создан!";
                db.Questions.Add(question);
                // ans_cnt.AnswCount = question.PossibleAnswers.Count;
                db.SaveChanges();
                return RedirectToAction("CreateQuestionAndTests", "Tests");
                
            }


            return View(question);

        }
        public ActionResult IndexBlock(Question quest)
        {
            int id = quest.ID;
            Question question = db.Questions.First(q => q.ID == id);
            //IEnumerable<QuestionsIndex> query = (from q in db.Questions

            //                                     select new QuestionsIndex { ID = q.ID, Title = q.Title, Body = q.Body, BlockBody = q.Blocks.Select(ids => ids.Body) }).ToList();

            //QuestionsIndex querylast= query.Last();

            //return View(querylast);
            return View(question);

        }

        //
        // GET: /Question/Edit/5
        [HttpGet]
        public ActionResult Edit(int id = 0)
        {
            if (db.Structures.ToList().Count() != 0) ViewBag.structs = db.Structures.ToList();
            Question question = db.Questions.Find(id);
            ViewBag.thems = db.Themes.Where(x => x.Structure.Id == question.Theme.Structure.Id).ToList();
            var tht = ViewBag.thems;
            question.Body = HttpUtility.HtmlDecode(question.Body);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        //Changing blocks
        [HttpPost]
        public void Edit_Blocks(List<string> blocks, int id, List<bool> right_block)
        {
            Question qq = db.Questions.First(y => y.ID == id);
            if (right_block == null)
            {

                for (int i = 0; i < blocks.Count; i++)
                {
                    qq.Blocks.ElementAt(i).Body = blocks[i];
                    //question.Blocks.ElementAt(i).Body = blocks[i];
                }
                //db.Entry(qq.Blocks).State = EntityState.Modified;
                
            }
            else
            {
                for (int i = 0; i < blocks.Count; i++)
                {
                    qq.Excess_Blocks.ElementAt(i).Body = blocks[i];
                    qq.Excess_Blocks.ElementAt(i).IsRight = right_block.ElementAt(i);
                }
            }
            db.SaveChanges();
            
        }
        // POST: /Questions/Edit/

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit(Question q, int ID, int theme_id)
        {
            var question = db.Questions.Find(ID);
            question.Body = q.Body;
            if (ModelState.IsValid)
             {
                 if (db.Structures.ToList().Count() != 0) ViewBag.structs = db.Structures.ToList();
                 var th = db.Themes.Find(theme_id);
                 question.Title = th.Name;
                 question.Theme = th;
                 //db.Entry(question.Theme).State = EntityState.Modified;
                 if (question.CorrectAnswer != null)
                 {
                     
                     question.CorrectAnswer.QuestionID = question.ID;
                     db.Entry(question.CorrectAnswer).State = EntityState.Modified;
                 }
                
                 
                 
                 question.Body = HttpUtility.HtmlEncode(question.Body);
                 
            question.CreationDataTime = System.DateTime.Now;
            
            db.Entry(question).State = EntityState.Modified;
            TempData["message_success"] = "Вопрос изменен!";
            db.SaveChanges();
            return RedirectToAction("Create", "Tests");
             }
            return View(question);
        }

        //Удаление картинок с помощью корзины
        [HttpPost]
        public void Delete_image(List<int> ids)
        {
            foreach (var id in ids)
            {
                Image image = db.Images.Find(id);
                db.Images.Remove(image);
                db.SaveChanges();
            }
            //return Json("success");
        }


        //
        // GET: /Question/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        //
        // POST: /Questions/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Question question = db.Questions.Find(id);
            TempData["message_success"] = "Вопрос удален!";
            db.Questions.Remove(question);
            db.SaveChanges();
            return RedirectToAction("Create", "Tests");
        }
        [HttpPost]
        public void Delete_several_questions(List<int> ids)
        {
            foreach (var id in ids)
            {
                Question quest = db.Questions.Find(id);
                db.Questions.Remove(quest);
            }
            db.SaveChanges();
            if (ids.Count > 1)
            {
                TempData["message_success"] = "Вопросы удалены!";
            }
            else
            {
                TempData["message_success"] = "Вопрос удален!";
            }
            //return RedirectToAction("Create", "Tests");
        }
    }
}
