﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSOK.Core;
using CSOK.Models;
using WebMatrix.WebData;

namespace CSOK.Controllers
{
    public class CoursesController : Controller
    {
        private CoursesCore _core;

        public CoursesController()
        {
            _core = new CoursesCore();
        }
        public ActionResult Index()
        {
            throw new Exception("Access Denied");
        }
        #region Teacher
        [Authorize(Roles = "Teacher")]
        public ActionResult Teacher()
        {
            return View(_core.GetTeacherDisciplines(User.Identity.Name));
        }

        [Authorize(Roles = "Teacher")]
        [HttpPost]
        public ActionResult TeacherDiscipline(int id)
        {
            ViewBag.DisciplineId = id;
            return View("pTeacher", _core.GetCurrentDiscipline(id, User.Identity.Name));
        }

        [Authorize(Roles = "Teacher")]
        [HttpPost]
        public JsonResult AddSection(string name, int disciplineId)
        {
            
            return new JsonResult
            {
                Data=new {id = _core.AddSection(User.Identity.Name, disciplineId, name), name = name},
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [Authorize(Roles = "Teacher")]
        [HttpPost]
        public JsonResult RemoveSection(int id)
        {
            _core.RemoveSection(id);
            return new JsonResult{
                Data=new {ok = true},
                JsonRequestBehavior = JsonRequestBehavior.AllowGet};
        }

        [Authorize(Roles = "Teacher")]
        [HttpPost]
        public JsonResult AddTheme(int sectionId, string themeName)
        {
            return new JsonResult
            {
                Data = new { id = _core.AddTheme(sectionId, themeName), name = themeName },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [Authorize(Roles = "Teacher")]
        [HttpPost]
        public JsonResult RemoveTheme(int id)
        {
            _core.RemoveTheme(id);
            return new JsonResult
            {
                Data = new { ok = true },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        #endregion
        #region student
        [Authorize(Roles = "Student")]
        public ActionResult Student()
        {
            var l = User.Identity.Name;
            var q = User.IsInRole("Student");
            return View(_core.GetStudentCoursesList(User.Identity.Name));
        }
        [Authorize(Roles = "Student")]
        public ActionResult StudentDetails(int id)
        {
            ViewBag.CoursName = _core.GetDiscName(id);
            return View(_core.GetCurrentDisciplineStudent(id, User.Identity.Name));
        }
        #endregion
        private void AddUser(MyDbContext _dbContext, RegisterModel model)
        {
            WebSecurity.CreateUserAndAccount(model.UserName, model.Password, new { Email = model.Email });
            WebSecurity.Login(model.UserName, model.Password);
            int id = WebSecurity.GetUserId(model.UserName);
            if (model.Num_of_group == null)
            {
                Teacher teacher = new Teacher();
                teacher.Name = model.Name;
                int num = Convert.ToInt32(model.Num_of_sub_faculty);
                teacher.SubFaculty = _dbContext.SubFaculties.First(); 
                teacher.Post = model.Post;
                teacher.Rank = model.Rank;
                teacher.YOB = model.YOB;
                teacher.UserId = id;
                _dbContext.Teachers.Add(teacher);
                _dbContext.SaveChanges();
            }
        
        }

        public ActionResult TestData2()
        {
            var _dbContext = new MyDbContext();
            try
            {
                AddUser(_dbContext, new RegisterModel
                {
                    UserName = "prepod2",
                    Password = "qwerty123",
                    Email = "mailStudent2@mail.ru",
                    Name = "NamePrepod2",
                    Num_of_sub_faculty = "3",
                    Post = "PostPrepod2",
                    Rank = "RankPrepod2",
                    YOB = new DateTime(2010, 01, 01)
                });
            }
            catch (Exception ex) { }
            _dbContext.SaveChanges();
            _dbContext.Dispose();
            return View();
        }
        public ActionResult TestData()
        {
            var _dbContext = new MyDbContext();

            _dbContext.SaveChanges();
            try
            {
                AddUser(_dbContext, new RegisterModel
                {
                    UserName = "prepod2",
                    Password = "qwerty123",
                    Email = "mailStudent2@mail.ru",
                    Name = "NamePrepod2",
                    Num_of_sub_faculty = "3",
                    Post = "PostPrepod2",
                    Rank = "RankPrepod2",
                    YOB = new DateTime(2010, 01, 01)
                });
            }
            catch (Exception ex){}
            _dbContext.SaveChanges();
            var ourTeacher = _dbContext.Teachers.First(x => x.Name.Equals("NamePrepod2"));
            var d1 = new Discipline
            {
                Name = "disc1"
            };
            var d2 = new Discipline
            {
                Name = "disc2"
            };
            var d3 = new Discipline
            {
                Name = "disc3"
            };
            _dbContext.Disciplines.Add(d1);
            _dbContext.Disciplines.Add(d2);
            _dbContext.Disciplines.Add(d3);
            _dbContext.SaveChanges();
            var group = _dbContext.Groups.First();
            _dbContext.TimeTable.Add(new TimeTable
            {
                aud = "208",
                day = "monday",
                lesson = d1,
                Group = group,
                Teacher = ourTeacher,
                time = "12:00",
                typeless = "lect",
                week = "good"
            });
            _dbContext.TimeTable.Add(new TimeTable
            {
                aud = "208",
                day = "monday",
                lesson = d2,
                Group = group,
                Teacher = ourTeacher,
                time = "12:00",
                typeless = "lect",
                week = "good"
            });
            _dbContext.TimeTable.Add(new TimeTable
            {
                aud = "208",
                day = "monday",
                lesson = d3,
                Group = group,
                Teacher = ourTeacher,
                time = "12:00",
                typeless = "lect",
                week = "good"
            });
            _dbContext.Dispose();
            return View("Inbox");
        }
    }
}
