﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSOK.Models;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Data;
using System.Text;
using System.IO;

namespace CSOK.Controllers
{
    public class TimeTableController : Controller
    {
        //
        // GET: /TimeTable/
        private MyDbContext db = new MyDbContext();

        [Authorize]
        public ActionResult Index()
        {

            if (TempData["message_success"] != null)
            {
                ViewBag.message_success = TempData["message_success"];
            }
            if (TempData["message_fail"] != null)
            {
                ViewBag.message_fail = TempData["message_fail"];
            }

            var tt = db.TimeTable.ToList();
            try
            {
                if (User.IsInRole("Admin"))
                {
                    ViewBag.Role = "~/Views/TimeTable/Menu_Admin.cshtml";
                }
                if (User.IsInRole("Student") || User.IsInRole("Teacher"))
                {
                    ViewBag.Role = "~/Views/TimeTable/Menu_St_and_Tch.cshtml";
                }
                return View(tt);
            }
            catch
            {
                return RedirectToAction("Login", "Account");
            }
        }
        //public ActionResult TimeTableSave()
        //{
        //    return View();
        //}
        [HttpPost]
        public ActionResult TimeTableSave(List<string> column1, List<string> column2, List<string> column3, List<string> column4, List<string> column5, List<string> column6, List<string> column7)
        {
            if (column1 != null)
            {
                for (int i = 0; i < column1.Count; i++)
                {
                    var tableRow = new TimeTable();
                    tableRow.day = column1[i];
                    tableRow.time = column2[i];
                    tableRow.week = column3[i];
                    var d = column4[i];
                    var less = db.Disciplines.First(x => x.Name.ToLower() == d.ToLower());
                    tableRow.lesson = less;
                    tableRow.typeless = column5[i];
                    var t = column6[i];
                    var teach = db.Teachers.First(x => x.Name.ToLower() == t.ToLower());
                    tableRow.Teacher = teach;
                    tableRow.aud = column7[i];
                    db.TimeTable.Add(tableRow);
                }

                db.SaveChanges();
                TempData["message_success"] = "Расписание изменено!";
            }
            return RedirectToAction("Index", "TimeTable");
        }

        //public static bool IsFileLocked(string filename)
        //{
        //    FileStream stream = null;
        //    try
        //    {
        //        stream = System.IO.File.Open(filename, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
        //    }
        //    catch (IOException)
        //    {
        //        return true;
        //    }
        //    finally
        //    {
        //        if (stream != null) stream.Close();
        //    }
        //    return false;
        //}

        [HttpPost]
        public ActionResult UploadTimeTable(HttpPostedFileBase file)
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            DataSet ds = new DataSet();
            if (Request.Files["file"].ContentLength > 0)
            {
                string fileExtension =
                                     System.IO.Path.GetExtension(Request.Files["file"].FileName);

                if (fileExtension == ".xls" || fileExtension == ".xlsx")
                {
                    string fileLocation = Server.MapPath("~/Upload/") + Request.Files["file"].FileName;
                    if (System.IO.File.Exists(fileLocation))
                    {

                        System.IO.File.Delete(fileLocation);
                    }
                    Request.Files["file"].SaveAs(fileLocation);
                    string excelConnectionString = string.Empty;
                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                    fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                        fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                        fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                }
                if (fileExtension.ToString().ToLower().Equals(".xml"))
                {
                    string fileLocation = Server.MapPath("~/Upload/") + Request.Files["FileUpload"].FileName;
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }

                    Request.Files["FileUpload"].SaveAs(fileLocation);
                    XmlTextReader xmlreader = new XmlTextReader(fileLocation);
                    // DataSet ds = new DataSet();
                    ds.ReadXml(xmlreader);
                    xmlreader.Close();
                }
                try
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        //string conn = ConfigurationManager.ConnectionStrings["MyDbContext"].ConnectionString;
                        //SqlConnection con = new SqlConnection(conn);
                        //string query = "Insert into TimeTables(day,time,week,typeless,aud) Values('" +
                        //ds.Tables[0].Rows[i][0].ToString() + "','" + ds.Tables[0].Rows[i][1].ToString() +
                        //"','" + ds.Tables[0].Rows[i][2].ToString() + "','" + ds.Tables[0].Rows[i][3].ToString() + "','" + ds.Tables[0].Rows[i][4].ToString() + "')";
                        //con.Open();
                        //SqlCommand cmd = new SqlCommand(query, con);
                        //cmd.ExecuteNonQuery();
                        //con.Close();
                        var discipline = ds.Tables[0].Rows[i][3].ToString();
                        var less = db.Disciplines.First(x => x.Name.ToLower() == discipline.ToLower());
                        var teacher = ds.Tables[0].Rows[i][5].ToString();
                        var prepod = db.Teachers.First(x => x.Name.ToLower() == teacher.ToLower());
                        var grp = ds.Tables[0].Rows[i][6].ToString();
                        var _group = db.Groups.First(x => x.GroupName.ToLower() == grp.ToLower());
                        TimeTable tt = new TimeTable
                        {
                            day = ds.Tables[0].Rows[i][0].ToString(),
                            time = ds.Tables[0].Rows[i][1].ToString(),
                            week = ds.Tables[0].Rows[i][2].ToString(),
                            lesson = less,
                            typeless = ds.Tables[0].Rows[i][4].ToString(),
                            Teacher = prepod,
                            Group = _group,
                            aud = ds.Tables[0].Rows[i][7].ToString(),
                        };
                        db.TimeTable.Add(tt);
                    }
                    db.SaveChanges();
                }
                catch
                {
                    TempData["message_fail"] = "Ошибка сервера";
                    return RedirectToAction("Index", "TimeTable");
                }
            }
            TempData["message_success"] = "Расписание создано!";
            return RedirectToAction("Index", "TimeTable");
        }

        public ActionResult ClearTimeTable()
        {
            var query = db.TimeTable.Select(x => x);
            db.TimeTable.RemoveRange(query);
            db.SaveChanges();
            TempData["message_success"] = "Расписание удалено!";
            return RedirectToAction("Index", "TimeTable");
        }
    }
}
