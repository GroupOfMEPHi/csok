﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using System.Net;
using System.Net.Mail;
using CSOK.Filters;
using CSOK.Models;

namespace CSOK.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RequestController : Controller
    {
        private MyDbContext db = new MyDbContext();
        //
        // GET: /Request/

        [HttpGet]
        public ActionResult Details(int id)
        {
            //IEnumerable<Request> query1 = db.Request;
            //ViewBag.Request_list = query1;
            IEnumerable<Request> query = (from q in db.Request where (q.Num_of_request == id) select q);
            ViewBag.Request = query;
            return View();
        }
        [HttpGet]
        public ActionResult Index(int id = 1)
        {
            ViewBag.message_success = TempData["request_accept"];
            ViewBag.message_fail = TempData["request_deny"];
            List<Request> query1 = db.Request.ToList();
            ViewBag.Request_list = query1;
            //IEnumerable<Request> query = (from q in db.Request where (q.Num_of_request == id) select q);
            //ViewBag.Request = query;
            return View();
        }

        [HttpPost]
        public ActionResult Index(string submit, Request model, string reason)
        {
            if (submit == "Одобрить")
            {
                if (ModelState.IsValid)
                {
                    TempData["request_accept"] = "Заявка одобрена!";
                    bool b = true;
                    string new_name = model.UserName;
                    while (b)
                    {
                        User check_user = db.User.FirstOrDefault(x => x.UserName.ToLower() == new_name.ToLower());
                        
                        if (check_user == null)
                        {
                            b = false;
                            // Попытка зарегистрировать пользователя
                            WebSecurity.CreateUserAndAccount(new_name, model.Password, new { Email = model.Email });
                        }
                        else
                        {
                            new_name = new_name + "1";
                        }
                    }
                    //WebSecurity.Login(model.UserName, model.Password);
                    int id = WebSecurity.GetUserId(new_name);
                    var roles = (SimpleRoleProvider)Roles.Provider;
                    string UserName = new_name;
                    
                    string UserPassword = model.Password;
                    if (model.Num_of_group == null)
                    {
                        Teacher teacher = new Teacher();
                        teacher.Name = model.Name;
                        int num = Convert.ToInt32(model.Num_of_sub_faculty);
                        SubFaculty sf = (from subf in db.SubFaculties
                                         where subf.Id == num
                                         select subf).First();
                        teacher.SubFaculty = sf;
                        teacher.Post = model.Post;
                        teacher.Rank = model.Rank;
                        teacher.YOB = model.YOB;
                        teacher.UserId = id;
                        db.Teachers.Add(teacher);
                        roles.AddUsersToRoles(new[] { UserName }, new[] { "Teacher" });
                        db.SaveChanges();
                    }
                    else
                    {
                        Student student = new Student();
                        student.Num_of_record_book = model.Num_of_record_book;
                        student.Name = model.Name;
                        student.YOB = model.YOB.Date;
                        //student.Num_of_sub_faculty = Convert.ToInt32(model.Num_of_sub_faculty);
                        Group gr = (from groupp in db.Groups
                                  where groupp.GroupName == model.Num_of_group
                                  select groupp).First();
                        student.Group = gr;
                        student.UserId = id;
                        db.Students.Add(student);
                        roles.AddUsersToRoles(new[] { UserName }, new[] { "Student" });
                        db.SaveChanges();
                    }
                    //отправляется письмо про одобрение
                    var fromAddress = new MailAddress("cyber221@gmail.com", "Кафедра 22");
                    var toAddress = new MailAddress(model.Email);
                    const string fromPassword = "221221221";
                    const string subject = "Ваша заявка одобрена!";
                    string body = "<h2>Вы теперь зарегистрированный пользователь в Системе обучения и контроля знаний!</h2>Для входа на сайт используйте следующие данные:<ul><li>Ваш логин: " + UserName + "" + "</li><li>Ваш пароль: " + UserPassword + "</li></ul>";
                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                    };
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        Body = body,
                        IsBodyHtml = true
                    })
                    {
                        try
                        {
                            smtp.Send(message);
                        }
                        catch
                        {
                            TempData["alert_message"] = "Ошибка сервера";
                            ViewBag.alert_message = TempData["alert_message"];
                            return RedirectToAction("Index", "Request");
                        }
                    }
                }
                
            }
            else
            {
                //отправляется письмо про отклонение
                var fromAddress = new MailAddress("cyber221@gmail.com", "Кафедра 22");
                var toAddress = new MailAddress(model.Email);
                const string fromPassword = "221221221";
                const string subject = "Ваша заявка отклонена!";
                string body = "";
                if (reason == null || reason == "")
                {
                    body = "Ваша заявка была отклонена администратором!";
                }
                else
                {
                    body = "Ваша заявка была отклонена администратором по причине: " + reason;
                }
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    try
                    {
                        smtp.Send(message);
                    }
                    catch
                    {
                        TempData["alert_message"] = "Ошибка сервера";
                        ViewBag.alert_message = TempData["alert_message"];
                        return RedirectToAction("Index", "Request");
                    }
                }
                TempData["request_deny"] = "Заявка отклонена!";
            }
            //удаление из request строки
            
            Request req = db.Request.Find(model.Num_of_request);
            db.Request.Remove(req);
            db.SaveChanges();
            IEnumerable<Request> query1 = db.Request;
            ViewBag.Request_list = query1;
            IEnumerable<Request> query = (from q in db.Request where (q.Num_of_request == 1) select q);
            ViewBag.Request = query;
            return RedirectToAction("Index", "Request");
        }
    }
}