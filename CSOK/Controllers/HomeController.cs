﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CSOK.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.hello = TempData["hello"]; 
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            try
            {
                if (User.IsInRole("Admin"))
                {
                    ViewBag.Role = "~/Views/Home/Menu_admin.cshtml";
                }
                if (User.IsInRole("Student"))
                {
                    ViewBag.Role = "~/Views/Home/Menu_student.cshtml";
                }
                if (User.IsInRole("Teacher"))
                {
                    ViewBag.Role = "~/Views/Home/Menu_teacher.cshtml";
                }
                return View();
            }
            catch {
               return RedirectToAction("Login","Account");
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
