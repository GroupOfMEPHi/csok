﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSOK.Models;
using System.IO;
using System.Data.Entity;
using System.Data;
using WebMatrix.WebData;

namespace CSOK.Controllers
{
    public class EntrieE
    {
        public int id { get; set; }
        public string text { get; set; }
        public string type { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public string NameA { get; set; }
    }
    [Authorize]
    public class EventController : Controller
    {
        private MyDbContext db = new MyDbContext();
   
        public ActionResult Index()
        {
            //IEnumerable<CalendarEvent> query = (from q in db.CalendarEvents select new CalendarEvent {id=q.id, text=q.text, start_date=q.start_date, end_date=q.end_date  });
            ViewBag.message_success = TempData["message_success"];
            var Names =
                (from nameA in db.Admin select new { ID = nameA.UserID, Name = nameA.Name }).Union(from nameT in db.Teachers select new { ID = nameT.UserId, Name = nameT.Name });

            var mostRecentEntries = (from entry in db.CalendarEvents
                                     join usr in Names on entry.Author.UserId equals usr.ID
                                     select new EntrieE() { id = entry.id, start_date = entry.start_date, end_date = entry.end_date, type = entry.type, NameA = usr.Name, text = entry.text }).Distinct().OrderByDescending(x => x.start_date).ToList();
            ViewBag.Entries = mostRecentEntries;
            try
            {
                if (User.IsInRole("Admin") || User.IsInRole("Teacher"))
                {
                    ViewBag.Role = "~/Views/Event/Menu_Writer.cshtml";
                }
                return View(db.CalendarEvents.ToList());
            }
            catch
            {
                return RedirectToAction("Login", "Account");
            }
        }


        public ActionResult Details(int id)
        {
            var user = (from us in db.CalendarEvents
                        where (us.id == id)
                        select us).First();
            var Names =
                (from nameA in db.Admin select new { ID = nameA.UserID, Name = nameA.Name }).Union(from nameT in db.Teachers select new { ID = nameT.UserId, Name = nameT.Name });
            
            var entry = (from res in db.CalendarEvents
                         join usr in Names on res.Author.UserId equals usr.ID
                         where (res.id == id)
                         select new EntrieE() { id = res.id, start_date = res.start_date, end_date = res.end_date, type = res.type, NameA = usr.Name, text = res.text }).Distinct().ToList().First();
            try
            {
                if (User.IsInRole("Admin"))
                {
                    ViewBag.Role = "~/Views/Event/Menu_Admin.cshtml";
                }
                if ((User.IsInRole("Teacher")) && (WebSecurity.GetUserId(User.Identity.Name) == user.Author.UserId))
                {
                    ViewBag.Role = "~/Views/Event/Menu_Admin.cshtml";
                }
                return View(entry);
            }
            catch
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Teacher")]
        public ActionResult Create_Eve()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        public ActionResult Create_Eve(CalendarEvent c_eve, string choice)
        {
            c_eve.type = choice;
            int id = WebSecurity.GetUserId(User.Identity.Name);
            User usr = (from Userr in db.User
                        where Userr.UserId == id
                        select Userr).First();
            c_eve.Author = usr;
            c_eve = db.CalendarEvents.Add(c_eve);
            TempData["message_success"] = "Событие создано!";
            db.SaveChanges();
            return RedirectToAction("Index", "Event");
        }


        [Authorize(Roles = "Admin,Teacher")]
        public ActionResult Edit(int id = 0)
        {
            CalendarEvent c_eve = db.CalendarEvents.Find(id);
            if (c_eve == null)
            {
                return HttpNotFound();
            }
            return View(c_eve);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        public ActionResult Edit(CalendarEvent calendarevent)
        {
            db.Entry(calendarevent).State = EntityState.Modified;
            db.SaveChanges();
            TempData["message_success"] = "Событие изменено!";
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Teacher")]
        public ActionResult Delete(int id = 0)
        {
            CalendarEvent c_eve = db.CalendarEvents.Find(id);
            if (c_eve == null)
            {
                return HttpNotFound();
            }
            return View(c_eve);
        }

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin,Teacher")]
        public ActionResult DeleteConfirmed(int id)
        {
            CalendarEvent calendarevent = db.CalendarEvents.Find(id);
            db.CalendarEvents.Remove(calendarevent);
            db.SaveChanges();
            TempData["message_success"] = "Событие удалено!";
            return RedirectToAction("Index");
        }

    }
}
