﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSOK.Models;
using System.IO;
using System.Data.Entity;
using System.Data;

namespace CSOK.Controllers
{
    public class CalendarController : Controller
    {

        private MyDbContext db = new MyDbContext();
        //
        // GET: /Calendar/
        [Authorize]
        public ActionResult Index()
        {
            List<CalendarEvent> cal_eves = db.CalendarEvents.ToList();
            return View(cal_eves);
        }
        [HttpGet]
        public JsonResult Get_Events()
        {
            List<CalendarEvent> cal_eves = db.CalendarEvents.ToList();
            return Json(cal_eves, JsonRequestBehavior.AllowGet);
        }

    }
}
