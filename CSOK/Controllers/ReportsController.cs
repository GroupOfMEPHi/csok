﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSOK.Models;
using WebMatrix.WebData;
using SimpleChart = System.Web.Helpers;
using System.Data.Entity;

namespace CSOK.Controllers
{
    [Authorize(Roles = "Teacher, Student, Admin")]
    public class ReportsController : Controller
    {
        //
        // GET: /Reports/
        private MyDbContext db = new MyDbContext();

        [Authorize(Roles = "Teacher, Student")]
        public ActionResult Index()
        {
            if (User.IsInRole("Student"))
            {
                ViewBag.Role = "~/Views/Reports/Student_Reports.cshtml";
            }
            if (User.IsInRole("Teacher"))
            {
                ViewBag.Role = "~/Views/Reports/Teacher_Reports.cshtml";
            }
            ViewBag.message = TempData["message_success"];
            return View();
        }

        public ActionResult StudentTestsResults() //отчет для студентов "Результаты тестов"
        {
            var model = new GroupDisciplineTestDropdown();
            List<Discipline> courses = new List<Discipline>();
            model.AvailableDisciplines = new SelectList(GetDisciplines(), "Id", "Name");
            courses = GetDisciplines().ToList();
            if (courses.Count() != 0) ViewBag.courses = courses;
            return View(model);
        }

        public IQueryable<Discipline> GetDisciplines() //получение списка всех курсов для данного студента или преподавателя
        {
            List<Discipline> disc = new List<Discipline>();
            // disc.Add(new Discipline { Id = 0, Name = "Выберите предмет..." });
            if (User.IsInRole("Student"))
            {
                var disciplines = (from timetab in db.TimeTable
                                   join student in db.Students on timetab.Group.GroupId equals student.Group.GroupId
                                   where student.UserId == WebSecurity.CurrentUserId
                                   select timetab.lesson).Distinct();
                foreach (var d in disciplines)
                {
                    disc.Add(d);
                }
            }
            else if (User.IsInRole("Teacher"))
            {
                var disciplines = (from timetab in db.TimeTable
                                   where timetab.Teacher.UserId == WebSecurity.CurrentUserId
                                   select timetab.lesson).Distinct();
                foreach (var d in disciplines)
                {
                    disc.Add(d);
                }
            }
            return disc.AsQueryable();
        }

        public IQueryable<Discipline> GetDisciplines(int groupId) //получение списка курсов для данной группы
        {
            List<Discipline> disc = new List<Discipline>();
            //disc.Add(new Discipline { Id = 0, Name = "Выберите предмет..." });
            var disciplines = (from timetab in db.TimeTable
                               where timetab.Teacher.UserId == WebSecurity.CurrentUserId
                               & timetab.Group.GroupId == groupId
                               select timetab.lesson).Distinct();
            foreach (var d in disciplines)
            {
                disc.Add(d);
            }
            return disc.AsQueryable();
        }

        [HttpPost]
        public ActionResult GetCourses(int groupId)
        {
            var disciplines = GetDisciplines(groupId);

            return Json(new SelectList(disciplines, "Id", "Name"));
        }

        public IQueryable<TestItem> GetTestItems(int disciplineId)//получение списка всех тестов, относящихся к данному курсу
        {
            List<TestItem> tests = new List<TestItem>();
            var test1 = from testitem in db.Testitems
                        where testitem.Discipline.Id == disciplineId
                        select testitem;
            foreach (var t in test1)
            {
                tests.Add(t);
            }
            return tests.AsQueryable();
        }

        [HttpPost]
        public ActionResult GetTests(int disciplineId)
        {
            var tests = GetTestItems(disciplineId);

            return Json(new SelectList(tests, "ID", "Subject"));
        }

        [HttpPost]
        public ActionResult GetStudentTestResult(int testItemId)
        {
            List<Results> results = new List<Results>();
            string timePass = "";
            long DatetimeMinTimeTicks = (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).Ticks;
            string spTime;
            var result = from testItemAns in db.TestItemAnswers
                         where testItemAns.Author.UserId == WebSecurity.CurrentUserId
                         & testItemAns.TestItem.ID == testItemId
                         select testItemAns;
            foreach (var r in result)
            {
                /*if (r.SpentTime == null)
                    spTime = "-";
                else
                    spTime = r.SpentTime;*/
                if (r.TestItem.Time == 0)
                { timePass = "Нет ограничения"; }
                else { timePass = (r.EndtDateTime - r.StartDateTime).Hours.ToString() + " ч." + (r.EndtDateTime - r.StartDateTime).Minutes.ToString() + " мин." + (r.EndtDateTime - r.StartDateTime).Seconds.ToString() + " сек."; }
                results.Add(new Results
                {
                    TestName = r.Subject,
                    DatePass = (long)((r.StartDateTime.ToUniversalTime().Ticks - DatetimeMinTimeTicks) / 10000),//чтобы js нормально воспринимал дату
                    TimePass = timePass,
                    NumberTry = r.CurrentAttempt,
                    QuestCount = r.TestItem.Questions.Count(),
                    RightCount = r.TestScore,
                    Result = ((float)r.TestScore / (float)r.TestItem.Questions.Count()) * 100
                });
            }
            return Json(new { TestResults = results }, JsonRequestBehavior.AllowGet);
        }

        public List<TestProgress> GetStudentAllCourseProgress() //получение данных Курс+%усвоения для отчета студентов "Общая успеваемость"
        {
            List<TestProgress> progress = new List<TestProgress>();
            List<float> testProgress = new List<float>();
            var disciplines = GetDisciplines().ToList();
            //disciplines.RemoveAt(0);
            foreach (var course in disciplines)
            {
                var tests = GetTestItems(course.Id);
                foreach (var tst in tests)
                {
                    TestItem testitemm = db.Testitems.First(q => q.ID == tst.ID);
                    List<TestItemAnswer> t = testitemm.TestItemAnswers.Where(q => q.Author.UserId == WebSecurity.CurrentUserId).ToList();
                    int res = 0;
                    if (t.Count() != 0)
                    {
                        if (testitemm.Estimation.EstID == 1)
                        {
                            res = t.Max(e => e.TestScore);
                        }
                        else if (testitemm.Estimation.EstID == 2) { res = t.Sum(e => e.TestScore) / testitemm.Attempts; }
                        else if (testitemm.Estimation.EstID == 3) { res = t.First().TestScore; }
                        else if (testitemm.Estimation.EstID == 4) { res = t.Last().TestScore; }
                        testProgress.Add((float)res / (float)testitemm.Questions.Count() * 100);
                    }
                }
                if (testProgress.Count() != 0)
                {
                    progress.Add(new TestProgress { Name = course.Name, Progress = (float)Math.Round((double)testProgress.Average(), 2) });
                }
                testProgress.Clear();
            }
            return progress;
        }

        public List<float> GetStudentAllCourseProgress(int studentId) //получение %усвоения по всем курсам для заданного студента
        {
            List<float> progress = new List<float>();
            List<float> testProgress = new List<float>();
            var disciplines = GetDisciplines().ToList();
            //disciplines.RemoveAt(0);
            foreach (var course in disciplines)
            {
                var tests = GetTestItems(course.Id);
                foreach (var tst in tests)
                {
                    TestItem testitemm = db.Testitems.First(q => q.ID == tst.ID);
                    List<TestItemAnswer> t = testitemm.TestItemAnswers.Where(q => q.Author.UserId == studentId).ToList();
                    int res = 0;
                    if (t.Count() != 0)
                    {
                        if (testitemm.Estimation.EstID == 1)
                        {
                            res = t.Max(e => e.TestScore);
                        }
                        else if (testitemm.Estimation.EstID == 2) { res = t.Sum(e => e.TestScore) / testitemm.Attempts; }
                        else if (testitemm.Estimation.EstID == 3) { res = t.First().TestScore; }
                        else if (testitemm.Estimation.EstID == 4) { res = t.Last().TestScore; }
                        testProgress.Add((float)res / (float)testitemm.Questions.Count() * 100);
                    }
                }
                if (testProgress.Count() != 0)
                {
                    progress.Add((float)Math.Round((double)testProgress.Average(), 2));
                }
                testProgress.Clear();
            }
            return progress;
        }

        public ActionResult StudentAllCourseProgress() //отчет для студентов "Общая успеваемость" (таблица)
        {
            List<TestProgress> progress = new List<TestProgress>();
            progress = GetStudentAllCourseProgress();
            return View(progress);
        }

        public ActionResult StudentCourseProgress()//отчет для студентов "Успеваемость по курсу" - график
        {
            var model = new GroupDisciplineTestDropdown();
            model.AvailableDisciplines = new SelectList(GetDisciplines(), "Id", "Name");
            return View(model);
        }

        [HttpPost]
        public ActionResult GetCourseProgressToChart(int disciplineId)//возвращает частичное представление - график успеваемости
        {
            List<TestItemAnswer> t = new List<TestItemAnswer>();
            List<TestProgress> progress = new List<TestProgress>();
            //string title = "";
            var tests = GetTestItems(disciplineId);
            foreach (var tst in tests)
            {
                TestItem testitemm = db.Testitems.First(q => q.ID == tst.ID);
                if (User.IsInRole("Student"))
                {
                    t = testitemm.TestItemAnswers.Where(q => q.Author.UserId == WebSecurity.CurrentUserId).ToList();
                }
                else if (User.IsInRole("Teacher"))
                {
                    t = testitemm.TestItemAnswers.ToList();
                }
                int res = 0;
                if (t.Count() != 0)
                {
                    if (testitemm.Estimation.EstID == 1)
                    {
                        res = t.Max(e => e.TestScore);
                    }
                    else if (testitemm.Estimation.EstID == 2) { res = t.Sum(e => e.TestScore) / testitemm.Attempts; }
                    else if (testitemm.Estimation.EstID == 3) { res = t.First().TestScore; }
                    else if (testitemm.Estimation.EstID == 4) { res = t.Last().TestScore; }
                    progress.Add(new TestProgress { Name = testitemm.Subject, Progress = (float)res / (float)testitemm.Questions.Count() * 100, Discipline = db.Disciplines.First(q => q.Id == disciplineId) });
                    //testProgress.Add((float)res / (float)testitemm.Questions.Count() * 100);
                    //testNames.Add(testitemm.Subject);
                }
            }
            ViewBag.disciplineId = disciplineId;
            return View("StudentCourseProgressPartial", progress);
        }


        public ActionResult CourseProgressChart(int disciplineId)
        {
            List<float> testProgress = new List<float>();
            List<string> testNames = new List<string>();
            List<TestItemAnswer> t = new List<TestItemAnswer>();
            string title = "";
            var tests = GetTestItems(disciplineId);
            foreach (var tst in tests)
            {
                TestItem testitemm = db.Testitems.First(q => q.ID == tst.ID);
                if (User.IsInRole("Student"))
                {
                    t = testitemm.TestItemAnswers.Where(q => q.Author.UserId == WebSecurity.CurrentUserId).ToList();
                }
                else if (User.IsInRole("Teacher"))
                {
                    List<int> student_id = (from st in db.Students
                                               select st.UserId).ToList();
                    //отбираем только студентов
                    t = testitemm.TestItemAnswers.ToList().Where(m=> student_id.Contains(m.Author.UserId)).ToList();
                }
                float res = 0;
                if (t.Count() != 0)
                {
                    if (User.IsInRole("Student"))
                    {
                        if (testitemm.Estimation.EstID == 1)
                        {
                            res = t.Max(e => e.TestScore);
                        }
                        else if (testitemm.Estimation.EstID == 2) { res = t.Sum(e => e.TestScore) / testitemm.Attempts; }
                        else if (testitemm.Estimation.EstID == 3) { res = t.First().TestScore; }
                        else if (testitemm.Estimation.EstID == 4) { res = t.Last().TestScore; }
                    }
                    if (User.IsInRole("Teacher"))
                    {
                        res = (float)t.Sum(m => m.TestScore) / (float)t.Count();
                    }
                    testProgress.Add((float)res / (float)testitemm.Questions.Count() * 100);
                    testNames.Add(testitemm.Subject);
                }
            }
            /*foreach (var pr in progress)
            {
                testNames.Add(pr.Name);
                testProgress.Add(pr.Progress);
            }
            if (User.IsInRole("Student"))
            {
                title = "Успеваемость по курсу " + progress.First().Name + " студента " + db.Students.First(s => s.UserId == WebSecurity.CurrentUserId).Name;
            }
            else if (User.IsInRole("Teacher"))
            {
                title = "Успеваемость по курсу " + progress.First().Name;
            }*/
            //if (testNames.Count() != 0 || testProgress.Count() != 0)
            //{
            var chart = new SimpleChart.Chart(width: 1000, height: 500)
                  .AddTitle(title)
                  .AddSeries(
                         name: "Процент усвоения по тесту",
                         chartType: "Column",
                         xValue: testNames.ToArray(),
                         yValues: testProgress.ToArray())
                  .SetYAxis(
                         min:0,
                         max: 100)
                  .AddLegend()
                  .Write();
            return null;
        }

        public ActionResult StudentAllCourseProgressChart()
        {
            List<TestProgress> progress = new List<TestProgress>();
            progress = GetStudentAllCourseProgress();
            return View(progress);
        }

        public ActionResult GetStudentAllCourseProgressChart()
        {
            List<TestProgress> progress = new List<TestProgress>();
            List<string> courseNames = new List<string>();
            List<float> courseProgress = new List<float>();
            progress = GetStudentAllCourseProgress();
            foreach (var progr in progress)
            {
                courseNames.Add(progr.Name);
                courseProgress.Add(progr.Progress);
            }
            var chart = new SimpleChart.Chart(width: 1000, height: 500)
                  .AddTitle("Успеваемость по всем курсам студента " + db.Students.First(s => s.UserId == WebSecurity.CurrentUserId).Name)
                  .AddSeries(
                         name: "Процент усвоения по курсу",
                         chartType: "Column",
                         xValue: courseNames.ToArray(),
                         yValues: courseProgress.ToArray())
                  .SetYAxis(
                         min:0,
                         max: 100)
                  .AddLegend()

                  .Write();
            return null;
        }

        public ActionResult TeacherTestsResultsStudent()//отчет для преподавателей "Результаты учащихся по каждому тесту"
        {
            var model = new GroupDisciplineTestDropdown();
            model.AvailableGroups = new SelectList(GetGroups(), "GroupId", "GroupName");
            return View(model);
        }

        public ActionResult GetTeacherTestsResultsStudent(int testItemId, int groupId)
        {
            List<Results> results = new List<Results>();
            var result = from testItemAns in db.TestItemAnswers
                         join student in db.Students on testItemAns.Author.UserId equals student.UserId
                         where testItemAns.TestItem.ID == testItemId
                         & student.Group.GroupId == groupId
                         select testItemAns;
            foreach (var r in result)
            {
                results.Add(new Results
                {
                    StudentName = db.Students.Find(r.Author.UserId).Name,
                    Result = ((float)r.TestScore / (float)r.TestItem.Questions.Count()) * 100,
                    NumberTry = r.CurrentAttempt,
                    MaxNumberTry = r.TestItem.Attempts,
                    Bonus = r.ID //это не бонус, но это поле пока не используется, поэтому я в него кладу ид попытки, чтобы ссылку во вью прописать на русную проверку
                });
            }
            return Json(new { TestResults = results }, JsonRequestBehavior.AllowGet);
        }

        public IQueryable<Group> GetGroups()// получение списка групп для текущего преподавателя
        {
            List<Group> groups = new List<Group>();
            //groups.Add(new Group { GroupId = 0, GroupName = "Выберите группу..." });
            var gr = (from timetab in db.TimeTable
                      where timetab.Teacher.UserId == WebSecurity.CurrentUserId
                      select timetab.Group).Distinct();
            foreach (var g in gr)
            {
                groups.Add(g);
            }
            return groups.AsQueryable();
        }

        public ActionResult TeacherAllTestsResults()
        {
            List<TestItem> tests = new List<TestItem>();
            List<Results> results = new List<Results>();
            List<TestItemAnswer> answers = new List<TestItemAnswer>();
            float res = 0;
            var disciplines = GetDisciplines();
            foreach (var disc in disciplines)
            {
                tests.AddRange(GetTestItems(disc.Id));
            }
            foreach (var t in tests)
            {
                var tstans = from testItemAns in db.TestItemAnswers
                             join student in db.Students on testItemAns.Author.UserId equals student.UserId
                             join timeTable in db.TimeTable on student.Group.GroupId equals timeTable.Group.GroupId
                             where testItemAns.TestItem.ID == t.ID
                             & timeTable.Teacher.UserId == WebSecurity.CurrentUserId
                             select testItemAns;
                if (tstans.Count() != 0)
                {
                    /*if (t.Estimation.EstID == 1)
                    {
                        res = tstans.Max(e => e.TestScore);
                    }
                    else if (t.Estimation.EstID == 2) {*/
                    res = (float)tstans.Average(e => e.TestScore) / (float)t.Questions.Count() * 100; /*}
                    else if (t.Estimation.EstID == 3) { res = tstans.First().TestScore; }
                    else if (t.Estimation.EstID == 4) { res = tstans.Last().TestScore; }
                    res = (float)res / (float)t.Questions.Count() * 100;*/
                    var averTry = from ta in tstans
                                  group ta.CurrentAttempt by ta.Author into currAtt
                                  select new { attemptMax = currAtt.Max() };

                    results.Add(new Results
                    {
                        TestName = t.Subject,
                        Result = (float)Math.Round((double)res, 2),
                        AveragePoint = (float)tstans.Average(e => e.TestScore),
                        QuestCount = t.Questions.Count(),
                        AverageNumberTry = (int)averTry.Average(e => e.attemptMax),
                        Estimation = t.Estimation.Name,
                        TestItem = t
                    });
                }
            }
            return View(results);
        }

        public ActionResult TeacherTestsResults() //отчет для преподавателей "Результаты по каждому тесту"
        {
            var model = new GroupDisciplineTestDropdown();
            model.AvailableDisciplines = new SelectList(GetDisciplines(), "Id", "Name");
            return View(model);
        }

        [HttpPost]
        public ActionResult GetTeacherTestResult(int testItemId)
        {
            List<Results> results = new List<Results>();
            List<TestItemAnswer> difTry = new List<TestItemAnswer>();
            long DatetimeMinTimeTicks = (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).Ticks;
            List<float> rightAns_Question = new List<float>();
            var tstans = from testItemAns in db.TestItemAnswers
                         join student in db.Students on testItemAns.Author.UserId equals student.UserId
                         join timeTable in db.TimeTable on student.Group.GroupId equals timeTable.Group.GroupId
                         where testItemAns.TestItem.ID == testItemId
                         & timeTable.Teacher.UserId == WebSecurity.CurrentUserId
                         select testItemAns;
            if (tstans.Count() != 0)
            {
                var quest = tstans.First().Questions;
                foreach (var q in quest)
                {
                    rightAns_Question = RightAns_Question(q, testItemId);
                    results.Add(new Results
                    {
                        QuestionId = q.ID,
                        QuestionSubject = q.Theme.Name,
                        QuestionBody = q.Body,
                        RightCount = (int)rightAns_Question[0],
                        WrongCount = (int)rightAns_Question[1],
                        Result = rightAns_Question[2],
                    });
                }
            }
            return Json(new { TestResults = results }, JsonRequestBehavior.AllowGet);
        }

        public List<float> RightAns_Question(Question question, int testItemId) //метод для определения кол-ва верных ответов в разрезе вопроса теста
        {
            /*Возвращает 1) кол-во верных ответов, 2) кол-во неверных оветов, 3) процент верных ответов*/
            List<float> result = new List<float>();
            int rigth = 0;
            int wrong = 0;
            float res = 0;
            var answers = (from testres in db.TestResults
                           where testres.Question.ID == question.ID
                           & testres.TestItemAnswer.TestItem.ID == testItemId
                           select testres).ToList();
            //rightAnsCount = answers.Where(m=>m.IsRight==true).Count();
            if (answers.Count() != 0)
            {
                rigth = answers.Where(m => m.IsRight == true).Count();
                wrong = answers.Count() - answers.Where(m => m.IsRight == true).Count();
                res = (float)rigth / (float)answers.Count();
                result.Add(answers.Where(m => m.IsRight == true).Count());
                result.Add(answers.Count() - answers.Where(m => m.IsRight == true).Count());
                result.Add(((float)answers.Where(m => m.IsRight == true).Count() / (float)answers.Count()) * 100);
            }
            else
            {
                result.Add(0);
                result.Add(0);
                result.Add(0);
            }
            return result;
        }

        [HttpPost]
        public ActionResult GetTeacherTestDates(int testItemId)
        {
            List<Results> results = new List<Results>();
            long DatetimeMinTimeTicks = (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).Ticks;
            var d_o = db.Testitems.First(m => m.ID == testItemId).OpeningDateTime;
            var d_o_Un = d_o.ToUniversalTime();
            var d_o_Ticks = d_o_Un.Ticks;
            results.Add(new Results
            {
                DateOpen = (db.Testitems.First(m => m.ID == testItemId).OpeningDateTime.ToUniversalTime().Ticks - DatetimeMinTimeTicks) / 10000,
                DateClose = (db.Testitems.First(m => m.ID == testItemId).ClosingDateTime.ToUniversalTime().Ticks - DatetimeMinTimeTicks) / 10000,
                AveragePoint = (float)(from ta in
                                           (from testItemAns in db.TestItemAnswers
                                            join student in db.Students on testItemAns.Author.UserId equals student.UserId
                                            join timeTable in db.TimeTable on student.Group.GroupId equals timeTable.Group.GroupId
                                            where testItemAns.TestItem.ID == testItemId
                                            & timeTable.Teacher.UserId == WebSecurity.CurrentUserId
                                            select testItemAns)
                                       select ta.TestScore).Average(),
                NumberTry = db.Testitems.Find(testItemId).TestItemAnswers.Count()
            });
            return Json(new { TestResults = results }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TeacherAllTestsResultsChart()
        {
            var model = new GroupDisciplineTestDropdown();
            model.AvailableDisciplines = new SelectList(GetDisciplines(), "Id", "Name");
            return View(model);
        }

        [HttpPost]
        public ActionResult GetTeacherAllTestsResultsChart(int disciplineId)//возвращает частичное представление
        {
            List<TestItemAnswer> t = new List<TestItemAnswer>();
            List<TestProgress> progress = new List<TestProgress>();
            //string title = "";
            var tests = GetTestItems(disciplineId);
            foreach (var tst in tests)
            {
                TestItem testitemm = db.Testitems.First(q => q.ID == tst.ID);
                if (User.IsInRole("Student"))
                {
                    t = testitemm.TestItemAnswers.Where(q => q.Author.UserId == WebSecurity.CurrentUserId).ToList();
                }
                else if (User.IsInRole("Teacher"))
                {
                    t = testitemm.TestItemAnswers.ToList();
                }
                int res = 0;
                if (t.Count() != 0)
                {
                    if (testitemm.Estimation.EstID == 1)
                    {
                        res = t.Max(e => e.TestScore);
                    }
                    else if (testitemm.Estimation.EstID == 2) { res = t.Sum(e => e.TestScore) / testitemm.Attempts; }
                    else if (testitemm.Estimation.EstID == 3) { res = t.First().TestScore; }
                    else if (testitemm.Estimation.EstID == 4) { res = t.Last().TestScore; }
                    progress.Add(new TestProgress { Name = testitemm.Subject, Progress = (float)res / (float)testitemm.Questions.Count() * 100, Discipline = db.Disciplines.First(q => q.Id == disciplineId) });
                    /*testProgress.Add((float)res / (float)testitemm.Questions.Count() * 100);
                    testNames.Add(testitemm.Subject);*/
                }
            }
            ViewBag.disciplineId = disciplineId;
            return View("TeacherAllTestsResultsChartPartial", progress);
        }

        public ActionResult TeacherThemesProgressChart()
        {
            var model = new GroupDisciplineTestDropdown();
            model.AvailableDisciplines = new SelectList(GetDisciplines(), "Id", "Name");
            return View(model);
        }

        [HttpPost]
        public ActionResult GetTeacherThemesProgressChart(int testItemId)//возвращает частичное представление
        {
            List<float> tempProgress = new List<float>();
            var testItem = db.Testitems.First(q => q.ID == testItemId); //получили тест
            //темы
            var themes = (from quest in testItem.Questions
                          select quest.Theme).Distinct().ToList();
            foreach (var th in themes)
            {
                foreach (var quest in testItem.Questions.Where(m => m.Theme.Id == th.Id))
                {

                    tempProgress.Add(RightAns_Question(quest, testItemId)[2]);   //gca здесь приходит пустой объект

                }
            }
            ViewBag.testItemId = testItemId;
            return View("TeacherThemesProgressChartPartial", tempProgress);
        }

        public ActionResult ThemesProgressChart(int testItemId)
        {
            List<float> themeProgress = new List<float>();
            List<string> themeNames = new List<string>();
            List<float> tempProgress = new List<float>();
            var testItem = db.Testitems.First(q => q.ID == testItemId); //получили тест
            //темы
            var themes = (from quest in testItem.Questions
                          select quest.Theme).Distinct().ToList();
            foreach (var th in themes)
            {
                foreach (var quest in testItem.Questions.Where(m => m.Theme.Id == th.Id))
                {

                    tempProgress.Add(RightAns_Question(quest, testItemId)[2]);   //gca здесь приходит пустой объект

                }
                themeProgress.Add(tempProgress.Average());
                themeNames.Add(th.Name);
                tempProgress.Clear();
            }
            if (themeProgress.Where(m => m != 0).Count() != 0)
            {
                var chart = new SimpleChart.Chart(width: 1000, height: 500)
                      .AddTitle("График усвоения тем по тесту " + testItem.Subject)
                      .AddSeries(
                             name: "Процент усвоения по теме",
                             chartType: "Column",
                             xValue: themeNames.ToArray(),
                             yValues: themeProgress.ToArray())
                      .SetYAxis(
                             min:0,
                             max: 100)
                      .AddLegend()

                      .Write();
                TempData["IsShowChart"] = "1";
            }
            else
            {
                TempData["IsShowChart"] = "0";
            }
            return null;
        }

        public ActionResult TeacherStudentsRating()
        {
            List<TestProgress> rating = new List<TestProgress>();
            List<TestProgress> ratingSort = new List<TestProgress>();
            List<Group> groups = GetGroups().ToList();
            List<float> progresss = new List<float>();
            //groups.RemoveAt(0);
            if (groups.Count() != 0)
            {
                foreach (var gr in groups)
                {
                    if (gr.Students.Count() != 0)
                    {
                        foreach (var st in gr.Students)
                        {
                            progresss = GetStudentAllCourseProgress(st.UserId);
                            if (progresss.Count() != 0)
                            {
                                rating.Add(new TestProgress { Name = st.Name, Progress = progresss.Average(), Group = gr.GroupName });
                            }
                        }
                    }
                }
            }
            ratingSort = rating.OrderByDescending(m=>m.Progress).ToList();
            //rating.Sort();
            return View(ratingSort);
        }


        /*========================================================

         Блок Егора (ручная проверка теста)

        ========================================================*/
        public ActionResult TeacherTestListNew()
        {
            return View(db.Testitems.ToList());
        }

        public ActionResult TeacherResultStudentNew(int id/*id теста*/) //отчет для преподавателей "Результаты учащихся по тесту"
        {
            TestItem testitem = db.Testitems.First(q => q.ID == id);
            //List<TestItemAnswer> testitem = db.TestItemAnswers.Where(q => q.TestID == id).ToList();
            TempData["Check"] = "true";
            return View(testitem);
        }

        public ActionResult TeacherStudentResultView(int id) //отчет для преподавателей "Результаты учащихся по тесту"
        {
            TempData["Check"] = TempData["Check"];
            TempData["id"] = id;
            TestItemAnswer testitem = db.TestItemAnswers.First(q => q.ID == id);
            string kon = "";
            if (testitem.TestScore == 1)
            {
                kon = "балл";
            }
            else if (testitem.TestScore == 2 || testitem.TestScore == 3 || testitem.TestScore == 4)
            {
                kon = "балла";
            }
            else
            {
                kon = "баллов";

            }
            ViewBag.Kon = kon;

            return View(testitem);
        }

        /*========================================================

         Блок Егора (ручная проверка теста)

        ========================================================*/

        public ActionResult TeacherTestAnalysisMain()
        {
            List<Discipline> disciplines = new List<Discipline>();
            List<TestItem> tests = new List<TestItem>();
            List<DateTime> CalcDates = new List<DateTime>();
            disciplines = GetDisciplines().ToList();
            foreach (var disc in disciplines)
            {
                tests.AddRange(GetTestItems(disc.Id));
            }
            //Время последнего расчета
            if (tests.Count() != 0)
            {
                foreach (var test in tests)
                {
                    var dates = from rushstat in db.RushSatistics
                                where test.ID == rushstat.TestItem.ID
                                select rushstat.CalcDate;
                    if (dates.Count() != 0)
                    {
                        CalcDates.Add(dates.First());
                    }
                    else
                    {
                        CalcDates.Add(new DateTime(2000, 1, 1));
                    }
                }
            }
            ViewBag.calcDates = CalcDates;
            return View(tests.ToList());
        }

        public ActionResult TeacherRushStatNew(int testItemId)
        {
            double Xi = 0; //кол-во верных ответов юзера на данный тест
            double Rj = 0; //кол-во верных ответов на задание данного теста
            double pi = 0; //доля верных ответов юзера
            double qi = 0; //доля неверных ответов юзера
            double fi0 = 0; //начальное значение уровня подготовки студента
            double pj = 0; //доля верных ответов на задание
            double qj = 0; //доля неверных ответов на задание
            double beta0 = 0; //начальное значение уровня сложности задания
            List<Fi> listfi0 = new List<Fi>();
            List<Beta> listbeta0 = new List<Beta>();
            double sumfi0 = 0;
            double sumbeta0 = 0;
            double sumsqFi0 = 0;
            double sumsqBeta0 = 0;
            //средние значения
            double fiaverage = 0;
            double betaaverage = 0;
            //дисперсии
            double V = 0;
            double U = 0;
            //поправочные коэффициенты
            double X = 0;
            double Y = 0;
            //List<Fi> listfi = new List<Fi>();
            //List<Beta> listbeta = new List<Beta>();
            List<Beta> listPj = new List<Beta>();
            List<Beta> listIj = new List<Beta>();
            TestItemAnswer attemptCalc = null; //попытка юзера, по которой идет расчет
            List<TestItemAnswer> allAttempts = new List<TestItemAnswer>(); //все попытки, по которым шел расчет (чтобы ограничить потом отбор вопросов)
            DateTime time = System.DateTime.Now;

            //удаление старых данных
            List<RushSatistics> resultDel = (from stat in db.RushSatistics
                                             where stat.TestItem.ID == testItemId
                                             select stat).ToList();
            db.RushSatistics.RemoveRange(resultDel);
            db.SaveChanges();

            //Все результаты по даному тесту
            List<TestItemAnswer> results = (from testItemAns in db.TestItemAnswers
                                            where testItemAns.TestItem.ID == testItemId
                                            select testItemAns).ToList();
            if (results.Count() != 0)
            {
                //для каждого юзера, ответившего на тест, находим начальные значения уровня подготовки
                foreach (User user in (from tia in results
                                       select tia.Author).Distinct())
                {
                    //Находим попытки данного юзера
                    List<TestItemAnswer> Atempts = (from r in results
                                                    where r.Author == user
                                                    select r).ToList();
                    //Находим попытку, по которой будем производить расчет
                    if (db.Testitems.First(m => m.ID == testItemId).Estimation.EstID == 1)
                    {
                        attemptCalc = Atempts.Where(m => m.TestScore == Atempts.Max(e => e.TestScore)).First(); //first на случай, если несколько попыток с максимальной оценкой
                    }
                    else if (db.Testitems.First(m => m.ID == testItemId).Estimation.EstID == 2
                        || db.Testitems.First(m => m.ID == testItemId).Estimation.EstID == 4) //если средняя попытка, то берем последнюю, потому что неизвестно, какая попытка средняя и есть ли она вообще
                    {
                        attemptCalc = Atempts.Last();
                    }
                    else if (db.Testitems.First(m => m.ID == testItemId).Estimation.EstID == 3)
                    {
                        attemptCalc = Atempts.First();
                    }
                    //добавим ее в список попыток для расчета
                    allAttempts.Add(attemptCalc);
                    //кол-во верных ответов юзера
                    Xi = attemptCalc.TestScore;
                    //доля верных ответов
                    pi = Xi / attemptCalc.TestItem.Questions.Count();
                    if (pi == 0) //чтобы не было деления на 0 и логарифма(0)
                    { pi = 0.1f; }
                    qi = 1 - pi;
                    if (qi == 0)
                    { qi = 0.1f; }
                    //вычисляем начальное значение уровня подготовки студента
                    fi0 = Math.Log(pi / qi);
                    listfi0.Add(new Fi { StidentId = user.UserId, Value = fi0 }); //gca уложили данные в список начальных уровней подготовки испытуемых 
                    Xi = 0;
                }
            }
            List<Question> questions = db.Testitems.First(m => m.ID == testItemId).Questions.ToList(); //получили вопросы данного теста
            if (questions.Count() != 0)
            {
                //для каждого задания находим начальный уровень сложности
                foreach (Question q in questions)
                {
                    //достанем все ответы на данный вопрос
                    List<TestResults> answers = (from testres in db.TestResults
                                                 where testres.Question.ID == q.ID
                                                 select testres).ToList().Where(m => allAttempts.Contains(m.TestItemAnswer)).ToList();
                    //кол-во верных ответов
                    foreach (var ans in answers)
                    {
                        if (ans.IsRight == true)
                        { Rj++; }
                    }
                    //доля верных ответов
                    pj = Rj / answers.Count();
                    if (pj == 0)
                    { pj = 0.1f; }
                    qj = 1 - pj;
                    if (qj == 0)
                    { qj = 0.1f; }
                    //начальное значение уровня сложности задания
                    beta0 = Math.Log(pj / qj);
                    listbeta0.Add(new Beta { Value = beta0, QuestionId = q.ID });//gca уложили в список начальных значения трудности заданий
                    Rj = 0;
                }
            }
            //gca находим средние
            if (listfi0.Count() != 0)
            {
                foreach (var f0 in listfi0)
                {
                    sumfi0 += f0.Value;
                    sumsqFi0 += f0.Value * f0.Value;
                }
                fiaverage = sumfi0 / listfi0.Count();
            }
            if (listbeta0.Count() != 0)
            {
                foreach (var b0 in listbeta0)
                {
                    sumbeta0 += b0.Value;
                    sumsqBeta0 += b0.Value * b0.Value;
                }
                betaaverage = sumbeta0 / listbeta0.Count();
            }
            //gca находим дисперсии
            if ((from tia in results
                 select tia.Author).Distinct().Count() > 1 && questions.Count() > 1)
            {
                V = (sumsqFi0 - (from tia in results
                                 select tia.Author).Distinct().Count() * fiaverage * fiaverage)
                    / ((from tia in results
                        select tia.Author).Distinct().Count() - 1);
                U = (sumsqBeta0 - questions.Count() * betaaverage * betaaverage) / (questions.Count() - 1);
            }
            //gca находим поправочные коэффициенты
            X = Math.Sqrt((1 + U / 2.89) / (1 - U * V / 8.35));
            Y = Math.Sqrt((1 + V / 2.89) / (1 - U * V / 8.35));
            //gca оценки параметров
            if (listfi0.Count() != 0 && listbeta0.Count() != 0)
            {
                foreach (var f in listfi0)
                {
                    //listfi.Add(betaaverage + X * f.Value);
                    f.Value = betaaverage + X * f.Value;
                }
                foreach (var b in listbeta0)
                {
                    //listbeta.Add(new Beta { QuestionId = b.QuestionId, Value = fiaverage + Y * b.Value });
                    b.Value = fiaverage + Y * b.Value;
                }
            }
            //Для учащегося
            // Pj - Вероятность правильного ответа при различных уровнях подготовки
            foreach (var beta in listbeta0)
            {
                for (float fi = -5; fi <= 5; fi = fi + (float)0.5)
                {
                    listPj.Add(new Beta { DifficultQuestLvl = beta.Value, QuestionId = beta.QuestionId, Value = (float)((float)Math.Exp(1.7 * (fi - beta.Value))) / (1 + (float)Math.Exp(1.7 * (fi - beta.Value))) });
                    listIj.Add(new Beta { DifficultQuestLvl = beta.Value, QuestionId = beta.QuestionId, Value = (float)(2.89 * (float)Math.Exp(1.7 * (fi - beta.Value))) / ((1 + (float)Math.Exp(1.7 * (fi - beta.Value))) * (1 + (float)Math.Exp(1.7 * (fi - beta.Value)))) });
                }
                double j = -5;
                foreach (var p in listPj)
                {
                    db.RushSatistics.Add(new RushSatistics
                    {
                        FuntionType = 1,
                        TestItem = db.Testitems.First(m => m.ID == testItemId),
                        Question = db.Questions.First(m => m.ID == p.QuestionId),
                        LvlTraining = j,
                        Value = (float)p.Value,
                        CalcDate = time,
                        DifficultQuestLvl = p.DifficultQuestLvl
                    });
                    j += 0.5;
                }
                listPj.Clear();//очищаем коллекцию, чтобы рассматривать следующий вопрос
                j = -5;
                foreach (var p in listIj)
                {
                    db.RushSatistics.Add(new RushSatistics
                    {
                        FuntionType = 2,
                        TestItem = db.Testitems.First(m => m.ID == testItemId),
                        Question = db.Questions.First(m => m.ID == p.QuestionId),
                        LvlTraining = j,
                        Value = (float)p.Value,
                        CalcDate = time,
                        DifficultQuestLvl = p.DifficultQuestLvl
                    });
                    j += 0.5;
                }
                listIj.Clear();//очищаем коллекцию, чтобы рассматривать следующий вопрос
            }
            db.SaveChanges();
            ViewBag.testItemId = testItemId;
            return View("TeacherRushStatOld", questions);
        }

        [HttpPost]
        public ActionResult GetTeacherRushStatNewChart(int questionId, int testItemId) //возвращает част. предст. - кривую задания
        {
            List<RushSatistics> result = (from stat in db.RushSatistics
                                          where stat.TestItem.ID == testItemId
                                          & stat.Question.ID == questionId
                                          & stat.FuntionType == 1
                                          select stat).ToList();
            ViewBag.testItemId = testItemId;
            ViewBag.questionId = questionId;
            ExpPoint points = ExperimentPoint(questionId, testItemId);
            if (points.nFi.Count() != 0 & points.nPj.Count() != 0)
            {
                ViewBag.Message = "";
                if (points.ChiSq < 0.05)
                {
                    ViewBag.Advice = "Данное задание не согласуется с моделью Г. Раша. Его следует исключить из теста.";
                }
                else if (points.ChiSq > 0.05 & points.ChiSq <= 0.5)
                {
                    ViewBag.Advice = "Данное задание имеет удовлетворительное согласие с моделью Г. Раша. Его можно временно оставить в тесте, имея в виду дальнейшую проверку теста в целом.";
                }
                else if (points.ChiSq > 0.5 & points.ChiSq <= 0.8)
                {
                    ViewBag.Advice = "Данное задание имеет хорошее согласие с моделью Раша с моделью Г. Раша. Его можно оставить в тесте.";
                }
                else if (points.ChiSq > 0.8 & points.ChiSq <= 1)
                {
                    ViewBag.Advice = "Данное задание имеет отличное согласие с моделью Раша с моделью Г. Раша. Его следует оставить в тесте.";
                }
            }
            else
            {
                ViewBag.Message = "Недостаточно данных для построения экспериментальных точек";
            }
            return View("TeacherRushStatNewICCChartPartial", result);
        }


        public ActionResult ICCChart(int questionId, int testItemId)
        {
            List<RushSatistics> result = (from stat in db.RushSatistics
                                          where stat.TestItem.ID == testItemId
                                          & stat.Question.ID == questionId
                                          & stat.FuntionType == 1
                                          select stat).ToList();
            List<double> LvlTraining = new List<double>();
            List<float> Pj = new List<float>();
            string title = "";
            foreach (var st in result)
            {
                LvlTraining.Add(st.LvlTraining);
                Pj.Add(st.Value);
            }
            title = "Характеристическая кривая задания " + result.First().Question.Body;
            ExpPoint points = ExperimentPoint(questionId, testItemId);
            if (points.nFi.Count() !=0 & points.nPj.Count() !=0)
            {
                var chart = new SimpleChart.Chart(width: 1000, height: 500)
                      .AddTitle(title)
                      .AddSeries(
                             name: "Вероятность верного ответа",
                             chartType: "Line",
                             xValue: LvlTraining.ToArray(),
                             yValues: Pj.ToArray())
                      .AddSeries(
                             name: "Экспериментальные точки",
                             chartType: "Point",
                             xValue: points.nFi.ToArray(),
                             yValues: points.nPj.ToArray())
                      .SetYAxis(
                             min: 0,
                             max: 1)
                      .SetXAxis(
                             min: -5,
                             max: 5)
                      .AddLegend()
                      .Write();
                
            }
            else
            {
                var chart = new SimpleChart.Chart(width: 1000, height: 500)
                   .AddTitle(title)
                   .AddSeries(
                          name: "Вероятность верного ответа",
                          chartType: "Line",
                          xValue: LvlTraining.ToArray(),
                          yValues: Pj.ToArray())
                      .SetYAxis(
                             min: 0,
                             max: 1)
                      .SetXAxis(
                             min: -5,
                             max: 5)
                   .AddLegend()
                   .Write();
                
            }
            return null;
        }

        public ActionResult TeacherRushStatOld(int testItemId)
        {
            List<Question> questions = db.Testitems.Find(testItemId).Questions;
            ViewBag.testItemId = testItemId;
            return View(questions);
        }

        public ActionResult TeacherInfFunctionChart(int testItemId)
        {
            List<double> LvlTraining = new List<double>();
            List<float> Pj = new List<float>();
            List<RushSatistics> result = (from stat in db.RushSatistics
                                          where stat.TestItem.ID == testItemId
                                          & stat.FuntionType == 2
                                          select stat).ToList();
            List<ICCRush> infFunc = new List<ICCRush>();
            if (result.Count() != 0)
            {
                for (double i = -5; i <= 5; i += 0.5)
                {
                    infFunc.Add(new ICCRush { nFi = i, nPj = result.Where(m => m.LvlTraining == i).Sum(e => e.Value) });
                }
            }
            ViewBag.testItemId = testItemId;
            //проверяем на наличие нескольких макс. значений
            bool max = false;
            int maxdubl = 0;
            int down = 0;
            foreach (var st in infFunc)
            {
                LvlTraining.Add(st.nFi);
                Pj.Add((float)st.nPj);
            }
            for (int i = 0; i < Pj.Count() - 1; i++)
            {
                if (Pj[i] > Pj[i + 1] & !max)//если первый максимум
                {
                    max = true;
                }
                else if (Pj[i] < Pj[i + 1] & max & maxdubl==0) //если второй максимум
                {
                    maxdubl = 1;
                    down = i;
                }
            }
            if (maxdubl == 0) //если один максимум - хороший тест
            {
                ViewBag.Advice = "Информационная функция имеет один четко выраженный максимум. Тест не нуждается в доработке.";
            }
            else if (maxdubl == 1)// если больше одного максимума - плохой тест
            {
                ViewBag.Advice = "Информационная функция имеет более одного максимума. Тест нуждается в доработке. Необходимо добавить задания с уровнем трудности, равным " + LvlTraining[down] + " логит.";
            }
            return View(infFunc);
        }

        public ActionResult GetTeacherInfFunctionChart(int testItemId)
        {
            List<RushSatistics> result = (from stat in db.RushSatistics
                                          where stat.TestItem.ID == testItemId
                                          & stat.FuntionType == 2
                                          select stat).ToList();
            List<ICCRush> infFunc = new List<ICCRush>();
            if (result.Count() != 0)
            {
                for (double i = -5; i <= 5; i += 0.5)
                {
                    infFunc.Add(new ICCRush { nFi = i, nPj = result.Where(m => m.LvlTraining == i).Sum(e => e.Value) });
                }
            }
            List<double> LvlTraining = new List<double>();
            List<float> Pj = new List<float>();
            string title = "";
            foreach (var st in infFunc)
            {
                LvlTraining.Add(st.nFi);
                Pj.Add((float)st.nPj);
            }
                title = "Информационная функция теста " + db.Testitems.Find(testItemId).Subject;
            var chart = new SimpleChart.Chart(width: 1000, height: 500)
                  .AddTitle(title)
                  .AddSeries(
                         name: "Информационная функция",
                         chartType: "Line",
                         xValue: LvlTraining.ToArray(),
                         yValues: Pj.ToArray())
                  .SetYAxis(
                         min:0,
                         max: 2)
                  .SetXAxis(
                         min: -5,
                         max: 5)
                  .AddLegend()
                  .Write();
            return null;
        }

        [HttpPost]
        public ActionResult ScoreEdit(string score, int id)
        {
            ViewBag.Score = score;
            TestItemAnswer t = db.TestItemAnswers.Find(id);
            t.TestScore = Convert.ToInt32(score);
            db.Entry(t).State = EntityState.Modified;
            db.SaveChanges();
            string kon = "";
            if (score == "1")
            {
                kon = "балл";
            }
            else if (score == "2" || score == "3" || score == "4")
            {
                kon = "балла";
            }
            else
            {
                kon = "баллов";

            }
            ViewBag.Kon = kon;

            return View("_ScoreEdit");
        }

        public ExpPoint ExperimentPoint(int questionId, int testItemId)
        {
            ExpPoint points = new ExpPoint();
            points.nFi = new List<double>();
            points.nPj = new List<double>();
            TestItemAnswer attemptCalc = null; //попытка юзера, по которой идет расчет
            List<TestItemAnswer> allAttempts = new List<TestItemAnswer>(); //все попытки, по которым шел расчет (чтобы ограничить потом отбор вопросов)
            double Xi = 0; //кол-во верных ответов юзера на данный тест
            double pi = 0; //доля верных ответов юзера
            double qi = 0; //доля неверных ответов юзера
            double fi0 = 0; //начальное значение уровня подготовки студента
            List<Fi> listfi0 = new List<Fi>();
            int rightCount = 0;
            int studentInGroupCount = 0;
            List<double> levels = new List<double>();
            List<double> expPj = new List<double>();
            List<double> keys = new List<double>();
            List<int> studentsInGroups = new List<int>();
            List<double> levelsFi = new List<double>();
            double x2 = 0;
            double chiSq = 0;
            //Все результаты по даному тесту
            List<TestItemAnswer> results = (from testItemAns in db.TestItemAnswers
                                            where testItemAns.TestItem.ID == testItemId
                                            select testItemAns).ToList();
            if (results.Count() != 0)
            {
                //для каждого юзера, ответившего на тест, находим начальные значения уровня подготовки
                foreach (User user in (from tia in results
                                       select tia.Author).Distinct())
                {
                    //Находим попытки данного юзера
                    List<TestItemAnswer> Atempts = (from r in results
                                                    where r.Author == user
                                                    select r).ToList();
                    //Находим попытку, по которой будем производить расчет
                    if (db.Testitems.First(m => m.ID == testItemId).Estimation.EstID == 1)
                    {
                        attemptCalc = Atempts.Where(m => m.TestScore == Atempts.Max(e => e.TestScore)).First(); //first на случай, если несколько попыток с максимальной оценкой
                    }
                    else if (db.Testitems.First(m => m.ID == testItemId).Estimation.EstID == 2
                        || db.Testitems.First(m => m.ID == testItemId).Estimation.EstID == 4) //если средняя попытка, то берем последнюю, потому что неизвестно, какая попытка средняя и есть ли она вообще
                    {
                        attemptCalc = Atempts.Last();
                    }
                    else if (db.Testitems.First(m => m.ID == testItemId).Estimation.EstID == 3)
                    {
                        attemptCalc = Atempts.First();
                    }
                    //добавим ее в список попыток для расчета
                    allAttempts.Add(attemptCalc);
                    //кол-во верных ответов юзера
                    Xi = attemptCalc.TestScore;
                    //доля верных ответов
                    pi = Xi / attemptCalc.TestItem.Questions.Count();
                    if (pi == 0) //чтобы не было деления на 0 и логарифма(0)
                    { pi = 0.1f; }
                    qi = 1 - pi;
                    if (qi == 0)
                    { qi = 0.1f; }
                    //вычисляем начальное значение уровня подготовки студента
                    fi0 = Math.Log(pi / qi);
                    listfi0.Add(new Fi { StidentId = user.UserId, Value = fi0 }); //gca уложили данные в список начальных уровней подготовки испытуемых 
                    Xi = 0;
                }
            }
            //ответы студентов на данный вопрос
            List<TestResults> answers = (from testres in db.TestResults
                                         where testres.Question.ID == questionId
                                         select testres).ToList().Where(m => allAttempts.Contains(m.TestItemAnswer)).ToList();
            //сейчас у нас есть список начальных уровней подготовки студентов вида "студент - уровень подг."
            if (listfi0.Count() != 0)
            {
                //сортируем список по уровню подготовки
                /*var newListFi = listfi0.OrderBy(x => x.Value).ToList();
                double currentFi = listfi0.First().Value ; //текущий уровень
                foreach (var fi in newListFi)
                {
                    if (currentFi != fi.Value)  //если перешли к новому уровню подготовки
                    {
                    }
                    else
                    { 
                        
                    }
                    currentFi = fi.Value; //запоминаем текущий уровень
                }*/
                //группируем уровни подготовки
                var queryfiGroup =
                    from fi in listfi0
                    group fi by Math.Round(fi.Value, 2) into fiGroup
                    orderby fiGroup.Key
                    select fiGroup;
                //соответствие индексов и групп
                List<Fi> index = new List<Fi>();
                foreach (var gr in queryfiGroup)
                {
                    index.Add(new Fi { Value= gr.Key});
                }
                if (queryfiGroup.Count() < 3)//недостаточно групп для анализа
                {
                    return points;
                }
                else
                {
                    int excessGroupNum = queryfiGroup.Count() % 3; //лишние группы, которые надо добавить в другие
                    int enoughGroupNum = queryfiGroup.Count() / 3; //кол-во групп, которые войдут в одну большую группу
                    if (excessGroupNum == 1)  // если одна группа выбивается
                    {
                        for (int i = 0; i < enoughGroupNum; i++) // первая группа
                        {
                            double key = index[i].Value;
                            keys.Add(key);
                            foreach (var studentGroup in queryfiGroup.Where(m => m.Key == key))
                            {
                                studentInGroupCount += studentGroup.Count();
                                foreach (var student in studentGroup)
                                {
                                    //находим ответ данного студента
                                    var answer = (from ans in answers
                                                 where ans.TestItemAnswer.Author.UserId == student.StidentId
                                                 select ans).First();
                                    if (answer.IsRight == true)
                                    {
                                        rightCount++;
                                    }
                                }
                            }
                        }
                        //закончили считать первую группу
                        levels.Add(keys.Average());
                        levelsFi.Add(keys.Average());
                        keys.Clear();
                        expPj.Add((double)rightCount / (double)studentInGroupCount);
                        rightCount = 0;
                        studentsInGroups.Add(studentInGroupCount);
                        studentInGroupCount = 0;

                        for (int i = enoughGroupNum; i < enoughGroupNum*2; i++) // вторая группа
                        {
                            double key = index[i].Value;
                            keys.Add(key);
                            foreach (var studentGroup in queryfiGroup.Where(m => m.Key == key))
                            {
                                studentInGroupCount += studentGroup.Count();
                                foreach (var student in studentGroup)
                                {
                                    //находим ответ данного студента
                                    var answer = (from ans in answers
                                                  where ans.TestItemAnswer.Author.UserId == student.StidentId
                                                  select ans).First();
                                    if (answer.IsRight == true)
                                    {
                                        rightCount++;
                                    }
                                }
                            }
                        }
                        //закончили считать вторую группу
                        levels.Add(keys.Average());
                        levelsFi.Add(keys.Average());
                        keys.Clear();
                        expPj.Add((double)rightCount / (double)studentInGroupCount);
                        rightCount = 0;
                        studentsInGroups.Add(studentInGroupCount);
                        studentInGroupCount = 0;

                        for (int i = enoughGroupNum * 2; i < enoughGroupNum * 3 + 1; i++) // третья группа
                        {
                            double key = index[i].Value;
                            keys.Add(key);
                            foreach (var studentGroup in queryfiGroup.Where(m => m.Key == key))
                            {
                                studentInGroupCount += studentGroup.Count();
                                foreach (var student in studentGroup)
                                {
                                    //находим ответ данного студента
                                    var answer = (from ans in answers
                                                  where ans.TestItemAnswer.Author.UserId == student.StidentId
                                                  select ans).First();
                                    if (answer.IsRight == true)
                                    {
                                        rightCount++;
                                    }
                                }
                            }
                        }
                        //закончили считать третью группу
                        levels.Add(keys.Average());
                        levelsFi.Add(keys.Average());
                        keys.Clear();
                        expPj.Add((double)rightCount / (double)studentInGroupCount);
                        rightCount = 0;
                        studentsInGroups.Add(studentInGroupCount);
                        studentInGroupCount = 0;
                    }
                    else if (excessGroupNum == 2) //если две группы выбиваются
                    {
                        for (int i = 0; i < enoughGroupNum; i++) // первая группа
                        {
                            double key = index[i].Value;
                            keys.Add(key);
                            foreach (var studentGroup in queryfiGroup.Where(m => m.Key == key))
                            {
                                studentInGroupCount += studentGroup.Count();
                                foreach (var student in studentGroup)
                                {
                                    //находим ответ данного студента
                                    var answer = (from ans in answers
                                                  where ans.TestItemAnswer.Author.UserId == student.StidentId
                                                  select ans).First();
                                    if (answer.IsRight == true)
                                    {
                                        rightCount++;
                                    }
                                }
                            }
                        }
                        //закончили считать первую группу
                        levels.Add(keys.Average());
                        levelsFi.Add(keys.Average());
                        keys.Clear();
                        expPj.Add((double)rightCount / (double)studentInGroupCount);
                        rightCount = 0;
                        studentsInGroups.Add(studentInGroupCount);
                        studentInGroupCount = 0;

                        for (int i = enoughGroupNum; i < enoughGroupNum * 2 + 1; i++) // вторая группа
                        {
                            double key = index[i].Value;
                            keys.Add(key);
                            foreach (var studentGroup in queryfiGroup.Where(m => m.Key == key))
                            {
                                studentInGroupCount += studentGroup.Count();
                                foreach (var student in studentGroup)
                                {
                                    //находим ответ данного студента
                                    var answer = (from ans in answers
                                                  where ans.TestItemAnswer.Author.UserId == student.StidentId
                                                  select ans).First();
                                    if (answer.IsRight == true)
                                    {
                                        rightCount++;
                                    }
                                }
                            }
                        }
                        //закончили считать вторую группу
                        levels.Add(keys.Average());
                        levelsFi.Add(keys.Average());
                        keys.Clear();
                        expPj.Add((double)rightCount / (double)studentInGroupCount);
                        rightCount = 0;
                        studentsInGroups.Add(studentInGroupCount);
                        studentInGroupCount = 0;

                        for (int i = enoughGroupNum * 2 + 1; i < enoughGroupNum * 3 + 1; i++) // третья группа
                        {
                            double key = index[i].Value;
                            keys.Add(key);
                            foreach (var studentGroup in queryfiGroup.Where(m => m.Key == key))
                            {
                                studentInGroupCount += studentGroup.Count();
                                foreach (var student in studentGroup)
                                {
                                    //находим ответ данного студента
                                    var answer = (from ans in answers
                                                  where ans.TestItemAnswer.Author.UserId == student.StidentId
                                                  select ans).First();
                                    if (answer.IsRight == true)
                                    {
                                        rightCount++;
                                    }
                                }
                            }
                        }
                        //закончили считать третью группу
                        levels.Add(keys.Average());
                        levelsFi.Add(keys.Average());
                        keys.Clear();
                        expPj.Add((double)rightCount / (double)studentInGroupCount);
                        rightCount = 0;
                        studentsInGroups.Add(studentInGroupCount);
                        studentInGroupCount = 0;
                    }
                    else // если кол-во групп кратно трем
                    {
                        for (int i = 0; i < enoughGroupNum; i++) // первая группа
                        {
                            double key = index[i].Value;
                            keys.Add(key);
                            foreach (var studentGroup in queryfiGroup.Where(m => m.Key == key))
                            {
                                studentInGroupCount += studentGroup.Count();
                                foreach (var student in studentGroup)
                                {
                                    //находим ответ данного студента
                                    var answer = (from ans in answers
                                                  where ans.TestItemAnswer.Author.UserId == student.StidentId
                                                  select ans).First();
                                    if (answer.IsRight == true)
                                    {
                                        rightCount++;
                                    }
                                }
                            }
                        }
                        //закончили считать первую группу
                        levels.Add(keys.Average());
                        levelsFi.Add(keys.Average());
                        keys.Clear();
                        expPj.Add((double)rightCount / (double)studentInGroupCount);
                        rightCount = 0;
                        studentsInGroups.Add(studentInGroupCount);
                        studentInGroupCount = 0;

                        for (int i = enoughGroupNum; i < enoughGroupNum * 2; i++) // вторая группа
                        {
                            double key = index[i].Value;
                            keys.Add(key);
                            foreach (var studentGroup in queryfiGroup.Where(m => m.Key == key))
                            {
                                studentInGroupCount += studentGroup.Count();
                                foreach (var student in studentGroup)
                                {
                                    //находим ответ данного студента
                                    var answer = (from ans in answers
                                                  where ans.TestItemAnswer.Author.UserId == student.StidentId
                                                  select ans).First();
                                    if (answer.IsRight == true)
                                    {
                                        rightCount++;
                                    }
                                }
                            }
                        }
                        //закончили считать вторую группу
                        levels.Add(keys.Average());
                        levelsFi.Add(keys.Average());
                        keys.Clear();
                        expPj.Add((double)rightCount / (double)studentInGroupCount);
                        rightCount = 0;
                        studentsInGroups.Add(studentInGroupCount);
                        studentInGroupCount = 0;

                        for (int i = enoughGroupNum * 2; i < enoughGroupNum * 3; i++) // третья группа
                        {
                            double key = index[i].Value;
                            keys.Add(key);
                            foreach (var studentGroup in queryfiGroup.Where(m => m.Key == key))
                            {
                                studentInGroupCount += studentGroup.Count();
                                foreach (var student in studentGroup)
                                {
                                    //находим ответ данного студента
                                    var answer = (from ans in answers
                                                  where ans.TestItemAnswer.Author.UserId == student.StidentId
                                                  select ans).First();
                                    if (answer.IsRight == true)
                                    {
                                        rightCount++;
                                    }
                                }
                            }
                        }
                        //закончили считать третью группу
                        levels.Add(keys.Average());
                        levelsFi.Add(keys.Average());
                        keys.Clear();
                        expPj.Add((double)rightCount / (double)studentInGroupCount);
                        rightCount = 0;
                        studentsInGroups.Add(studentInGroupCount);
                        studentInGroupCount = 0;
                    }
                    //сложность текущего задания
                    double difficultQuest = (from rush in db.RushSatistics
                                             where rush.Question.ID == questionId
                                             & rush.TestItem.ID == testItemId
                                             select rush.DifficultQuestLvl).First();

                    
                    for (int i = 0; i < queryfiGroup.Count()-1; i++)
                    {
                        x2 += studentsInGroups[i] * (
                                                    Math.Pow(
                                                             (expPj[i] - (
                                                                          (float)(
                                                                                  (float)Math.Exp(1.7 * (levels[i] - difficultQuest))
                                                                                 ) 
                                                                          / (1 + (float)Math.Exp(1.7 * (levels[i] - difficultQuest)))
                                                                          )
                                                             ), 2
                                                            ) 
                                                    / (
                                                       (
                                                        (float)((float)Math.Exp(1.7 * (levels[i] - difficultQuest))) 
                                                        / (1 + (float)Math.Exp(1.7 * (levels[i] - difficultQuest)))
                                                       ) 
                                                       * (1 / (1 + (float)Math.Exp(1.7 * (levels[i] - difficultQuest))))
                                                      )
                                                    );
                    }
                    //уровень значимости статистики хи-квадрат
                    // если меньше 0.05, то задание не соотв. модели Раша
                    chiSq = 1-ChiSqFunctions.chisquaredistribution((double)queryfiGroup.Count(), x2);
                }
                points.nFi = levels;
                points.nPj = expPj;
                points.ChiSq = chiSq;
            }
            return points;
        }

    }
}
