namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Matrices", "binary2", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Matrices", "binary2");
        }
    }
}
