namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Изменениемоделисобытиядобавлениеавтора : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CalendarEvents", "Author_UserId", c => c.Int());
            CreateIndex("dbo.CalendarEvents", "Author_UserId");
            AddForeignKey("dbo.CalendarEvents", "Author_UserId", "dbo.User", "UserId");
        }

        public override void Down()
        {
            DropForeignKey("dbo.CalendarEvents", "Author_UserId", "dbo.User");
            DropIndex("dbo.CalendarEvents", new[] { "Author_UserId" });
            DropColumn("dbo.CalendarEvents", "Author_UserId");
        }
    }
}
