namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PaginType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TestItems", "Paging", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TestItems", "Paging");
        }
    }
}
