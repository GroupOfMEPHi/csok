namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TestItemAnswers", "TestID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TestItemAnswers", "TestID");
        }
    }
}
