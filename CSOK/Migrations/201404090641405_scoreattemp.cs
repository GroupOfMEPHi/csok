namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class scoreattemp : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TestItemAnswers", "CurrentAttempt", c => c.Int(nullable: false));
            AddColumn("dbo.TestItemAnswers", "TestScore", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TestItemAnswers", "TestScore");
            DropColumn("dbo.TestItemAnswers", "CurrentAttempt");
        }
    }
}
