namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tst : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.TestItemAnswers", "TestID");
            DropColumn("dbo.TestItemAnswers", "CreationDateTime");
            DropColumn("dbo.TestItemAnswers", "ClosingDateTime");
            DropColumn("dbo.TestItemAnswers", "OpeningDateTime");
            DropColumn("dbo.TestItems", "AuthorID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TestItems", "AuthorID", c => c.Int(nullable: false));
            AddColumn("dbo.TestItemAnswers", "OpeningDateTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.TestItemAnswers", "ClosingDateTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.TestItemAnswers", "CreationDateTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.TestItemAnswers", "TestID", c => c.Int(nullable: false));
        }
    }
}
