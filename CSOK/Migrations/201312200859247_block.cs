namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class block : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnswerBlocks",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Order = c.Int(nullable: false),
                        Body = c.String(),
                        AnsernDataTime = c.DateTime(nullable: false),
                        Block_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Blocks", t => t.Block_ID)
                .Index(t => t.Block_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnswerBlocks", "Block_ID", "dbo.Blocks");
            DropIndex("dbo.AnswerBlocks", new[] { "Block_ID" });
            DropTable("dbo.AnswerBlocks");
        }
    }
}
