namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class answer_excess_blocks : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnswerExcessBlocks",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuthorID = c.Int(nullable: false),
                        Body = c.String(),
                        IsRight = c.Boolean(nullable: false),
                        AnsernDataTime = c.DateTime(nullable: false),
                        TestID = c.Int(nullable: false),
                        Attempt = c.Int(nullable: false),
                        Excess_Block_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Excess_Block", t => t.Excess_Block_ID)
                .Index(t => t.Excess_Block_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnswerExcessBlocks", "Excess_Block_ID", "dbo.Excess_Block");
            DropIndex("dbo.AnswerExcessBlocks", new[] { "Excess_Block_ID" });
            DropTable("dbo.AnswerExcessBlocks");
        }
    }
}
