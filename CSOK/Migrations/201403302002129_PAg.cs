namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PAg : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.TestItems", "Paging");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TestItems", "Paging", c => c.Int(nullable: false));
        }
    }
}
