namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fix : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Structures", "Discipline_Id", "dbo.Disciplines");
            DropForeignKey("dbo.Themes", "Structure_Id", "dbo.Structures");
            DropForeignKey("dbo.Questions", "Theme_Id", "dbo.Themes");
            DropIndex("dbo.Structures", new[] { "Discipline_Id" });
            DropIndex("dbo.Themes", new[] { "Structure_Id" });
            DropIndex("dbo.Questions", new[] { "Theme_Id" });
            AlterColumn("dbo.Questions", "Theme_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Structures", "Discipline_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Themes", "Structure_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Structures", "Discipline_Id");
            CreateIndex("dbo.Themes", "Structure_Id");
            CreateIndex("dbo.Questions", "Theme_Id");
            AddForeignKey("dbo.Structures", "Discipline_Id", "dbo.Disciplines", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Themes", "Structure_Id", "dbo.Structures", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Questions", "Theme_Id", "dbo.Themes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Questions", "Theme_Id", "dbo.Themes");
            DropForeignKey("dbo.Themes", "Structure_Id", "dbo.Structures");
            DropForeignKey("dbo.Structures", "Discipline_Id", "dbo.Disciplines");
            DropIndex("dbo.Questions", new[] { "Theme_Id" });
            DropIndex("dbo.Themes", new[] { "Structure_Id" });
            DropIndex("dbo.Structures", new[] { "Discipline_Id" });
            AlterColumn("dbo.Themes", "Structure_Id", c => c.Int());
            AlterColumn("dbo.Structures", "Discipline_Id", c => c.Int());
            AlterColumn("dbo.Questions", "Theme_Id", c => c.Int());
            CreateIndex("dbo.Questions", "Theme_Id");
            CreateIndex("dbo.Themes", "Structure_Id");
            CreateIndex("dbo.Structures", "Discipline_Id");
            AddForeignKey("dbo.Questions", "Theme_Id", "dbo.Themes", "Id");
            AddForeignKey("dbo.Themes", "Structure_Id", "dbo.Structures", "Id");
            AddForeignKey("dbo.Structures", "Discipline_Id", "dbo.Disciplines", "Id");
        }
    }
}
