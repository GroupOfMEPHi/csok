namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class answercorr : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AnswerCorrectAnswers", "Author_UserId", "dbo.User");
            DropIndex("dbo.AnswerCorrectAnswers", new[] { "Author_UserId" });
            AddColumn("dbo.AnswerCorrectAnswers", "TestItemAnswer_ID", c => c.Int());
            CreateIndex("dbo.AnswerCorrectAnswers", "TestItemAnswer_ID");
            AddForeignKey("dbo.AnswerCorrectAnswers", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID");
            DropColumn("dbo.AnswerCorrectAnswers", "AnsernDataTime");
            DropColumn("dbo.AnswerCorrectAnswers", "TestID");
            DropColumn("dbo.AnswerCorrectAnswers", "Attempt");
            DropColumn("dbo.AnswerCorrectAnswers", "Author_UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AnswerCorrectAnswers", "Author_UserId", c => c.Int());
            AddColumn("dbo.AnswerCorrectAnswers", "Attempt", c => c.Int(nullable: false));
            AddColumn("dbo.AnswerCorrectAnswers", "TestID", c => c.Int(nullable: false));
            AddColumn("dbo.AnswerCorrectAnswers", "AnsernDataTime", c => c.DateTime(nullable: false));
            DropForeignKey("dbo.AnswerCorrectAnswers", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropIndex("dbo.AnswerCorrectAnswers", new[] { "TestItemAnswer_ID" });
            DropColumn("dbo.AnswerCorrectAnswers", "TestItemAnswer_ID");
            CreateIndex("dbo.AnswerCorrectAnswers", "Author_UserId");
            AddForeignKey("dbo.AnswerCorrectAnswers", "Author_UserId", "dbo.User", "UserId");
        }
    }
}
