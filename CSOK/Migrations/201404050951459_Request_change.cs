namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Request_change : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Request", "Num_of_record_book", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Request", "Num_of_record_book", c => c.Int(nullable: false));
        }
    }
}
