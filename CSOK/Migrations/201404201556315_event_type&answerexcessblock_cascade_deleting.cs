namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class event_typeanswerexcessblock_cascade_deleting : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AnswerExcessBlocks", "Excess_Block_ID", "dbo.Excess_Block");
            DropIndex("dbo.AnswerExcessBlocks", new[] { "Excess_Block_ID" });
            AddColumn("dbo.CalendarEvents", "type", c => c.String());
            AlterColumn("dbo.AnswerExcessBlocks", "Excess_Block_ID", c => c.Int(nullable: false));
            CreateIndex("dbo.AnswerExcessBlocks", "Excess_Block_ID");
            AddForeignKey("dbo.AnswerExcessBlocks", "Excess_Block_ID", "dbo.Excess_Block", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnswerExcessBlocks", "Excess_Block_ID", "dbo.Excess_Block");
            DropIndex("dbo.AnswerExcessBlocks", new[] { "Excess_Block_ID" });
            AlterColumn("dbo.AnswerExcessBlocks", "Excess_Block_ID", c => c.Int());
            DropColumn("dbo.CalendarEvents", "type");
            CreateIndex("dbo.AnswerExcessBlocks", "Excess_Block_ID");
            AddForeignKey("dbo.AnswerExcessBlocks", "Excess_Block_ID", "dbo.Excess_Block", "ID");
        }
    }
}
