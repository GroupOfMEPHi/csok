namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testmodee : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.TestItems", "TestMode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TestItems", "TestMode", c => c.Boolean(nullable: false));
        }
    }
}
