namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeModelRushStatAddPropertyValue : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RushSatistics", "Value", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.RushSatistics", "Value");
        }
    }
}
