namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class svyz2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TestItemAnswers", "Question_ID", "dbo.Questions");
            DropIndex("dbo.TestItemAnswers", new[] { "Question_ID" });
            DropColumn("dbo.TestItemAnswers", "Question_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TestItemAnswers", "Question_ID", c => c.Int());
            CreateIndex("dbo.TestItemAnswers", "Question_ID");
            AddForeignKey("dbo.TestItemAnswers", "Question_ID", "dbo.Questions", "ID");
        }
    }
}
