namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class svyz3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TestItemAnswerQuestions",
                c => new
                    {
                        TestItemAnswer_ID = c.Int(nullable: false),
                        Question_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TestItemAnswer_ID, t.Question_ID })
                .ForeignKey("dbo.TestItemAnswers", t => t.TestItemAnswer_ID, cascadeDelete: true)
                .ForeignKey("dbo.Questions", t => t.Question_ID, cascadeDelete: true)
                .Index(t => t.TestItemAnswer_ID)
                .Index(t => t.Question_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TestItemAnswerQuestions", "Question_ID", "dbo.Questions");
            DropForeignKey("dbo.TestItemAnswerQuestions", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropIndex("dbo.TestItemAnswerQuestions", new[] { "Question_ID" });
            DropIndex("dbo.TestItemAnswerQuestions", new[] { "TestItemAnswer_ID" });
            DropTable("dbo.TestItemAnswerQuestions");
        }
    }
}
