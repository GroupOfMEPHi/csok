namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Excess_Blocks : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Excess_Block",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(),
                        IsRight = c.Boolean(nullable: false),
                        Question_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Questions", t => t.Question_ID)
                .Index(t => t.Question_ID);
            
            AddColumn("dbo.Questions", "HasExcessBlocks", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Excess_Block", "Question_ID", "dbo.Questions");
            DropIndex("dbo.Excess_Block", new[] { "Question_ID" });
            DropColumn("dbo.Questions", "HasExcessBlocks");
            DropTable("dbo.Excess_Block");
        }
    }
}
