namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class currentimeChangeEndTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TestItemAnswers", "EndtDateTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.TestItemAnswers", "SpentTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TestItemAnswers", "SpentTime", c => c.String());
            DropColumn("dbo.TestItemAnswers", "EndtDateTime");
        }
    }
}
