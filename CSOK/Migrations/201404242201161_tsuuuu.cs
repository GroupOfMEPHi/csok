namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tsuuuu : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TestItems", "Author_UserId", c => c.Int());
            CreateIndex("dbo.TestItems", "Author_UserId");
            AddForeignKey("dbo.TestItems", "Author_UserId", "dbo.User", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TestItems", "Author_UserId", "dbo.User");
            DropIndex("dbo.TestItems", new[] { "Author_UserId" });
            DropColumn("dbo.TestItems", "Author_UserId");
        }
    }
}
