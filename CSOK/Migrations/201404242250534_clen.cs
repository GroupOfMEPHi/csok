namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class clen : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AnswerCorrectAnswers", "AuthorID");
            DropColumn("dbo.Questions", "AuthorID");
            DropColumn("dbo.AnswerBlocks", "AuthorID");
            DropColumn("dbo.Answers", "AuthorID");
            DropColumn("dbo.TestItemAnswers", "AuthorID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TestItemAnswers", "AuthorID", c => c.Int(nullable: false));
            AddColumn("dbo.Answers", "AuthorID", c => c.Int(nullable: false));
            AddColumn("dbo.AnswerBlocks", "AuthorID", c => c.Int(nullable: false));
            AddColumn("dbo.Questions", "AuthorID", c => c.Int(nullable: false));
            AddColumn("dbo.AnswerCorrectAnswers", "AuthorID", c => c.Int(nullable: false));
        }
    }
}
