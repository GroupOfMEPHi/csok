namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renameAnswer : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Answers", "PossibleAnswer_ID", "dbo.PossibleAnswers");
            DropForeignKey("dbo.Answers", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropIndex("dbo.Answers", new[] { "PossibleAnswer_ID" });
            DropIndex("dbo.Answers", new[] { "TestItemAnswer_ID" });
            CreateTable(
                "dbo.AnswerPossibleAnswers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IsRight = c.Boolean(nullable: false),
                        PossibleAnswer_ID = c.Int(nullable: false),
                        TestItemAnswer_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PossibleAnswers", t => t.PossibleAnswer_ID, cascadeDelete: true)
                .ForeignKey("dbo.TestItemAnswers", t => t.TestItemAnswer_ID)
                .Index(t => t.PossibleAnswer_ID)
                .Index(t => t.TestItemAnswer_ID);
            
            DropTable("dbo.Answers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Answers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IsRight = c.Boolean(nullable: false),
                        PossibleAnswer_ID = c.Int(nullable: false),
                        TestItemAnswer_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
            DropForeignKey("dbo.AnswerPossibleAnswers", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropForeignKey("dbo.AnswerPossibleAnswers", "PossibleAnswer_ID", "dbo.PossibleAnswers");
            DropIndex("dbo.AnswerPossibleAnswers", new[] { "TestItemAnswer_ID" });
            DropIndex("dbo.AnswerPossibleAnswers", new[] { "PossibleAnswer_ID" });
            DropTable("dbo.AnswerPossibleAnswers");
            CreateIndex("dbo.Answers", "TestItemAnswer_ID");
            CreateIndex("dbo.Answers", "PossibleAnswer_ID");
            AddForeignKey("dbo.Answers", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID");
            AddForeignKey("dbo.Answers", "PossibleAnswer_ID", "dbo.PossibleAnswers", "ID", cascadeDelete: true);
        }
    }
}
