// <auto-generated />
namespace CSOK.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.1-21010")]
    public sealed partial class testmode : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(testmode));
        
        string IMigrationMetadata.Id
        {
            get { return "201404130019230_testmode"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
