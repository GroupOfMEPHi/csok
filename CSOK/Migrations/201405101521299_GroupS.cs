namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GroupS : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Group", newName: "Groups");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Groups", newName: "Group");
        }
    }
}
