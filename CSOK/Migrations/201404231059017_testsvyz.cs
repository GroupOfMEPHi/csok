namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testsvyz : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TestItemAnswers", "TestItem_ID", c => c.Int());
            CreateIndex("dbo.TestItemAnswers", "TestItem_ID");
            AddForeignKey("dbo.TestItemAnswers", "TestItem_ID", "dbo.TestItems", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TestItemAnswers", "TestItem_ID", "dbo.TestItems");
            DropIndex("dbo.TestItemAnswers", new[] { "TestItem_ID" });
            DropColumn("dbo.TestItemAnswers", "TestItem_ID");
        }
    }
}
