namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class alter_title : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Questions", "Title", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Questions", "Title", c => c.String(nullable: false));
        }
    }
}
