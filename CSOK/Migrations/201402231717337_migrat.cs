namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migrat : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Matrices", "Question_ID", "dbo.Questions");
            DropIndex("dbo.Matrices", new[] { "Question_ID" });
            AlterColumn("dbo.Matrices", "Question_ID", c => c.Int(nullable: false));
            CreateIndex("dbo.Matrices", "Question_ID");
            AddForeignKey("dbo.Matrices", "Question_ID", "dbo.Questions", "ID", cascadeDelete: true);
            //DropColumn("dbo.Matrices", "binary");
        }
        
        public override void Down()
        {
            //AddColumn("dbo.Matrices", "binary", c => c.String());
            DropForeignKey("dbo.Matrices", "Question_ID", "dbo.Questions");
            DropIndex("dbo.Matrices", new[] { "Question_ID" });
            AlterColumn("dbo.Matrices", "Question_ID", c => c.Int());
            CreateIndex("dbo.Matrices", "Question_ID");
            AddForeignKey("dbo.Matrices", "Question_ID", "dbo.Questions", "ID");
        }
    }
}
