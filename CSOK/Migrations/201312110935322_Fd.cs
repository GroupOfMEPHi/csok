namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnswerCorrectAnswers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AnsernDataTime = c.DateTime(nullable: false),
                        Body = c.String(),
                        CorrectAnswer_QuestionID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CorrectAnswers", t => t.CorrectAnswer_QuestionID)
                .Index(t => t.CorrectAnswer_QuestionID);
            
            CreateTable(
                "dbo.CorrectAnswers",
                c => new
                    {
                        QuestionID = c.Int(nullable: false),
                        Body = c.String(),
                    })
                .PrimaryKey(t => t.QuestionID)
                .ForeignKey("dbo.Questions", t => t.QuestionID, cascadeDelete: true)
                .Index(t => t.QuestionID);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Body = c.String(nullable: false),
                        HasPossibleAnswer = c.Boolean(nullable: false),
                        CheckBoxAnswer = c.Boolean(nullable: false),
                        RadioAnswer = c.Boolean(nullable: false),
                        isSelected = c.Boolean(nullable: false),
                        Tag = c.String(),
                        CreationDataTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Blocks",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(),
                        Question_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Questions", t => t.Question_ID, cascadeDelete: true)
                .Index(t => t.Question_ID);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        ImageId = c.Int(nullable: false, identity: true),
                        ImageType = c.String(),
                        ImageBinary = c.Binary(),
                        ImageLength = c.Int(nullable: false),
                        ImageName = c.String(),
                        Question_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ImageId)
                .ForeignKey("dbo.Questions", t => t.Question_ID)
                .Index(t => t.Question_ID);
            
            CreateTable(
                "dbo.Matrices",
                c => new
                    {
                        Matrix_Id = c.Int(nullable: false, identity: true),
                        nRows = c.Int(nullable: false),
                        nCols = c.Int(nullable: false),
                        Question_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Matrix_Id)
                .ForeignKey("dbo.Questions", t => t.Question_ID)
                .Index(t => t.Question_ID);
            
            CreateTable(
                "dbo.PossibleAnswers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(),
                        IsRight = c.Boolean(nullable: false),
                        Question_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Questions", t => t.Question_ID, cascadeDelete: true)
                .Index(t => t.Question_ID);
            
            CreateTable(
                "dbo.Answers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AnsernDataTime = c.DateTime(nullable: false),
                        IsRight = c.Boolean(nullable: false),
                        PossibleAnswer_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PossibleAnswers", t => t.PossibleAnswer_ID, cascadeDelete: true)
                .Index(t => t.PossibleAnswer_ID);
            
            CreateTable(
                "dbo.TestItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Subject = c.String(nullable: false),
                        CreationDateTime = c.DateTime(nullable: false),
                        Author_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.User", t => t.Author_UserId)
                .Index(t => t.Author_UserId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.TestItemQuestions",
                c => new
                    {
                        TestItem_ID = c.Int(nullable: false),
                        Question_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TestItem_ID, t.Question_ID })
                .ForeignKey("dbo.TestItems", t => t.TestItem_ID, cascadeDelete: true)
                .ForeignKey("dbo.Questions", t => t.Question_ID, cascadeDelete: true)
                .Index(t => t.TestItem_ID)
                .Index(t => t.Question_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TestItemQuestions", "Question_ID", "dbo.Questions");
            DropForeignKey("dbo.TestItemQuestions", "TestItem_ID", "dbo.TestItems");
            DropForeignKey("dbo.TestItems", "Author_UserId", "dbo.User");
            DropForeignKey("dbo.PossibleAnswers", "Question_ID", "dbo.Questions");
            DropForeignKey("dbo.Answers", "PossibleAnswer_ID", "dbo.PossibleAnswers");
            DropForeignKey("dbo.Matrices", "Question_ID", "dbo.Questions");
            DropForeignKey("dbo.Images", "Question_ID", "dbo.Questions");
            DropForeignKey("dbo.CorrectAnswers", "QuestionID", "dbo.Questions");
            DropForeignKey("dbo.Blocks", "Question_ID", "dbo.Questions");
            DropForeignKey("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID", "dbo.CorrectAnswers");
            DropIndex("dbo.TestItemQuestions", new[] { "Question_ID" });
            DropIndex("dbo.TestItemQuestions", new[] { "TestItem_ID" });
            DropIndex("dbo.TestItems", new[] { "Author_UserId" });
            DropIndex("dbo.PossibleAnswers", new[] { "Question_ID" });
            DropIndex("dbo.Answers", new[] { "PossibleAnswer_ID" });
            DropIndex("dbo.Matrices", new[] { "Question_ID" });
            DropIndex("dbo.Images", new[] { "Question_ID" });
            DropIndex("dbo.CorrectAnswers", new[] { "QuestionID" });
            DropIndex("dbo.Blocks", new[] { "Question_ID" });
            DropIndex("dbo.AnswerCorrectAnswers", new[] { "CorrectAnswer_QuestionID" });
            DropTable("dbo.TestItemQuestions");
            DropTable("dbo.User");
            DropTable("dbo.TestItems");
            DropTable("dbo.Answers");
            DropTable("dbo.PossibleAnswers");
            DropTable("dbo.Matrices");
            DropTable("dbo.Images");
            DropTable("dbo.Blocks");
            DropTable("dbo.Questions");
            DropTable("dbo.CorrectAnswers");
            DropTable("dbo.AnswerCorrectAnswers");
        }
    }
}
