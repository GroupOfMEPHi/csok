namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeRushStatModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RushSatistics", "DifficultQuestLvl", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.RushSatistics", "DifficultQuestLvl");
        }
    }
}
