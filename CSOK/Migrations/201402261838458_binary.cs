namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class binary : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Matrices", "matr_bin", c => c.Binary());
            DropColumn("dbo.Matrices", "binary");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Matrices", "binary", c => c.Binary());
            DropColumn("dbo.Matrices", "matr_bin");
        }
    }
}
