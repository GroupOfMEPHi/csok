namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeModelTestResults : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TestResults", "TestItemAnswer_ID", c => c.Int());
            AlterColumn("dbo.TestResults", "Question_ID", c => c.Int());
            CreateIndex("dbo.TestResults", "Question_ID");
            CreateIndex("dbo.TestResults", "TestItemAnswer_ID");
            AddForeignKey("dbo.TestResults", "Question_ID", "dbo.Questions", "ID");
            AddForeignKey("dbo.TestResults", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID");
            DropColumn("dbo.TestResults", "Student_ID");
            DropColumn("dbo.TestResults", "TestItem_ID");
            DropColumn("dbo.TestResults", "DatePass");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TestResults", "DatePass", c => c.DateTime(nullable: false));
            AddColumn("dbo.TestResults", "TestItem_ID", c => c.Int(nullable: false));
            AddColumn("dbo.TestResults", "Student_ID", c => c.Int(nullable: false));
            DropForeignKey("dbo.TestResults", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropForeignKey("dbo.TestResults", "Question_ID", "dbo.Questions");
            DropIndex("dbo.TestResults", new[] { "TestItemAnswer_ID" });
            DropIndex("dbo.TestResults", new[] { "Question_ID" });
            AlterColumn("dbo.TestResults", "Question_ID", c => c.Int(nullable: false));
            DropColumn("dbo.TestResults", "TestItemAnswer_ID");
        }
    }
}
