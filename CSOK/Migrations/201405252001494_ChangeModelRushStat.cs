namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeModelRushStat : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RushSatistics", "LvlTraining", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "Question_ID", c => c.Int());
            AddColumn("dbo.RushSatistics", "TestItem_ID", c => c.Int());
            CreateIndex("dbo.RushSatistics", "Question_ID");
            CreateIndex("dbo.RushSatistics", "TestItem_ID");
            AddForeignKey("dbo.RushSatistics", "Question_ID", "dbo.Questions", "ID");
            AddForeignKey("dbo.RushSatistics", "TestItem_ID", "dbo.TestItems", "ID");
            DropColumn("dbo.RushSatistics", "TestId");
            DropColumn("dbo.RushSatistics", "QuestionId");
            DropColumn("dbo.RushSatistics", "LvlTrainingNeg5_0");
            DropColumn("dbo.RushSatistics", "LvlTrainingNeg4_5");
            DropColumn("dbo.RushSatistics", "LvlTrainingNeg4_0");
            DropColumn("dbo.RushSatistics", "LvlTrainingNeg3_5");
            DropColumn("dbo.RushSatistics", "LvlTrainingNeg3_0");
            DropColumn("dbo.RushSatistics", "LvlTrainingNeg2_5");
            DropColumn("dbo.RushSatistics", "LvlTrainingNeg2_0");
            DropColumn("dbo.RushSatistics", "LvlTrainingNeg1_5");
            DropColumn("dbo.RushSatistics", "LvlTrainingNeg1_0");
            DropColumn("dbo.RushSatistics", "LvlTrainingNeg0_5");
            DropColumn("dbo.RushSatistics", "LvlTraining0_0");
            DropColumn("dbo.RushSatistics", "LvlTraining0_5");
            DropColumn("dbo.RushSatistics", "LvlTraining1_0");
            DropColumn("dbo.RushSatistics", "LvlTraining1_5");
            DropColumn("dbo.RushSatistics", "LvlTraining2_0");
            DropColumn("dbo.RushSatistics", "LvlTraining2_5");
            DropColumn("dbo.RushSatistics", "LvlTraining3_0");
            DropColumn("dbo.RushSatistics", "LvlTraining3_5");
            DropColumn("dbo.RushSatistics", "LvlTraining4_0");
            DropColumn("dbo.RushSatistics", "LvlTraining4_5");
            DropColumn("dbo.RushSatistics", "LvlTraining5_0");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RushSatistics", "LvlTraining5_0", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTraining4_5", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTraining4_0", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTraining3_5", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTraining3_0", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTraining2_5", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTraining2_0", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTraining1_5", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTraining1_0", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTraining0_5", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTraining0_0", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTrainingNeg0_5", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTrainingNeg1_0", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTrainingNeg1_5", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTrainingNeg2_0", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTrainingNeg2_5", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTrainingNeg3_0", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTrainingNeg3_5", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTrainingNeg4_0", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTrainingNeg4_5", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "LvlTrainingNeg5_0", c => c.Double(nullable: false));
            AddColumn("dbo.RushSatistics", "QuestionId", c => c.Int(nullable: false));
            AddColumn("dbo.RushSatistics", "TestId", c => c.Int(nullable: false));
            DropForeignKey("dbo.RushSatistics", "TestItem_ID", "dbo.TestItems");
            DropForeignKey("dbo.RushSatistics", "Question_ID", "dbo.Questions");
            DropIndex("dbo.RushSatistics", new[] { "TestItem_ID" });
            DropIndex("dbo.RushSatistics", new[] { "Question_ID" });
            DropColumn("dbo.RushSatistics", "TestItem_ID");
            DropColumn("dbo.RushSatistics", "Question_ID");
            DropColumn("dbo.RushSatistics", "LvlTraining");
        }
    }
}
