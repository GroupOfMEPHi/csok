namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class matrix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Matrices", "binary", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Matrices", "binary");
        }
    }
}
