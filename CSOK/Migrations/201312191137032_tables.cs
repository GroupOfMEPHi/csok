namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admin",
                c => new
                    {
                        UserID = c.Int(nullable: false),
                        Name = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.UserID);
            
            CreateTable(
                "dbo.Request",
                c => new
                    {
                        Num_of_request = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        YOB = c.DateTime(nullable: false),
                        Num_of_sub_faculty = c.Int(nullable: false),
                        Rank = c.String(),
                        Post = c.String(),
                        Num_of_record_book = c.Int(nullable: false),
                        Num_of_group = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Num_of_request);
            
            CreateTable(
                "dbo.Student",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        Num_of_record_book = c.Int(nullable: false),
                        Name = c.String(),
                        YOB = c.DateTime(nullable: false),
                        Num_of_sub_faculty = c.Int(nullable: false),
                        Num_of_group = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Teacher",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        Name = c.String(),
                        YOB = c.DateTime(nullable: false),
                        Num_of_sub_faculty = c.Int(nullable: false),
                        Rank = c.String(),
                        Post = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Teacher");
            DropTable("dbo.Student");
            DropTable("dbo.Request");
            DropTable("dbo.Admin");
        }
    }
}
