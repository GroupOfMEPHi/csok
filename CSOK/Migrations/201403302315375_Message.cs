namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Message : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MessageAttachments",
                c => new
                    {
                        AttachmentUrl = c.String(nullable: false, maxLength: 128),
                        Message_Id = c.Int(),
                    })
                .PrimaryKey(t => t.AttachmentUrl)
                .ForeignKey("dbo.Messages", t => t.Message_Id)
                .Index(t => t.Message_Id);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Subject = c.String(),
                        Text = c.String(),
                        TimeSend = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserMessages",
                c => new
                    {
                        UserMessageId = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        AreSender = c.Boolean(nullable: false),
                        AreDeleted = c.Boolean(nullable: false),
                        DeletedTime = c.DateTime(),
                        AreRead = c.Boolean(nullable: false),
                        Message_Id = c.Int(),
                    })
                .PrimaryKey(t => t.UserMessageId)
                .ForeignKey("dbo.Messages", t => t.Message_Id)
                .Index(t => t.Message_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserMessages", "Message_Id", "dbo.Messages");
            DropForeignKey("dbo.MessageAttachments", "Message_Id", "dbo.Messages");
            DropIndex("dbo.UserMessages", new[] { "Message_Id" });
            DropIndex("dbo.MessageAttachments", new[] { "Message_Id" });
            DropTable("dbo.UserMessages");
            DropTable("dbo.Messages");
            DropTable("dbo.MessageAttachments");
        }
    }
}
