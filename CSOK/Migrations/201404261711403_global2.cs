namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class global2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID", "dbo.CorrectAnswers");
            DropForeignKey("dbo.AnswerBlocks", "Block_ID", "dbo.Blocks");
            DropForeignKey("dbo.Questions", "Theme_Id", "dbo.Themes");
            DropIndex("dbo.AnswerCorrectAnswers", new[] { "CorrectAnswer_QuestionID" });
            DropIndex("dbo.AnswerBlocks", new[] { "Block_ID" });
            DropIndex("dbo.Questions", new[] { "Theme_Id" });
            RenameColumn(table: "dbo.Structures", name: "DisciplineId", newName: "Discipline_Id");
            RenameColumn(table: "dbo.Themes", name: "StructureId", newName: "Structure_Id");
            AlterColumn("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID", c => c.Int(nullable: false));
            AlterColumn("dbo.Questions", "Theme_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.AnswerBlocks", "Block_ID", c => c.Int(nullable: false));
            CreateIndex("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID");
            CreateIndex("dbo.AnswerBlocks", "Block_ID");
            CreateIndex("dbo.Questions", "Theme_Id");
            AddForeignKey("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID", "dbo.CorrectAnswers", "QuestionID", cascadeDelete: true);

            AddForeignKey("dbo.AnswerBlocks", "Block_ID", "dbo.Blocks", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Questions", "Theme_Id", "dbo.Themes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Questions", "Theme_Id", "dbo.Themes");
            DropForeignKey("dbo.AnswerBlocks", "Block_ID", "dbo.Blocks");
            DropForeignKey("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID", "dbo.CorrectAnswers");
            DropIndex("dbo.Questions", new[] { "Theme_Id" });
            DropIndex("dbo.AnswerBlocks", new[] { "Block_ID" });
            DropIndex("dbo.AnswerCorrectAnswers", new[] { "CorrectAnswer_QuestionID" });
            AlterColumn("dbo.AnswerBlocks", "Block_ID", c => c.Int());
            AlterColumn("dbo.Questions", "Theme_Id", c => c.Int());
            AlterColumn("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID", c => c.Int());
            RenameColumn(table: "dbo.Themes", name: "Structure_Id", newName: "StructureId");
            RenameColumn(table: "dbo.Structures", name: "Discipline_Id", newName: "DisciplineId");
            CreateIndex("dbo.Questions", "Theme_Id");
            CreateIndex("dbo.AnswerBlocks", "Block_ID");
            CreateIndex("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID");
            AddForeignKey("dbo.Questions", "Theme_Id", "dbo.Themes", "Id");
            AddForeignKey("dbo.AnswerBlocks", "Block_ID", "dbo.Blocks", "ID");
            AddForeignKey("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID", "dbo.CorrectAnswers", "QuestionID");
        }
    }
}
