namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testmode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TestItems", "TestMode", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TestItems", "TestMode");
        }
    }
}
