namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test1 : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.Matrices", "binary", c => c.Binary());
            DropColumn("dbo.Matrices", "binary2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Matrices", "binary2", c => c.Binary());
            //DropColumn("dbo.Matrices", "binary");
        }
    }
}
