// <auto-generated />
namespace CSOK.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.1-21010")]
    public sealed partial class answer_matrix : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(answer_matrix));
        
        string IMigrationMetadata.Id
        {
            get { return "201404210541224_answer_matrix"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
