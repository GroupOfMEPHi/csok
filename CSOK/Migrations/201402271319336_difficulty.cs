namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class difficulty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Questions", "difficulty", c => c.Single(nullable: false));
            
        }
        
        public override void Down()
        {
            
            DropColumn("dbo.Questions", "difficulty");
        }
    }
}
