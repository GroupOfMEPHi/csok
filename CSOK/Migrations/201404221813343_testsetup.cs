namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testsetup : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TestItemAnswers", "SpentTime", c => c.String());
            AddColumn("dbo.TestItems", "Description", c => c.String());
            AddColumn("dbo.TestItems", "OrderOfQuestion", c => c.Boolean(nullable: false));
            AddColumn("dbo.TestItems", "Estimation", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TestItems", "Estimation");
            DropColumn("dbo.TestItems", "OrderOfQuestion");
            DropColumn("dbo.TestItems", "Description");
            DropColumn("dbo.TestItemAnswers", "SpentTime");
        }
    }
}
