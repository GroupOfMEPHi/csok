namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class request : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Request", "UserName", c => c.String());
            AddColumn("dbo.Request", "Password", c => c.String());
            AddColumn("dbo.Request", "Email", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Request", "Email");
            DropColumn("dbo.Request", "Password");
            DropColumn("dbo.Request", "UserName");
        }
    }
}
