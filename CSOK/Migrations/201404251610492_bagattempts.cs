namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class bagattempts : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.TestItemAnswers", "Attempts");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TestItemAnswers", "Attempts", c => c.Int(nullable: false));
        }
    }
}
