namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cascade_for_matrices : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AnswerMatrices", "Matrix_Matrix_Id", "dbo.Matrices");
            DropIndex("dbo.AnswerMatrices", new[] { "Matrix_Matrix_Id" });
            AlterColumn("dbo.AnswerMatrices", "Matrix_Matrix_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.AnswerMatrices", "Matrix_Matrix_Id");
            AddForeignKey("dbo.AnswerMatrices", "Matrix_Matrix_Id", "dbo.Matrices", "Matrix_Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnswerMatrices", "Matrix_Matrix_Id", "dbo.Matrices");
            DropIndex("dbo.AnswerMatrices", new[] { "Matrix_Matrix_Id" });
            AlterColumn("dbo.AnswerMatrices", "Matrix_Matrix_Id", c => c.Int());
            CreateIndex("dbo.AnswerMatrices", "Matrix_Matrix_Id");
            AddForeignKey("dbo.AnswerMatrices", "Matrix_Matrix_Id", "dbo.Matrices", "Matrix_Id");
        }
    }
}
