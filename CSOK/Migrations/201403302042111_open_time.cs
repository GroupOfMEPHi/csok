namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class open_time : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TestItems", "OpeningDateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TestItems", "OpeningDateTime");
        }
    }
}
