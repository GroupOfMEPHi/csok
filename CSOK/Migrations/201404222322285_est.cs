namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class est : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Estimations",
                c => new
                    {
                        EstID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.EstID);
            
            AddColumn("dbo.TestItems", "Estimation_EstID", c => c.Int());
            CreateIndex("dbo.TestItems", "Estimation_EstID");
            AddForeignKey("dbo.TestItems", "Estimation_EstID", "dbo.Estimations", "EstID");
            DropColumn("dbo.TestItems", "Estimation");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TestItems", "Estimation", c => c.Int(nullable: false));
            DropForeignKey("dbo.TestItems", "Estimation_EstID", "dbo.Estimations");
            DropIndex("dbo.TestItems", new[] { "Estimation_EstID" });
            DropColumn("dbo.TestItems", "Estimation_EstID");
            DropTable("dbo.Estimations");
        }
    }
}
