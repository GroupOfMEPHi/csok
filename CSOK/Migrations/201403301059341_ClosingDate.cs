namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClosingDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TestItems", "ClosingDateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TestItems", "ClosingDateTime");
        }
    }
}
