namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testitem_deleting : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TestItemAnswers", "TestItem_ID", "dbo.TestItems");
            DropIndex("dbo.TestItemAnswers", new[] { "TestItem_ID" });
            AlterColumn("dbo.TestItemAnswers", "TestItem_ID", c => c.Int(nullable: false));
            CreateIndex("dbo.TestItemAnswers", "TestItem_ID");
            AddForeignKey("dbo.TestItemAnswers", "TestItem_ID", "dbo.TestItems", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TestItemAnswers", "TestItem_ID", "dbo.TestItems");
            DropIndex("dbo.TestItemAnswers", new[] { "TestItem_ID" });
            AlterColumn("dbo.TestItemAnswers", "TestItem_ID", c => c.Int());
            CreateIndex("dbo.TestItemAnswers", "TestItem_ID");
            AddForeignKey("dbo.TestItemAnswers", "TestItem_ID", "dbo.TestItems", "ID");
        }
    }
}
