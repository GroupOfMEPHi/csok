namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tiatt : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AnswerCorrectAnswers", "TestID", c => c.Int(nullable: false));
            AddColumn("dbo.AnswerCorrectAnswers", "Attempt", c => c.Int(nullable: false));
            AddColumn("dbo.AnswerBlocks", "TestID", c => c.Int(nullable: false));
            AddColumn("dbo.AnswerBlocks", "Attempt", c => c.Int(nullable: false));
            AddColumn("dbo.Answers", "TestID", c => c.Int(nullable: false));
            AddColumn("dbo.Answers", "Attempt", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Answers", "Attempt");
            DropColumn("dbo.Answers", "TestID");
            DropColumn("dbo.AnswerBlocks", "Attempt");
            DropColumn("dbo.AnswerBlocks", "TestID");
            DropColumn("dbo.AnswerCorrectAnswers", "Attempt");
            DropColumn("dbo.AnswerCorrectAnswers", "TestID");
        }
    }
}
