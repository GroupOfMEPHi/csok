namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testcascadedelete2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AnswerBlocks", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropForeignKey("dbo.AnswerExcessBlocks", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropForeignKey("dbo.AnswerMatrices", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropForeignKey("dbo.AnswerPossibleAnswers", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropForeignKey("dbo.AnswerCorrectAnswers", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropIndex("dbo.AnswerBlocks", new[] { "TestItemAnswer_ID" });
            DropIndex("dbo.AnswerExcessBlocks", new[] { "TestItemAnswer_ID" });
            DropIndex("dbo.AnswerMatrices", new[] { "TestItemAnswer_ID" });
            DropIndex("dbo.AnswerPossibleAnswers", new[] { "TestItemAnswer_ID" });
            DropIndex("dbo.AnswerCorrectAnswers", new[] { "TestItemAnswer_ID" });
            AlterColumn("dbo.AnswerCorrectAnswers", "TestItemAnswer_ID", c => c.Int(nullable: false));
            AlterColumn("dbo.AnswerBlocks", "TestItemAnswer_ID", c => c.Int(nullable: false));
            AlterColumn("dbo.AnswerExcessBlocks", "TestItemAnswer_ID", c => c.Int(nullable: false));
            AlterColumn("dbo.AnswerMatrices", "TestItemAnswer_ID", c => c.Int(nullable: false));
            AlterColumn("dbo.AnswerPossibleAnswers", "TestItemAnswer_ID", c => c.Int(nullable: false));
            CreateIndex("dbo.AnswerBlocks", "TestItemAnswer_ID");
            CreateIndex("dbo.AnswerExcessBlocks", "TestItemAnswer_ID");
            CreateIndex("dbo.AnswerMatrices", "TestItemAnswer_ID");
            CreateIndex("dbo.AnswerPossibleAnswers", "TestItemAnswer_ID");
            CreateIndex("dbo.AnswerCorrectAnswers", "TestItemAnswer_ID");
            AddForeignKey("dbo.AnswerBlocks", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID", cascadeDelete: true);
            AddForeignKey("dbo.AnswerExcessBlocks", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID", cascadeDelete: true);
            AddForeignKey("dbo.AnswerMatrices", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID", cascadeDelete: true);
            AddForeignKey("dbo.AnswerPossibleAnswers", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID", cascadeDelete: true);
            AddForeignKey("dbo.AnswerCorrectAnswers", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnswerCorrectAnswers", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropForeignKey("dbo.AnswerPossibleAnswers", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropForeignKey("dbo.AnswerMatrices", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropForeignKey("dbo.AnswerExcessBlocks", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropForeignKey("dbo.AnswerBlocks", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropIndex("dbo.AnswerCorrectAnswers", new[] { "TestItemAnswer_ID" });
            DropIndex("dbo.AnswerPossibleAnswers", new[] { "TestItemAnswer_ID" });
            DropIndex("dbo.AnswerMatrices", new[] { "TestItemAnswer_ID" });
            DropIndex("dbo.AnswerExcessBlocks", new[] { "TestItemAnswer_ID" });
            DropIndex("dbo.AnswerBlocks", new[] { "TestItemAnswer_ID" });
            AlterColumn("dbo.AnswerPossibleAnswers", "TestItemAnswer_ID", c => c.Int());
            AlterColumn("dbo.AnswerMatrices", "TestItemAnswer_ID", c => c.Int());
            AlterColumn("dbo.AnswerExcessBlocks", "TestItemAnswer_ID", c => c.Int());
            AlterColumn("dbo.AnswerBlocks", "TestItemAnswer_ID", c => c.Int());
            AlterColumn("dbo.AnswerCorrectAnswers", "TestItemAnswer_ID", c => c.Int());
            CreateIndex("dbo.AnswerCorrectAnswers", "TestItemAnswer_ID");
            CreateIndex("dbo.AnswerPossibleAnswers", "TestItemAnswer_ID");
            CreateIndex("dbo.AnswerMatrices", "TestItemAnswer_ID");
            CreateIndex("dbo.AnswerExcessBlocks", "TestItemAnswer_ID");
            CreateIndex("dbo.AnswerBlocks", "TestItemAnswer_ID");
            AddForeignKey("dbo.AnswerCorrectAnswers", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID");
            AddForeignKey("dbo.AnswerPossibleAnswers", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID");
            AddForeignKey("dbo.AnswerMatrices", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID");
            AddForeignKey("dbo.AnswerExcessBlocks", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID");
            AddForeignKey("dbo.AnswerBlocks", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID");
        }
    }
}
