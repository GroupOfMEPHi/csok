namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeModelRushStat_AddDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RushSatistics", "CalcDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.RushSatistics", "CalcDate");
        }
    }
}
