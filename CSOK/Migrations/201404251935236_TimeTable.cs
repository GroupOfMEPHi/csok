namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TimeTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TimeTables",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        day = c.String(),
                        time = c.String(),
                        week = c.String(),
                        lesson = c.String(),
                        typeless = c.String(),
                        lector = c.String(),
                        aud = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TimeTables");
        }
    }
}
