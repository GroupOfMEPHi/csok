namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class news : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.News",
                c => new
                    {
                        Num_of_news = c.Int(nullable: false, identity: true),
                        DateTimeOfCreation = c.DateTime(nullable: false),
                        ID = c.Int(nullable: false),
                        Title = c.String(),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.Num_of_news);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.News");
        }
    }
}
