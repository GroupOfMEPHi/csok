namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cascade_excess_blocks : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Excess_Block", "Question_ID", "dbo.Questions");
            DropIndex("dbo.Excess_Block", new[] { "Question_ID" });
            AlterColumn("dbo.Excess_Block", "Question_ID", c => c.Int(nullable: false));
            CreateIndex("dbo.Excess_Block", "Question_ID");
            AddForeignKey("dbo.Excess_Block", "Question_ID", "dbo.Questions", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Excess_Block", "Question_ID", "dbo.Questions");
            DropIndex("dbo.Excess_Block", new[] { "Question_ID" });
            AlterColumn("dbo.Excess_Block", "Question_ID", c => c.Int());
            CreateIndex("dbo.Excess_Block", "Question_ID");
            AddForeignKey("dbo.Excess_Block", "Question_ID", "dbo.Questions", "ID");
        }
    }
}
