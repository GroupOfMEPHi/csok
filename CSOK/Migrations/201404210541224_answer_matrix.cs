namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class answer_matrix : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnswerMatrices",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuthorID = c.Int(nullable: false),
                        nRows = c.Int(nullable: false),
                        nCols = c.Int(nullable: false),
                        matr_bin = c.Binary(),
                        AnsernDataTime = c.DateTime(nullable: false),
                        TestID = c.Int(nullable: false),
                        Attempt = c.Int(nullable: false),
                        Matrix_Matrix_Id = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Matrices", t => t.Matrix_Matrix_Id)
                .Index(t => t.Matrix_Matrix_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnswerMatrices", "Matrix_Matrix_Id", "dbo.Matrices");
            DropIndex("dbo.AnswerMatrices", new[] { "Matrix_Matrix_Id" });
            DropTable("dbo.AnswerMatrices");
        }
    }
}
