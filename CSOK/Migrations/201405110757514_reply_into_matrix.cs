namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class reply_into_matrix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Questions", "MatrixAnswer", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Questions", "MatrixAnswer");
        }
    }
}
