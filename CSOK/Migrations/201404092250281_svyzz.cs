namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class svyzz : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Questions", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropIndex("dbo.Questions", new[] { "TestItemAnswer_ID" });
            AddColumn("dbo.TestItemAnswers", "Question_ID", c => c.Int());
            CreateIndex("dbo.TestItemAnswers", "Question_ID");
            AddForeignKey("dbo.TestItemAnswers", "Question_ID", "dbo.Questions", "ID");
            DropColumn("dbo.Questions", "TestItemAnswer_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Questions", "TestItemAnswer_ID", c => c.Int());
            DropForeignKey("dbo.TestItemAnswers", "Question_ID", "dbo.Questions");
            DropIndex("dbo.TestItemAnswers", new[] { "Question_ID" });
            DropColumn("dbo.TestItemAnswers", "Question_ID");
            CreateIndex("dbo.Questions", "TestItemAnswer_ID");
            AddForeignKey("dbo.Questions", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID");
        }
    }
}
