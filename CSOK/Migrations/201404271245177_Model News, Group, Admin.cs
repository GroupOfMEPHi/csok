namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelNewsGroupAdmin : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.News", "Author_UserId", c => c.Int());
            CreateIndex("dbo.News", "Author_UserId");
            AddForeignKey("dbo.News", "Author_UserId", "dbo.User", "UserId");
            DropColumn("dbo.Admin", "Email");
            DropColumn("dbo.Student", "Num_of_group");
            DropColumn("dbo.News", "ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.News", "ID", c => c.Int(nullable: false));
            AddColumn("dbo.Student", "Num_of_group", c => c.String());
            AddColumn("dbo.Admin", "Email", c => c.String());
            DropForeignKey("dbo.News", "Author_UserId", "dbo.User");
            DropIndex("dbo.News", new[] { "Author_UserId" });
            DropColumn("dbo.News", "Author_UserId");
        }
    }
}
