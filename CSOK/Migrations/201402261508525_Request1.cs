namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Request1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Request", "Num_of_group", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Request", "Num_of_group", c => c.Int(nullable: false));
        }
    }
}
