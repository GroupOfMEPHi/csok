namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class try1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID", "dbo.CorrectAnswers");
            DropForeignKey("dbo.AnswerBlocks", "Block_ID", "dbo.Blocks");
            DropForeignKey("dbo.Structures", "DisciplineId", "dbo.Disciplines");
            DropForeignKey("dbo.Themes", "StructureId", "dbo.Structures");
            DropIndex("dbo.AnswerCorrectAnswers", new[] { "CorrectAnswer_QuestionID" });
            DropIndex("dbo.AnswerBlocks", new[] { "Block_ID" });
            DropIndex("dbo.Structures", new[] { "DisciplineId" });
            DropIndex("dbo.Themes", new[] { "StructureId" });
            RenameColumn(table: "dbo.Structures", name: "DisciplineId", newName: "Discipline_Id");
            RenameColumn(table: "dbo.Themes", name: "StructureId", newName: "Structure_Id");
            AlterColumn("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID", c => c.Int(nullable: false));
            AlterColumn("dbo.AnswerBlocks", "Block_ID", c => c.Int(nullable: false));
            AlterColumn("dbo.Structures", "Discipline_Id", c => c.Int());
            AlterColumn("dbo.Themes", "Structure_Id", c => c.Int());
            CreateIndex("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID");
            CreateIndex("dbo.AnswerBlocks", "Block_ID");
            CreateIndex("dbo.Structures", "Discipline_Id");
            CreateIndex("dbo.Themes", "Structure_Id");
            AddForeignKey("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID", "dbo.CorrectAnswers", "QuestionID", cascadeDelete: true);
            AddForeignKey("dbo.AnswerBlocks", "Block_ID", "dbo.Blocks", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Structures", "Discipline_Id", "dbo.Disciplines", "Id");
            AddForeignKey("dbo.Themes", "Structure_Id", "dbo.Structures", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Themes", "Structure_Id", "dbo.Structures");
            DropForeignKey("dbo.Structures", "Discipline_Id", "dbo.Disciplines");
            DropForeignKey("dbo.AnswerBlocks", "Block_ID", "dbo.Blocks");
            DropForeignKey("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID", "dbo.CorrectAnswers");
            DropIndex("dbo.Themes", new[] { "Structure_Id" });
            DropIndex("dbo.Structures", new[] { "Discipline_Id" });
            DropIndex("dbo.AnswerBlocks", new[] { "Block_ID" });
            DropIndex("dbo.AnswerCorrectAnswers", new[] { "CorrectAnswer_QuestionID" });
            AlterColumn("dbo.Themes", "Structure_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Structures", "Discipline_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.AnswerBlocks", "Block_ID", c => c.Int());
            AlterColumn("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID", c => c.Int());
            RenameColumn(table: "dbo.Themes", name: "Structure_Id", newName: "StructureId");
            RenameColumn(table: "dbo.Structures", name: "Discipline_Id", newName: "DisciplineId");
            CreateIndex("dbo.Themes", "StructureId");
            CreateIndex("dbo.Structures", "DisciplineId");
            CreateIndex("dbo.AnswerBlocks", "Block_ID");
            CreateIndex("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID");
            AddForeignKey("dbo.Themes", "StructureId", "dbo.Structures", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Structures", "DisciplineId", "dbo.Disciplines", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AnswerBlocks", "Block_ID", "dbo.Blocks", "ID");
            AddForeignKey("dbo.AnswerCorrectAnswers", "CorrectAnswer_QuestionID", "dbo.CorrectAnswers", "QuestionID");
        }
    }
}
