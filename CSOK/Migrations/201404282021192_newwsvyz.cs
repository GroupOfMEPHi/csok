namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newwsvyz : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AnswerBlocks", "Author_UserId", "dbo.User");
            DropForeignKey("dbo.Answers", "Author_UserId", "dbo.User");
            DropIndex("dbo.AnswerBlocks", new[] { "Author_UserId" });
            DropIndex("dbo.Answers", new[] { "Author_UserId" });
            AddColumn("dbo.AnswerBlocks", "TestItemAnswer_ID", c => c.Int());
            AddColumn("dbo.AnswerExcessBlocks", "TestItemAnswer_ID", c => c.Int());
            AddColumn("dbo.AnswerMatrices", "TestItemAnswer_ID", c => c.Int());
            AddColumn("dbo.Answers", "TestItemAnswer_ID", c => c.Int());
            CreateIndex("dbo.AnswerBlocks", "TestItemAnswer_ID");
            CreateIndex("dbo.AnswerExcessBlocks", "TestItemAnswer_ID");
            CreateIndex("dbo.AnswerMatrices", "TestItemAnswer_ID");
            CreateIndex("dbo.Answers", "TestItemAnswer_ID");
            AddForeignKey("dbo.AnswerBlocks", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID");
            AddForeignKey("dbo.AnswerExcessBlocks", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID");
            AddForeignKey("dbo.AnswerMatrices", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID");
            AddForeignKey("dbo.Answers", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID");
            DropColumn("dbo.AnswerBlocks", "AnsernDataTime");
            DropColumn("dbo.AnswerBlocks", "TestID");
            DropColumn("dbo.AnswerBlocks", "Attempt");
            DropColumn("dbo.AnswerBlocks", "Author_UserId");
            DropColumn("dbo.AnswerExcessBlocks", "AuthorID");
            DropColumn("dbo.AnswerExcessBlocks", "AnsernDataTime");
            DropColumn("dbo.AnswerExcessBlocks", "TestID");
            DropColumn("dbo.AnswerExcessBlocks", "Attempt");
            DropColumn("dbo.AnswerMatrices", "AuthorID");
            DropColumn("dbo.AnswerMatrices", "AnsernDataTime");
            DropColumn("dbo.AnswerMatrices", "TestID");
            DropColumn("dbo.AnswerMatrices", "Attempt");
            DropColumn("dbo.Answers", "AnsernDataTime");
            DropColumn("dbo.Answers", "TestID");
            DropColumn("dbo.Answers", "Attempt");
            DropColumn("dbo.Answers", "Author_UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Answers", "Author_UserId", c => c.Int());
            AddColumn("dbo.Answers", "Attempt", c => c.Int(nullable: false));
            AddColumn("dbo.Answers", "TestID", c => c.Int(nullable: false));
            AddColumn("dbo.Answers", "AnsernDataTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.AnswerMatrices", "Attempt", c => c.Int(nullable: false));
            AddColumn("dbo.AnswerMatrices", "TestID", c => c.Int(nullable: false));
            AddColumn("dbo.AnswerMatrices", "AnsernDataTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.AnswerMatrices", "AuthorID", c => c.Int(nullable: false));
            AddColumn("dbo.AnswerExcessBlocks", "Attempt", c => c.Int(nullable: false));
            AddColumn("dbo.AnswerExcessBlocks", "TestID", c => c.Int(nullable: false));
            AddColumn("dbo.AnswerExcessBlocks", "AnsernDataTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.AnswerExcessBlocks", "AuthorID", c => c.Int(nullable: false));
            AddColumn("dbo.AnswerBlocks", "Author_UserId", c => c.Int());
            AddColumn("dbo.AnswerBlocks", "Attempt", c => c.Int(nullable: false));
            AddColumn("dbo.AnswerBlocks", "TestID", c => c.Int(nullable: false));
            AddColumn("dbo.AnswerBlocks", "AnsernDataTime", c => c.DateTime(nullable: false));
            DropForeignKey("dbo.Answers", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropForeignKey("dbo.AnswerMatrices", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropForeignKey("dbo.AnswerExcessBlocks", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropForeignKey("dbo.AnswerBlocks", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropIndex("dbo.Answers", new[] { "TestItemAnswer_ID" });
            DropIndex("dbo.AnswerMatrices", new[] { "TestItemAnswer_ID" });
            DropIndex("dbo.AnswerExcessBlocks", new[] { "TestItemAnswer_ID" });
            DropIndex("dbo.AnswerBlocks", new[] { "TestItemAnswer_ID" });
            DropColumn("dbo.Answers", "TestItemAnswer_ID");
            DropColumn("dbo.AnswerMatrices", "TestItemAnswer_ID");
            DropColumn("dbo.AnswerExcessBlocks", "TestItemAnswer_ID");
            DropColumn("dbo.AnswerBlocks", "TestItemAnswer_ID");
            CreateIndex("dbo.Answers", "Author_UserId");
            CreateIndex("dbo.AnswerBlocks", "Author_UserId");
            AddForeignKey("dbo.Answers", "Author_UserId", "dbo.User", "UserId");
            AddForeignKey("dbo.AnswerBlocks", "Author_UserId", "dbo.User", "UserId");
        }
    }
}
