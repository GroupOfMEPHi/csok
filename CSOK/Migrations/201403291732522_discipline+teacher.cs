namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class disciplineteacher : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Disciplines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Structures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DisciplineId = c.Int(nullable: false),
                        Name = c.String(),
                        Teacher_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Disciplines", t => t.DisciplineId, cascadeDelete: true)
                .ForeignKey("dbo.Teacher", t => t.Teacher_UserId)
                .Index(t => t.DisciplineId)
                .Index(t => t.Teacher_UserId);
            
            CreateTable(
                "dbo.Themes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StructureId = c.Int(nullable: false),
                        Name = c.String(),
                        Literature = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Structures", t => t.StructureId, cascadeDelete: true)
                .Index(t => t.StructureId);
            
            AddColumn("dbo.Questions", "Theme_Id", c => c.Int());
            CreateIndex("dbo.Questions", "Theme_Id");
            AddForeignKey("dbo.Questions", "Theme_Id", "dbo.Themes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Themes", "StructureId", "dbo.Structures");
            DropForeignKey("dbo.Questions", "Theme_Id", "dbo.Themes");
            DropForeignKey("dbo.Structures", "Teacher_UserId", "dbo.Teacher");
            DropForeignKey("dbo.Structures", "DisciplineId", "dbo.Disciplines");
            DropIndex("dbo.Themes", new[] { "StructureId" });
            DropIndex("dbo.Questions", new[] { "Theme_Id" });
            DropIndex("dbo.Structures", new[] { "Teacher_UserId" });
            DropIndex("dbo.Structures", new[] { "DisciplineId" });
            DropColumn("dbo.Questions", "Theme_Id");
            DropTable("dbo.Themes");
            DropTable("dbo.Structures");
            DropTable("dbo.Disciplines");
        }
    }
}
