namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testanswer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TestItemAnswers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuthorID = c.Int(nullable: false),
                        Subject = c.String(),
                        CreationDateTime = c.DateTime(nullable: false),
                        ClosingDateTime = c.DateTime(nullable: false),
                        OpeningDateTime = c.DateTime(nullable: false),
                        Time = c.Int(nullable: false),
                        Paging = c.Int(nullable: false),
                        Attempts = c.Int(nullable: false),
                        CurrentPasses = c.Boolean(nullable: false),
                        StartDateTime = c.DateTime(nullable: false),
                        Discipline_Id = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Disciplines", t => t.Discipline_Id)
                .Index(t => t.Discipline_Id);
            
            AddColumn("dbo.Questions", "TestItemAnswer_ID", c => c.Int());
            AddColumn("dbo.TestItems", "Attempts", c => c.Int(nullable: false));
            CreateIndex("dbo.Questions", "TestItemAnswer_ID");
            AddForeignKey("dbo.Questions", "TestItemAnswer_ID", "dbo.TestItemAnswers", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Questions", "TestItemAnswer_ID", "dbo.TestItemAnswers");
            DropForeignKey("dbo.TestItemAnswers", "Discipline_Id", "dbo.Disciplines");
            DropIndex("dbo.Questions", new[] { "TestItemAnswer_ID" });
            DropIndex("dbo.TestItemAnswers", new[] { "Discipline_Id" });
            DropColumn("dbo.TestItems", "Attempts");
            DropColumn("dbo.Questions", "TestItemAnswer_ID");
            DropTable("dbo.TestItemAnswers");
        }
    }
}
