namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tsuuuuu : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AnswerCorrectAnswers", "Author_UserId", c => c.Int());
            AddColumn("dbo.Questions", "Author_UserId", c => c.Int());
            AddColumn("dbo.AnswerBlocks", "Author_UserId", c => c.Int());
            AddColumn("dbo.Answers", "Author_UserId", c => c.Int());
            AddColumn("dbo.TestItemAnswers", "Author_UserId", c => c.Int());
            CreateIndex("dbo.AnswerCorrectAnswers", "Author_UserId");
            CreateIndex("dbo.Questions", "Author_UserId");
            CreateIndex("dbo.AnswerBlocks", "Author_UserId");
            CreateIndex("dbo.Answers", "Author_UserId");
            CreateIndex("dbo.TestItemAnswers", "Author_UserId");
            AddForeignKey("dbo.AnswerCorrectAnswers", "Author_UserId", "dbo.User", "UserId");
            AddForeignKey("dbo.Questions", "Author_UserId", "dbo.User", "UserId");
            AddForeignKey("dbo.AnswerBlocks", "Author_UserId", "dbo.User", "UserId");
            AddForeignKey("dbo.Answers", "Author_UserId", "dbo.User", "UserId");
            AddForeignKey("dbo.TestItemAnswers", "Author_UserId", "dbo.User", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TestItemAnswers", "Author_UserId", "dbo.User");
            DropForeignKey("dbo.Answers", "Author_UserId", "dbo.User");
            DropForeignKey("dbo.AnswerBlocks", "Author_UserId", "dbo.User");
            DropForeignKey("dbo.Questions", "Author_UserId", "dbo.User");
            DropForeignKey("dbo.AnswerCorrectAnswers", "Author_UserId", "dbo.User");
            DropIndex("dbo.TestItemAnswers", new[] { "Author_UserId" });
            DropIndex("dbo.Answers", new[] { "Author_UserId" });
            DropIndex("dbo.AnswerBlocks", new[] { "Author_UserId" });
            DropIndex("dbo.Questions", new[] { "Author_UserId" });
            DropIndex("dbo.AnswerCorrectAnswers", new[] { "Author_UserId" });
            DropColumn("dbo.TestItemAnswers", "Author_UserId");
            DropColumn("dbo.Answers", "Author_UserId");
            DropColumn("dbo.AnswerBlocks", "Author_UserId");
            DropColumn("dbo.Questions", "Author_UserId");
            DropColumn("dbo.AnswerCorrectAnswers", "Author_UserId");
        }
    }
}
