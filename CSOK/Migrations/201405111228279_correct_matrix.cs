namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class correct_matrix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Matrices", "correct_matr_bin", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Matrices", "correct_matr_bin");
        }
    }
}
