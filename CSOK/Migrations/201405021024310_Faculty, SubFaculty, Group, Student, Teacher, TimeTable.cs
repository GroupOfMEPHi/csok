namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FacultySubFacultyGroupStudentTeacherTimeTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SubFaculties",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                        Faculty_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Faculties", t => t.Faculty_Id)
                .Index(t => t.Faculty_Id);
            
            CreateTable(
                "dbo.Faculties",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        ShortName = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Teacher", "SubFaculty_Id", c => c.Int());
            AddColumn("dbo.Group", "SubFaculty_Id", c => c.Int());
            //AddColumn("dbo.News", "Author_UserId", c => c.Int());
            AddColumn("dbo.TimeTables", "Group_GroupId", c => c.Int());
            AddColumn("dbo.TimeTables", "lesson_Id", c => c.Int());
            AddColumn("dbo.TimeTables", "Teacher_UserId", c => c.Int());
            CreateIndex("dbo.Group", "SubFaculty_Id");
            CreateIndex("dbo.Teacher", "SubFaculty_Id");
            //CreateIndex("dbo.News", "Author_UserId");
            CreateIndex("dbo.TimeTables", "Group_GroupId");
            CreateIndex("dbo.TimeTables", "lesson_Id");
            CreateIndex("dbo.TimeTables", "Teacher_UserId");
            AddForeignKey("dbo.Group", "SubFaculty_Id", "dbo.SubFaculties", "Id");
            AddForeignKey("dbo.Teacher", "SubFaculty_Id", "dbo.SubFaculties", "Id");
            //AddForeignKey("dbo.News", "Author_UserId", "dbo.User", "UserId");
            AddForeignKey("dbo.TimeTables", "Group_GroupId", "dbo.Group", "GroupId");
            AddForeignKey("dbo.TimeTables", "lesson_Id", "dbo.Disciplines", "Id");
            AddForeignKey("dbo.TimeTables", "Teacher_UserId", "dbo.Teacher", "UserId");
            //DropColumn("dbo.Admin", "Email");
            DropColumn("dbo.Teacher", "Num_of_sub_faculty");
            DropColumn("dbo.Group", "NumberOfSubFaculty");
            DropColumn("dbo.Student", "Num_of_sub_faculty");
            //DropColumn("dbo.Student", "Num_of_group");
            //DropColumn("dbo.News", "ID");
            DropColumn("dbo.TimeTables", "lesson");
            DropColumn("dbo.TimeTables", "lector");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TimeTables", "lector", c => c.String());
            AddColumn("dbo.TimeTables", "lesson", c => c.String());
            //AddColumn("dbo.News", "ID", c => c.Int(nullable: false));
            //AddColumn("dbo.Student", "Num_of_group", c => c.String());
            AddColumn("dbo.Student", "Num_of_sub_faculty", c => c.Int(nullable: false));
            AddColumn("dbo.Group", "NumberOfSubFaculty", c => c.Int(nullable: false));
            AddColumn("dbo.Teacher", "Num_of_sub_faculty", c => c.Int(nullable: false));
            //AddColumn("dbo.Admin", "Email", c => c.String());
            DropForeignKey("dbo.TimeTables", "Teacher_UserId", "dbo.Teacher");
            DropForeignKey("dbo.TimeTables", "lesson_Id", "dbo.Disciplines");
            DropForeignKey("dbo.TimeTables", "Group_GroupId", "dbo.Group");
            //DropForeignKey("dbo.News", "Author_UserId", "dbo.User");
            DropForeignKey("dbo.Teacher", "SubFaculty_Id", "dbo.SubFaculties");
            DropForeignKey("dbo.Group", "SubFaculty_Id", "dbo.SubFaculties");
            DropForeignKey("dbo.SubFaculties", "Faculty_Id", "dbo.Faculties");
            DropIndex("dbo.TimeTables", new[] { "Teacher_UserId" });
            DropIndex("dbo.TimeTables", new[] { "lesson_Id" });
            DropIndex("dbo.TimeTables", new[] { "Group_GroupId" });
            //DropIndex("dbo.News", new[] { "Author_UserId" });
            DropIndex("dbo.Teacher", new[] { "SubFaculty_Id" });
            DropIndex("dbo.Group", new[] { "SubFaculty_Id" });
            DropIndex("dbo.SubFaculties", new[] { "Faculty_Id" });
            DropColumn("dbo.TimeTables", "Teacher_UserId");
            DropColumn("dbo.TimeTables", "lesson_Id");
            DropColumn("dbo.TimeTables", "Group_GroupId");
            //DropColumn("dbo.News", "Author_UserId");
            DropColumn("dbo.Group", "SubFaculty_Id");
            DropColumn("dbo.Teacher", "SubFaculty_Id");
            DropTable("dbo.Faculties");
            DropTable("dbo.SubFaculties");
        }
    }
}
