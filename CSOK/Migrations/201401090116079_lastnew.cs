namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lastnew : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TestItems", "Author_UserId", "dbo.User");
            DropIndex("dbo.TestItems", new[] { "Author_UserId" });
            AddColumn("dbo.AnswerCorrectAnswers", "AuthorID", c => c.Int(nullable: false));
            AddColumn("dbo.Questions", "AuthorID", c => c.Int(nullable: false));
            AddColumn("dbo.AnswerBlocks", "AuthorID", c => c.Int(nullable: false));
            AddColumn("dbo.Answers", "AuthorID", c => c.Int(nullable: false));
            AddColumn("dbo.TestItems", "AuthorID", c => c.Int(nullable: false));
            DropColumn("dbo.AnswerBlocks", "Order");
            DropColumn("dbo.TestItems", "Author_UserId");
           // DropTable("dbo.User");
        }
        
        public override void Down()
        {
            //CreateTable(
            //    "dbo.User",
            //    c => new
            //        {
            //            UserId = c.Int(nullable: false, identity: true),
            //            UserName = c.String(),
            //            Email = c.String(),
            //        })
            //    .PrimaryKey(t => t.UserId);
            
            AddColumn("dbo.TestItems", "Author_UserId", c => c.Int());
            AddColumn("dbo.AnswerBlocks", "Order", c => c.Int(nullable: false));
            DropColumn("dbo.TestItems", "AuthorID");
            DropColumn("dbo.Answers", "AuthorID");
            DropColumn("dbo.AnswerBlocks", "AuthorID");
            DropColumn("dbo.Questions", "AuthorID");
            DropColumn("dbo.AnswerCorrectAnswers", "AuthorID");
            CreateIndex("dbo.TestItems", "Author_UserId");
            AddForeignKey("dbo.TestItems", "Author_UserId", "dbo.User", "UserId");
        }
    }
}
