namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Studentnum_of_record_book : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Student", "Num_of_record_book", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Student", "Num_of_record_book", c => c.Int(nullable: false));
        }
    }
}
