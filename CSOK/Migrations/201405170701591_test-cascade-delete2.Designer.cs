// <auto-generated />
namespace CSOK.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.1-21010")]
    public sealed partial class testcascadedelete2 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(testcascadedelete2));
        
        string IMigrationMetadata.Id
        {
            get { return "201405170701591_test-cascade-delete2"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
