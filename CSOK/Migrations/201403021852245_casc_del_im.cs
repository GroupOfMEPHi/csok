namespace CSOK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class casc_del_im : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Images", "Question_ID", "dbo.Questions");
            DropIndex("dbo.Images", new[] { "Question_ID" });
            AlterColumn("dbo.Images", "Question_ID", c => c.Int(nullable: false));
            CreateIndex("dbo.Images", "Question_ID");
            AddForeignKey("dbo.Images", "Question_ID", "dbo.Questions", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Images", "Question_ID", "dbo.Questions");
            DropIndex("dbo.Images", new[] { "Question_ID" });
            AlterColumn("dbo.Images", "Question_ID", c => c.Int());
            CreateIndex("dbo.Images", "Question_ID");
            AddForeignKey("dbo.Images", "Question_ID", "dbo.Questions", "ID");
        }
    }
}
