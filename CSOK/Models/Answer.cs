﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CSOK.Models
{
    public class AnswerPossibleAnswer
    {
        public int ID { get; set; }
        //public virtual User Author { get; set; }
        //public int AuthorID { get; set; }
        //[Key]
        //[ForeignKey("PossibleAnswer")]
        //public int PossibleAnswerID { get; set; }
       // public int AuthorID { get; set; } 
       // public string Title { get; set; } // тема
        //public string Body { get; set; } // тело вопроса
        //public DateTime AnsernDataTime { get; set; }
        public Boolean IsRight { get; set; }
        //public virtual Question Question { get; set; }
        public virtual PossibleAnswer PossibleAnswer { get; set; }
        public virtual TestItemAnswer TestItemAnswer { get; set; }
        //public int TestID { get; set; } //id теста с ответами
        //public int Attempt { get; set; } //id номер попытки
        //public Answer()
        //{
        //    AnsernDataTime = System.DateTime.Now;
        //    IsRight = false;
        //}
    }
    public class AnswerCorrectAnswer {
        public int ID { get; set; }
        //public virtual User Author { get; set; }
        //public int AuthorID { get; set; }
        //public DateTime AnswernDataTime { get; set; }
        public string Body { get; set; }
        public virtual CorrectAnswer CorrectAnswer { get; set; }
        public virtual TestItemAnswer TestItemAnswer { get; set; }
        //public int TestID { get; set; } //id теста с ответами
        //public int Attempt { get; set; } //id номер попытки
    }
  
}


