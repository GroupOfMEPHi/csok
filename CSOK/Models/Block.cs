﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSOK.Models
{
    public class Block
    {
        public int ID { get; set; }
        public string Body { get; set; }
        public virtual List<AnswerBlock> AnswerBlocks { get; set; }
        public virtual Question Question { get; set; }
        public Block()
        {
            AnswerBlocks = new List<AnswerBlock>();
        }
    }
    public class Excess_Block
    {
        public int ID { get; set; }
        public string Body { get; set; }
        public bool IsRight { get; set; }
        public virtual List<AnswerExcessBlock> AnswerExcessBlocks { get; set; }
        public virtual Question Question { get; set; }
        public Excess_Block()
        {
            AnswerExcessBlocks = new List<AnswerExcessBlock>();
        }
    }

    public class AnswerBlock
    {
        public int ID { get; set; }
        //public virtual User Author { get; set; }
        //public int AuthorID { get; set; }
        [NotMapped]
        public int Order { get; set; }
        public string Body { get; set; }
        //public DateTime AnsernDataTime { get; set; }
        // public int AuthorID { get; set; } 
        public virtual Block Block { get; set; }
        public virtual TestItemAnswer TestItemAnswer { get; set; }
        //public int TestID { get; set; } //id теста с ответами
        //public int Attempt { get; set; } //id номер попытки
    }
    public class AnswerExcessBlock
    {
        public int ID { get; set; }
        //public int AuthorID { get; set; }
        [NotMapped]
        public int Order { get; set; }
        public string Body { get; set; }
        public bool IsRight { get; set; }
        //public DateTime AnsernDataTime { get; set; }
        // public int AuthorID { get; set; } 
        public virtual Excess_Block Excess_Block { get; set; }
        public virtual TestItemAnswer TestItemAnswer { get; set; }
        //public int TestID { get; set; } //id теста с ответами
        //public int Attempt { get; set; } //id номер попытки
    }
}