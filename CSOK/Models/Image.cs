﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CSOK.Models
{
    public class Image
    {
        [Key]
        public int ImageId { get; set; }
        public string ImageType { get; set; }
        public byte[] ImageBinary { get; set; }
        public int ImageLength { get; set; }
        public string ImageName { get; set; }
        public virtual Question Question { get; set; }

    }
}