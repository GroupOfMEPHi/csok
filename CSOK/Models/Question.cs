﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSOK.Models
{
    public class Question
    {
        public int ID { get; set; }
        //[Required(ErrorMessage = "Please enter a title")]
        public string Title { get; set; } // тема
        [Required(ErrorMessage = "Please enter a body")]
        public string Body { get; set; } // тело вопроса
        public virtual User Author { get; set; }
        //public int AuthorID { get; set; }
        public bool HasPossibleAnswer { get; set; } // тип ответа
        // public bool CheckBoxOrRadioAnswer { get; set; }//
        public bool HasExcessBlocks { get; set; }
        public bool CheckBoxAnswer { get; set; }//
        public bool RadioAnswer { get; set; }
        public bool MatrixAnswer { get; set; }//ответ внутрь полей матрицы
        [NotMapped]
        public string SelectedRadio { get; set; }// поле для radiobutton в базе не отображается
        public bool isSelected { get; set; }
        public string Tag { get; set; }
        public DateTime CreationDataTime { get; set; }
        public float difficulty { get; set; }
        public virtual CorrectAnswer CorrectAnswer { get; set; }
        //public virtual List<Answer> Answers { get; set; }
        public virtual List<PossibleAnswer> PossibleAnswers { get; set; }
        public virtual List<Image> Images { get; set; }
        public virtual List<Matrix> Matrices { get; set; }
        public virtual List<Block> Blocks { get; set; }
        public virtual List<Excess_Block> Excess_Blocks { get; set; }
        public virtual List<TestItem> TestItems { get; set; }
        public virtual List<TestItemAnswer> TestItemAnswers { get; set; }
        public virtual Theme Theme { get; set; }
        public Question()
        {
            Images = new List<Image>();
            PossibleAnswers = new List<PossibleAnswer>();
            //Answers = new List<Answer>();
            Blocks = new List<Block>();
            Excess_Blocks = new List<Excess_Block>();
            TestItems = new List<TestItem>();
            TestItemAnswers = new List<TestItemAnswer>();
        }
    }

    public class QuestionsIndex
    {
        public int ID { get; set; }
        [Display(Name = "Тема")]
        public string Title { get; set; }
        [Display(Name = "Вопрос")]
        public string Body { get; set; }
        [Display(Name = "Дата создания")]
        public DateTime CreationDataTime { get; set; }
        [Display(Name = "Кол-во ответов")]
        public int AnswCount { get; set; }
        [Display(Name = "Блоки")]
        public IEnumerable<string> BlockBody { get; set; }
        public IEnumerable<string> ExcessBlockBody { get; set; }
        [Display(Name = "Изображение")]
        public IEnumerable<int> ImageId { get; set; }
        [Display(Name = "Матрица")]
        public Matrix Matrix { get; set; }
        public List<PossibleAnswer> PossibleAnswers { get; set; }
        public bool CheckBoxAnswer { get; set; }//
        public bool RadioAnswer { get; set; }
        public CorrectAnswer CorrectAnswer { get; set; }
        public bool HasPossibleAnswer { get; set; }
        //public List<List<string>> Des_matrix { get; set; }

    }

    public class CorrectAnswer
    {
        [Key]
        [ForeignKey("Question")]
        public int QuestionID { get; set; }
       
        public string Body { get; set; }
        public virtual Question Question { get; set; }
        public virtual List<AnswerCorrectAnswer> AnswerCorrectAnswers { get; set; }
        public CorrectAnswer() {
            AnswerCorrectAnswers = new List<AnswerCorrectAnswer>();

        }

    }

    public class PossibleAnswer
    {
        public int ID { get; set; }
        public string Body { get; set; }
        public Boolean IsRight { get; set; }
        public virtual List<AnswerPossibleAnswer> AnswerPossibleAnswers { get; set; }
        public virtual Question Question { get; set; }
        public PossibleAnswer() {
            AnswerPossibleAnswers = new List<AnswerPossibleAnswer>();
        }
    }
}