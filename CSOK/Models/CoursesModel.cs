﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace CSOK.Models
{
    public enum CourseType 
    {
        [Description("Лекция")]
        Lection = 1, 
        [Description("Семинар")]
        Seminar = 2, 
        [Description("Лабораторный практикум")]
        Lab = 3
    }
    public class CoursesModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CourseType { get; set; }
        public string ExamType { get; set; }
        public string TeacherName { get; set; }
        
    }

    public class CourseDetails
    {
        public List<CourseSection> Sections { get; set; }
        public object TimeTable { get; set; } 
    }

    public class CourseSection
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<CourseTheme> Themes { get; set; } 
    }

    public class CourseTheme
    {
        public int Id { get; set; }
        public string Name { get; set; } 
    }
}