﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CSOK.Models
{
    /// <summary>
    /// Класс для одного сообщения в БД
    /// </summary>
    public class Message
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Subject { get; set; }
        public string Text { get; set; }
        public DateTime TimeSend { get; set; }
        public virtual List<UserMessage> UserMessages { get; set; } 
        public virtual List<MessageAttachment> MessageAttachments { get; set; }
        //public Message()
        //{
        //    UserMessages = new List<UserMessage>();
        //    MessageAttachments = new List<MessageAttachment>();
        //}
    }

    /// <summary>
    /// Класс для хранения в БД информации о связи пользователя с конкретным сообщением
    /// </summary>
    public class UserMessage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserMessageId { get; set; }
        public int UserId { get; set; }
        public bool AreSender { get; set; }
        [DefaultValue(false)]
        public bool AreDeleted { get; set; }
        public DateTime? DeletedTime { get; set; }
        [DefaultValue(false)]
        public bool AreRead { get; set; }
    }

    public class MessageAttachment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string AttachmentUrl { get; set; }
    }

    public class SendMailModel
    {
        public string To { get; set; }
        public string Subject { get; set; }
        public string Text { get; set; }
        public List<HttpPostedFileBase> AttachmentUrls { get; set; }

        public SendMailModel()
        {
            AttachmentUrls = new List<HttpPostedFileBase>();
        }
    }

    public class MessageSummuryModel
    {
        public List<string> To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public List<string> AttachmentUrls { get; set; }
        public bool CanDelete { get; set; }
        public DateTime SendTime { get; set; }
        public DateTime? DeleteTime { get; set; }
        public int Id { get; set; }
        public bool IsInbox { get; set; }
        public string Text { get; set; }
        public bool AreRead { get; set; }
    }
}