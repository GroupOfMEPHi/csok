﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSOK.Models
{
    public class CalendarEvent
    {
        public int id { get; set; }
        public string text { get; set; }
        public string type { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public virtual User Author { get; set; }
    }
}