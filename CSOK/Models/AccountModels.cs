﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace CSOK.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("MyDbContext")
        {
        }
        public DbSet<User> Users { get; set; }
    }
    [Table("User")]
    public class User
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        //public virtual Student Student { get; set; }
    }
    [Table("Student")]
    public class Student
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserId { get; set; }
        public string Num_of_record_book { get; set; }
        public string Name { get; set; }
        public DateTime YOB { get; set; }
		public virtual Group Group { get; set; }
    }

    [Table("Groups")]
    public class Group
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int AmountOfPeople { get; set; }
        public virtual SubFaculty SubFaculty { get; set; }
        public virtual List<Student> Students { get; set; }
    }

    [Table("Teacher")]
    public class Teacher
    {
        //[ForeignKey("User")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserId { get; set; }
        public string Name { get; set; }
        public DateTime YOB { get; set; }
        public virtual SubFaculty SubFaculty { get; set; }
        public string Rank { get; set; }
        public string Post { get; set; }
		public virtual List<Structure> Parts { get; set; }
        //public virtual User User { get; set; }
    }
    [Table("Admin")]
    public class Admin
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserID { get; set; }
        public string Name { get; set; }
    }


    [Table("Request")]
    public class Request
    {
        [Key]
        public int Num_of_request { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public DateTime YOB { get; set; }
        public int Num_of_sub_faculty { get; set; }
        public string Rank { get; set; }
        public string Post { get; set; }
        public string Num_of_record_book { get; set; }
        public string Num_of_group { get; set; }
    }
    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Текущий пароль")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение \"{0}\" должно содержать не менее {2} символов.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [Compare("NewPassword", ErrorMessage = "Новый пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить меня")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        public string UserName { get; set; }
        [Required]
        [RegularExpression(@"[А-Я][а-я]{2,}", ErrorMessage = "Вводите Фамилию")]
        [Display(Name = "Фамилия")]
        public string Name { get; set; }

        [Required]
        [RegularExpression(@"[А-Я][а-я]{2,}", ErrorMessage = "Вводите Имя")]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression(@"[А-Я][а-я]{2,}", ErrorMessage = "Вводите Отчество")]
        [Display(Name = "Отчество")]
        public string LastName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение \"{0}\" должно содержать не менее {2} символов.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [Compare("Password", ErrorMessage = "Пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Некорректный Email")]
        [Display(Name = "Электронная почта")]
        public string Email { get; set; }

        [Display(Name = "Номер зачётки")]
        [RegularExpression(@"\w{6,}", ErrorMessage = "Неверно введён номер зачётки")]
        public string Num_of_record_book { get; set; }

        [Display(Name = "Год поступления")]
        [RegularExpression(@"\d{4}", ErrorMessage = "Неверно введён год поступления")]
        public string Year { get; set; }

        [Required]
        [RegularExpression(@"\d{2,}", ErrorMessage = "Неверно введён номер выпускающей кафедры")]
        [Display(Name = "Номер кафедры")]
        public string Num_of_sub_faculty { get; set; }

        [RegularExpression(@"[КАИРУТФБ]\d-\d{2,}", ErrorMessage = "Неверно введён номер группы : К7-223")]
        [Display(Name = "Номер группы")]
        public string Num_of_group { get; set; }

        [Required(ErrorMessage = "Если не отображается автоматичский календарь, введите значения в виде: dd'/'MM'/'yyyy")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Дата рождения")]
        public DateTime YOB { get; set; }

        [Display(Name = "Должность")]
        public string Post { get; set; }

        [Display(Name = "Звание")]
        public string Rank { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
