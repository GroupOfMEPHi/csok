﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CSOK.Models
{
    public class Discipline
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<Structure> Parts { get; set; }
        public virtual List<TestItem> Tests { get; set; }
    }

    public class Structure
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual Discipline Discipline { get; set; }
        public virtual Teacher Teacher { get; set; }
        public virtual List<Theme> Themes { get; set; }
    }

    public class Theme
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Literature { get; set; }
        public virtual Structure Structure { get; set; }
        public virtual List<Question> Questions { get; set; }
    }

    public class Faculty
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string ShortName { get; set; }
        public string Name { get; set; }
        public virtual List<SubFaculty> SubFaculties { get; set; }
    }

    public class SubFaculty
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual Faculty Faculty { get; set; }
        public virtual List<Teacher> Teachers { get; set; }
        public virtual List<Group> Groups { get; set; }
    }

    public class GroupDisciplineTestDropdown
    {
        public int Group { get; set; }
        public int Discipline { get; set; }
        public int Test { get; set; }

        public SelectList AvailableGroups { get; set; }
        public SelectList AvailableDisciplines { get; set; }
        public SelectList AvailableTests { get; set; }

        public GroupDisciplineTestDropdown()
        {
            AvailableGroups = new SelectList(Enumerable.Empty<Discipline>(), "GroupId", "GroupName");
            AvailableDisciplines = new SelectList(Enumerable.Empty<Discipline>(), "Id", "Name");
            AvailableTests = new SelectList(Enumerable.Empty<TestItem>(), "ID", "Subject");
        }
    }
}