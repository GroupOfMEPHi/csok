﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSOK.Models
{
    public class Matrix
    {
        [Key]
        public int Matrix_Id { get; set; }
        [Required(ErrorMessage = "Please enter number of rows"), Range(1, 5, ErrorMessage = "The number of rows must be >0 and <6")]
        public int nRows { get; set; }
        [Required(ErrorMessage = "Please enter number of columns"), Range(1, 5, ErrorMessage = "The number of columns must be >0 and <6")]
        public int nCols { get; set; }
        //public byte[] binary { get; set; }
        public byte[] matr_bin { get; set; }
        public byte[] correct_matr_bin { get; set; }
        [NotMapped]
        public List<List<string>> deserialized_matrix { get; set; }
        //public virtual List<List<int>> elements { get; set; }
        public virtual Question Question { get; set; }
        public virtual List<AnswerMatrix> AnswerMatrices { get; set; }
        public Matrix()
        {
            AnswerMatrices = new List<AnswerMatrix>();
        }
    }
    public class AnswerMatrix
    {
        public int ID { get; set; }
        //public int AuthorID { get; set; }
        public int nRows { get; set; }
        public int nCols { get; set; }
        public byte[] matr_bin { get; set; }
        //public DateTime AnsernDataTime { get; set; }
        public virtual TestItemAnswer TestItemAnswer { get; set; }
        //public int TestID { get; set; } //id теста с ответами
        //public int Attempt { get; set; } //id номер попытки
        public virtual Matrix Matrix { get; set; }
    }
}