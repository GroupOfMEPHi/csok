﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSOK.Models
{
    [Table("News")]
    public class News
    {
        [Key]
        public int Num_of_news { get; set; }
        public DateTime DateTimeOfCreation { get; set; }
        public virtual User Author { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}