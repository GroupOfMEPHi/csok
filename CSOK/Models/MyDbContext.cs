﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace CSOK.Models
{
    public class MyDbContext : DbContext
    {
        public DbSet<Question> Questions { get; set; }
        public DbSet<TestItem> Testitems { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Discipline> Disciplines { get; set; }
        public DbSet<Structure> Structures { get; set; }
        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<SubFaculty> SubFaculties { get; set; }
        public DbSet<Theme> Themes { get; set; }
        public DbSet<Request> Request { get; set; }
        public DbSet<Admin> Admin { get; set; }
        public DbSet<TimeTable> TimeTable { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<CalendarEvent> CalendarEvents { get; set; }
       // public DbSet<User> Users { get; set; }
       // public DbSet<StudentProfile> StudentProfiles { get; set; } //Таблица с информацией о студентах
       // public DbSet<TeacherProfile> TeacherProfiles { get; set; } //Таблица с информацией о преподавателях
        public DbSet<PossibleAnswer> PossibleAnswers { get; set; }
        public DbSet<CorrectAnswer> CorrectAnswer { get; set; }
       // public DbSet<webpages_Roles> webpages_Roles { get; set; } //Таблица ролей
       // public DbSet<Course> Courses { get; set; } //Таблица учебных дисциплин
        public DbSet<AnswerPossibleAnswer> AnswerPossibleAnswers { get; set; }
       // public DbSet<AnswerRating> AnswerRatings { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Block> Blocks { get; set; }
        public DbSet<Excess_Block> Excess_Blocks { get; set; }
        public DbSet<Matrix> Matrices { get; set; }
        public DbSet<AnswerCorrectAnswer> AnswerCorrectAnswer { get; set; }
        public DbSet<TestItemAnswer> TestItemAnswers { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<UserMessage> UserMessages { get; set; }
        public DbSet<MessageAttachment> MessageAttachments { get; set; }
		public DbSet<TestResults> TestResults { get; set; }
		public DbSet<RushSatistics> RushSatistics { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Estimation> Estimations { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer(new MyDbContextInitializer());

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<PossibleAnswer>()
            .HasRequired(e => e.Question)
            .WithMany(t => t.PossibleAnswers)
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<Block>()
            .HasRequired(e => e.Question)
            .WithMany(t => t.Blocks)
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<Excess_Block>()
            .HasRequired(e => e.Question)
            .WithMany(t => t.Excess_Blocks)
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<Question>()
            .HasRequired(e => e.CorrectAnswer)
            .WithRequiredPrincipal(t=>t.Question)
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<AnswerPossibleAnswer>()
            .HasRequired(e => e.PossibleAnswer)
            .WithMany(t => t.AnswerPossibleAnswers)
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<Matrix>()
            .HasRequired(e => e.Question)
            .WithMany(t => t.Matrices)
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<Image>()
            .HasRequired(e => e.Question)
            .WithMany(t => t.Images)
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<AnswerBlock>()
            .HasRequired(e => e.Block)
            .WithMany(t => t.AnswerBlocks)
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<AnswerExcessBlock>()
            .HasRequired(e => e.Excess_Block)
            .WithMany(t => t.AnswerExcessBlocks)
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<AnswerCorrectAnswer>()
            .HasRequired(e => e.CorrectAnswer)
            .WithMany(t => t.AnswerCorrectAnswers)
            .WillCascadeOnDelete(true);

          
            modelBuilder.Entity<AnswerMatrix>()
            .HasRequired(e => e.Matrix)
            .WithMany(t => t.AnswerMatrices)
            .WillCascadeOnDelete(true);
          
            modelBuilder.Entity<Question>()
            .HasRequired(e => e.Theme)
            .WithMany(t => t.Questions)
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<Theme>()
            .HasRequired(e => e.Structure)
            .WithMany(t => t.Themes)
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<Structure>()
            .HasRequired(e => e.Discipline)
            .WithMany(t => t.Parts)
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<TestItemAnswer>()
            .HasRequired(e => e.TestItem)
            .WithMany(t => t.TestItemAnswers)
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<AnswerMatrix>()
            .HasRequired(e => e.TestItemAnswer)
            .WithMany()
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<AnswerBlock>()
            .HasRequired(e => e.TestItemAnswer)
            .WithMany()
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<AnswerCorrectAnswer>()
            .HasRequired(e => e.TestItemAnswer)
            .WithMany()
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<AnswerExcessBlock>()
            .HasRequired(e => e.TestItemAnswer)
            .WithMany()
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<AnswerPossibleAnswer>()
            .HasRequired(e => e.TestItemAnswer)
            .WithMany()
            .WillCascadeOnDelete(true);
        }
    }
}