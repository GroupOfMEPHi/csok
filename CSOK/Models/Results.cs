﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CSOK.Models
{
    public class Results
    {
        public string TestSubject { get; set; } //тема теста
        public string TestName { get; set; } //Название
        public string StudentName { get; set; } //id студента
        public long DatePass { get; set; } //Дата прохождения
        public string TimePass { get; set; } //Время прохождения
        public int NumberTry { get; set; } //№ попытки
        public int MaxNumberTry { get; set; } //Допустимое кол-во попыток
        public int TrainingNumberTry { get; set; } //Кол-во попыток в режиме обучения
        public int AverageNumberTry { get; set; } //Среднее кол-во попыток
        public int QuestCount { get; set; } //Всего вопросов
        public int RightCount { get; set; } //Кол-во правильных ответов
        public int WrongCount { get; set; } //Кол-во неправильных ответов
        public float Result { get; set; } //Результат, % усвоения
        public string RecommendedLit { get; set; } //Рекомендуемая лит-ра
        public string RecommendedTest { get; set; } //Рекоменд. тест
        public int Bonus { get; set; } //Бонус
        public float AveragePoint { get; set; } //Ср. балл, оценка
        public string ComplexSubject { get; set; } //Сложная тема
        public int QuestionId { get; set; } //№ попытки
        public string QuestionSubject { get; set; } //тема вопроса
        public string Estimation { get; set; }//метод оценивания 1-Лучшая попытка, 2-Средняя оценка, 3-Первая попытка, 4-Последняя попытка
        public string QuestionBody { get; set; }//тело вопроса
        public long DateOpen { get; set; } //Дата открытия теста
        public long DateClose { get; set; } //Дата закрытия теста
        public virtual TestItem TestItem { get; set; }
    }

    public class TestResults
    {
        [Key]
        public int ID { get; set; }
        //public int Question_ID { get; set; }
        public virtual Question Question { get; set; }
        //public int Student_ID { get; set; }
        public Boolean IsRight { get; set; }
        public virtual TestItemAnswer TestItemAnswer { get; set; }
        //public int TestItem_ID { get; set; }
        //public DateTime DatePass { get; set; }
    }

    public class TestProgress
    {
        public string Name { get; set; } //Название теста/имя студента
        public float Progress { get; set; } //Усвоение
        public string Group { get; set; } //группа
        public Discipline Discipline { get; set; } //курс
    }

    public class RushSatistics
    {
        public int Id { get; set; } //Id
        public int FuntionType { get; set; } //Тип функции: 1 - логистическая кривая задания, 2 - информационная функция
        public virtual Question Question { get; set; } //вопрос
        public virtual TestItem TestItem { get; set; } //Тест
        public double LvlTraining { get; set; } //Уровень подготовки
        public float Value { get; set; }
        public DateTime CalcDate { get; set; }//дата расчета
        public double DifficultQuestLvl { get; set; } //сложность задания
    }

    public class Fi
    {
        public int StidentId { get; set; } //Id студента
        public double Value { get; set; } //начальное значение уровня подготовки
    }

    public class Beta
    {
        public int QuestionId { get; set; } //Id вопроса
        public double Value { get; set; } //начальное значение уровня сложности задания
        public double DifficultQuestLvl { get; set; } //сложность задания
    }

    public class QuestionRush
    {
        public int QuestionId { get; set; } //Id вопроса
        public int TestId { get; set; } //Id вопроса
        public string Theme { get; set; } //Тема
        public string Body { get; set; } //Тело вопроса
    }

    public class ICCRush //модель для вывода ICC на экран
    {
        public double nFi { get; set; } //уровень подготовки
        public double nPj { get; set; } //Вер-ть правильного ответа
    }

    public class ExpPoint
    {
        public List<double> nFi { get; set; } //уровень подготовки
        public List<double> nPj { get; set; } //доля верных ответов
        public double ChiSq { get; set; } //уровень значимости хи-квадрат
    }
}