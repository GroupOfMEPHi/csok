﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CSOK.Models
{
    public class TestItemAnswer
    {
        public int ID { get; set; }
       
        public virtual User Author { get; set; }
       
     
   
        public string Subject { get; set; }
   
        public virtual List<Question> Questions { get; set; }
        public virtual TestItem TestItem { get; set; }
        //удалить
        public int Time { get; set; }
        //удалить
        public int Paging { get; set; } //0- все вопросы на одной странце/1- возможен переход назад /2- возможен переход только вперёд
        //удалить
        public virtual Discipline Discipline { get; set; }

        public int CurrentAttempt { get; set; }
        public int TestScore { get; set; }
        public bool CurrentPasses { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDateTime { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EndtDateTime { get; set; }
        //public string SpentTime { get; set; } // время затраченное на тест
    }
}