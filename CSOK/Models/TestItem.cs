﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CSOK.Models
{
    public class TestItem
    {
        public int ID { get; set; }
        public virtual User Author { get; set; }
        [Required(ErrorMessage = "Укажите тему теста")]
        public string Subject { get; set; }
        public string Description { get; set; } //описани теста
        public DateTime CreationDateTime { get; set; }
        [Required(ErrorMessage = "Если не отображается автоматичский календарь, введите значения в виде: dd'/'MM'/'yyyy")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Дата закрытия теста")]
        public DateTime ClosingDateTime { get; set; }
        [Required(ErrorMessage = "Если не отображается автоматичский календарь, введите значения в виде: dd'/'MM'/'yyyy")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Дата открытия теста")]
        public DateTime OpeningDateTime { get; set; }
        public virtual List<Question> Questions { get; set; }
        public int Time { get; set; }
        public int Paging { get; set; } //0- все вопросы на одной странце/1- возможен переход назад /2- возможен переход только вперёд
		public virtual Discipline Discipline { get; set; }
        public int Attempts { get; set; }
        //public bool TestMode { get; set; } //false-тестирование, true- обучение
        public int TestMode { get; set; } //0-тестирование// 1- результаты теста после прохождения теста// 2-переход к следующему вопросу возможен приправильном ответе на текущий
        public bool OrderOfQuestion { get; set; } //tue- перемешивать вопросы в тесте
        public virtual Estimation Estimation { get; set; } // метод оценивания 1-Лучшая попытка, 2-Средняя оценка, 3-Первая попытка, 4-Последняя попытка
        public virtual List<TestItemAnswer> TestItemAnswers { get; set; }
    }

    public class Estimation
    {
        [Key]
        public int EstID { get; set; }
        public string Name { get; set; }
    }
}