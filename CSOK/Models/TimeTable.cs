﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSOK.Models
{
    public class TimeTable
    {
        public int ID { get; set; }
        public virtual Teacher Teacher { get; set; }
        public virtual Group Group { get; set; }
        public virtual Discipline lesson { get; set; }
        public string day { get; set; }
        public string time { get; set; }
        public string week { get; set; }
        public string typeless { get; set; }
        public string aud { get; set; }
    }
}