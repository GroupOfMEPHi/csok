﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.ClientServices.Providers;
using CSOK.Filters;
using CSOK.Models;
using WebGrease.Css.Extensions;

namespace CSOK.Core
{
    /// <summary>
    /// Содержит основной функционал, отвечающий за сообщения
    /// </summary>
    public class MessageCore
    {
        #region private
        private int GetUserId(string userName)
        {
            try
            {
                return _userFull.FirstOrDefault(
                    x => x.Key.UserName.Equals(userName, StringComparison.InvariantCultureIgnoreCase)).Key.UserId;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        private string GetUserName(int userId)
        {
            try
            {
                return _userFull.FirstOrDefault(x => x.Key.UserId == userId).Key.UserName;
            }
            catch (Exception)
            {
                return "Удаленный пользователь";
            }
        }

        private string GetUserNameFull(int userId)
        {
            var student = _dbContext.Students.FirstOrDefault(x => x.UserId == userId);
            if (student != null) 
                return (student.Name ?? "Нет имени") + " - " + (student.Group!=null ? student.Group.GroupName : "Нет группы");
            var prepod = _dbContext.Teachers.FirstOrDefault(x => x.UserId == userId);
            return prepod == null ? "Удаленный пользователь" : prepod.Name + " - " + prepod.Rank;
        }
        private MessageSummuryModel GetMessageSummuryModel(Message x)
        {
            return new MessageSummuryModel
            {
                Id = x.Id,
                From = GetUserNameFull(x.UserMessages.FirstOrDefault(y => y.AreSender).UserId),
                To = x.UserMessages.Where(y => !y.AreSender).Select(y => GetUserNameFull(y.UserId)).ToList(),
                AttachmentUrls = x.MessageAttachments.Select(y => y.AttachmentUrl).ToList(),
                CanDelete = !x.UserMessages.Where(y=>y.UserId==_userId).All(y=>y.AreDeleted),
                SendTime = x.TimeSend,
                Subject = x.Subject,
                IsInbox = !x.UserMessages.FirstOrDefault(y => y.UserId == _userId).AreSender,
                AreRead = x.UserMessages.FirstOrDefault(y => y.UserId == _userId).AreRead,
                Text = x.Text
            };
        }
        #endregion
        private readonly MyDbContext _dbContext;
        private readonly Dictionary<User,string> _userFull = new Dictionary<User, string>();
        private readonly string _userName;
        private readonly int _userId;
        /// <summary>
        /// Основной конструктор
        /// </summary>
        /// <param name="pUserName">Имя пользователя, для которого создан экземпляр</param>
        /// <param name="users">Список всех пользователей системы. Необязательный параметр</param>
        public MessageCore(string pUserName, IEnumerable<User> users = null)
        {
            _dbContext = new MyDbContext();
            if (users != null) _userFull = users.AsParallel().ToDictionary(x => x, x => GetUserNameFull(x.UserId));
            _userName = pUserName;
            _userId = GetUserId(pUserName);
        }
        /// <summary>
        /// Отправляет сообщение
        /// </summary>
        /// <param name="model">Модель, содержащая полную информацию об отправляемом сообщении</param>
        /// <param name="serverPath">Путь на сервере к директории, куда складываются вложения</param>
        /// <returns>Ид сообщения в базе</returns>
        public int SendMessage(SendMailModel model, string serverPath)
        {
            var message = new Message
            {
                Subject = model.Subject ?? "No subject",
                Text = model.Text.Replace("\r\n", "<br>") ?? "No text",
                TimeSend = DateTime.Now,
                UserMessages = new List<UserMessage>(),
                MessageAttachments = new List<MessageAttachment>()
            };
            model.AttachmentUrls.Where(x=>x!=null).AsParallel().ForEach(x =>
            {
                //var newFileName = Server.MapPath("~/Upload/" + x.FileName);
                var fileNameShort = x.FileName;
                string fileNameEnd = "";
                if (fileNameShort.IndexOf(".") != -1)
                {
                    fileNameEnd = fileNameShort.Substring(fileNameShort.LastIndexOf("."));
                    fileNameShort = fileNameShort.Substring(0, fileNameShort.LastIndexOf("."));
                }
                var newFileName = serverPath+fileNameShort;
                int k = 0;
                while (System.IO.File.Exists(newFileName + "_" + k + fileNameEnd))
                    k++;
                message.MessageAttachments.Add(new MessageAttachment { AttachmentUrl = fileNameShort + "_" + k + fileNameEnd });
                x.SaveAs(newFileName + "_" + k + fileNameEnd);
            });
            _dbContext.Messages.Add(message);
            message.UserMessages.Add(new UserMessage
            {
                UserId = _userId,
                AreSender = true,
                AreRead = true
            });

            var regexp = new Regex(@"\([a-zA-Zа-яА-Я\-0-9\s]*\)");
            regexp
                .Replace(model.To,"")
                .Split(';')
                .Select(x=>x.Replace(" ",String.Empty))
                .Where(x=>!x.Equals(String.Empty))
                .ToList()
                .ForEach(x => message.UserMessages.Add(new UserMessage
                {
                    UserId = GetUserId(x),
                    AreSender = false
                }));
            _dbContext.SaveChanges();
            return 0;
        }
        /// <summary>
        /// Возвращает список сообщений, отвечающий фильтру
        /// </summary>
        /// <param name="filter">Фильтр</param>
        /// <returns>Перечисляемая последовательность сообщений</returns>
        public IEnumerable<MessageSummuryModel> GetFilteredMessageList(MessageFilter filter)
        {
            IEnumerable<Message> query = _dbContext.Messages
                .Where(x => x.UserMessages.Any(y => y.UserId == _userId
                                                    &&
                                                    (y.AreSender == !filter.WithInbox ||
                                                     y.AreSender == filter.WithOutbox)
                                                    && y.AreDeleted == filter.AreDeleted));
            switch (filter.SortOrder)
            {
                case MessageSortOrder.Date:
                    query = query.OrderBy(x => x.TimeSend);
                    break;
                case MessageSortOrder.DateDesc:
                    query = query.OrderByDescending(x => x.TimeSend);
                    break;
            }
            if (filter.StartIndex != null)
                query = query.Skip(filter.StartIndex.Value);
            if (filter.ItemCount != null)
                query = query.Take(filter.ItemCount.Value);
            if (!string.IsNullOrEmpty(filter.SearchString))
            {
                var queryText = 
                    String.Format(@"select distinct  Message_Id from UserMessages 
	                        where UserId In
	                        (
		                        select UserId from Teacher 
			                        where PATINDEX('%{0}%',Teacher.Name) > 0
		                        union
		                        select UserId from Student 
			                        where PATINDEX('%{0}%',Student.Name) > 0 
				                        or PATINDEX('%{0}%',Student.Num_of_group) > 0
	                        )", filter.SearchString);
                var resultIds = _dbContext.Database.SqlQuery<int>(queryText).ToList();
                query =
                    query.ToList().Where(
                        x => x.Subject.IndexOf(filter.SearchString, StringComparison.InvariantCultureIgnoreCase) != -1
                          || resultIds.Any(z=>z==x.Id)   
                        );
            }
            return query.ToList()
                    .Select(GetMessageSummuryModel);
        }

        /// <summary>
        /// Возвращает полную информацию о моделе
        /// </summary>
        /// <param name="messageId">Ид этой модели</param>
        /// <returns>Заполненная модель сообщения</returns>
        public MessageSummuryModel GetMessageById(int messageId)
        {
            var message = _dbContext.Messages.FirstOrDefault(x => x.Id == messageId);
            message.UserMessages.FirstOrDefault(x => x.UserId == _userId).AreRead = true;
            _dbContext.SaveChanges();
            return GetMessageSummuryModel(message);
        }

        public List<string> SearchUser(string pQuery)
        {
            var userIds = _userFull
                .Where
                (
                    x => x.Key.UserName.IndexOf(pQuery, StringComparison.InvariantCultureIgnoreCase) != -1
                         || x.Value.IndexOf(pQuery, StringComparison.InvariantCultureIgnoreCase) != -1
                );
            return userIds.Select(x=>String.Format("{0}({1})",x.Key.UserName,x.Value)).ToList();
        }


        /// <summary>
        /// Возвращает список имен пользователей из поданых на вход, которых нет в системе
        /// </summary>
        /// <param name="names">Список имен пользователей через ;</param>
        /// <returns>Список пользователей, которых нет</returns>
        public List<string> GetBadNames(string names)
        {
            var regexp = new Regex(@"\([a-zA-Zа-яА-Я\-0-9\s]*\)");
            var nameList = regexp.Replace(names, "")
                .Split(';')
                .Select(x => x.Replace(" ", ""))
                .Where(x => !x.Equals("")).ToList();
            if (nameList.Count==0)
                return new List<string>{"nothing"};
            return nameList.Select(x => x.Trim(' ')).Where(x => !_userFull
                .Any(y => y.Key.UserName.Equals(x, StringComparison.InvariantCultureIgnoreCase))).ToList();
        }
        /// <summary>
        /// Удаляет сообщение для пользователя
        /// </summary>
        /// <param name="messageId">Ид сообщения</param>
        public void RemoveMessage(int messageId)
        {
            _dbContext.Messages.FirstOrDefault(x => x.Id == messageId)
                .UserMessages.Where(x => x.UserId == _userId)
                .ForEach(x=>x.AreDeleted = true);
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Восстанавливает сообщение для пользователя
        /// </summary>
        /// <param name="messageId">Ид сообщения</param>
        public void RestoreMessage(int messageId)
        {
            _dbContext.Messages.FirstOrDefault(x => x.Id == messageId)
                .UserMessages.Where(x => x.UserId == _userId)
                .ForEach(x => x.AreDeleted = false);
            _dbContext.SaveChanges();
        }
        /// <summary>
        /// Создает модель сообщения для ответа
        /// </summary>
        /// <param name="id">Ид сообщения</param>
        /// <returns>Модель</returns>
        public SendMailModel GetRe(int id)
        {
            var message = _dbContext.Messages.FirstOrDefault(x => x.Id == id);
            var toId = message.UserMessages.FirstOrDefault(x => x.AreSender).UserId;
            return new SendMailModel
            {
                AttachmentUrls = new List<HttpPostedFileBase>(),
                Subject = "Re: "+message.Subject,
                Text = "\n-------------------------------------\n>"+message.Text.Replace("<br>", "\n>"),
                To = String.Format("{0}({1})", GetUserName(toId), GetUserNameFull(toId))
            };
            //return userIds.Select(x => String.Format("{0}({1})", x.Value, GetUserNameFull(x.Key))).ToList();
        }
    }
}