﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CSOK.Models;

namespace CSOK.Core
{
    public class CoursesCore
    {
        MyDbContext _context = new MyDbContext();
        UsersContext _usersContext = new UsersContext();
        private int GetUserId(string userName)
        {
            return
                _usersContext.Users.FirstOrDefault(
                    x => x.UserName.Equals(userName, StringComparison.InvariantCultureIgnoreCase)).UserId;
        }

        ~CoursesCore()
        {
            _context.Dispose();
            _usersContext.Dispose();
        }
        public Dictionary<int, string> GetTeacherDisciplines(string teacherName)
        {
            var id = GetUserId(teacherName);
            var disc = _context.TimeTable
                .Where(x => x.Teacher.UserId == id)
                .ToDictionary(x => x.lesson.Id, x => x.lesson.Name);
            return disc;
        }

        public List<CourseSection> GetCurrentDiscipline(int disciplineId, string userName)
        {
            var userId = GetUserId(userName);
            return _context.Disciplines.Find(disciplineId).Parts.Where(x=>x.Teacher.UserId==userId).Select(x => new CourseSection
            {
                Id = x.Id,
                Name = x.Name,
                Themes = x.Themes.Select(y => new CourseTheme
                {
                    Id = y.Id,
                    Name = y.Name
                }).ToList()
            }).ToList();
        }

        public int AddSection(string userName, int disciplineId, string sectionName)
        {
            var id = GetUserId(userName);
            var newStruct = new Structure
            {
                Discipline = _context.Disciplines.Find(disciplineId),
                Name = sectionName,
                Teacher = _context.Teachers.Find(id)
            };
            _context.Structures.Add(newStruct);
            _context.SaveChanges();
            return newStruct.Id;
        }

        public void RemoveSection(int id)
        {
            _context.Structures.Remove(_context.Structures.Find(id));
            _context.SaveChanges();
        }

        public int AddTheme(int sectionId, string themeName)
        {
            var newStruct = new Theme
            {
                Structure = _context.Structures.Find(sectionId),
                Name = themeName
            };
            _context.Themes.Add(newStruct);
            _context.SaveChanges();
            return newStruct.Id; 
        }

        public void RemoveTheme(int id)
        {
            _context.Themes.Remove(_context.Themes.Find(id));
            _context.SaveChanges();
        }

        public List<CoursesModel> GetStudentCoursesList(string userName)
        {
            return GetStudentCoursesList(GetUserId(userName));
        }
        public List<CoursesModel> GetStudentCoursesList(int teacherId)
        {
            var group =
                _context.Students.First(x => x.UserId == teacherId).Group;
            return _context.TimeTable.Where(x => x.Group.GroupId == group.GroupId).Select(x => new CoursesModel
            {
                Id = x.lesson.Id,
                Name = x.lesson.Name,
                CourseType = x.typeless,
                TeacherName = x.Teacher.Name
            }).ToList();
        }

        public List<CourseSection> GetCurrentDisciplineStudent(int id, string name)
        {
            var userId = GetUserId(name);
            var groupId = _context.Students.FirstOrDefault(x=>x.UserId==userId).Group.GroupId;
            var teacherId =
                _context.TimeTable.FirstOrDefault(x => x.lesson.Id == id && x.Group.GroupId == groupId).Teacher.UserId;
            var tName = _usersContext.Users.FirstOrDefault(x => x.UserId == teacherId);
            var tmp = GetCurrentDiscipline(id,tName.UserName);
            return tmp;
        }

        public string GetDiscName(int id)
        {
            return _context.Disciplines.FirstOrDefault(x => x.Id == id).Name;
        }
    }
}