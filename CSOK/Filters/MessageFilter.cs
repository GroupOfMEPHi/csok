﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace CSOK.Filters
{
    public class MessageFilter
    {
        public bool WithInbox { get; set; }
        public bool WithOutbox { get; set; }
        public bool AreDeleted { get; set; }
        public string SearchString { get; set; }
        public int? StartIndex { get; set; }
        public int? ItemCount { get; set; }
        public MessageSortOrder SortOrder { get; set; }
    }
    public enum MessageSortOrder {Nil = 0,Date = 1, DateDesc = 2}
}