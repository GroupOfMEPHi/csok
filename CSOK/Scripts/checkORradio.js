﻿function add_input() {
    $('#for_answ').fadeOut(400);
    $('#addinput, #delinput').removeClass('button_error');
    if ($("#qtype5").is(':checked')) {
        var nam = [document.getElementById('PossibleAnswers').getElementsByClassName('possible').length];
        var check = '<input type="checkbox" name="checkedValues" class="cB" value="' + nam + '">';
    }

    if ($("#qtype6").is(':checked')) {
        var nam = [document.getElementById('PossibleAnswers').getElementsByClassName('possible').length];
        var check = '<input type="radio" name="answrad" class="rB" value="' + nam + '">';
    }


    // Создаем новый div-элемент
    var new_input = document.createElement('div');
    new_input.className = "possible"

    // Тут мы уже можем воспользоваться innerHTML потому, что изменения не коснуться всего остального докуметна
    new_input.innerHTML = '<br>Вариант ответа №' + [document.getElementById('PossibleAnswers').getElementsByClassName('possible').length + 1] + '   ' + '<input type="text" name="answ" class="answ">' + check + " Правильный ответ";

    // Добавляем только что созданный div на страницу
    document.getElementById('PossibleAnswers').appendChild(new_input);
    $('.cB, .rB').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });

}

function del_input() {
    //  удаляем последний div в нутри id=inputi
    $("#PossibleAnswers .possible").last().remove();
    //document.getElementById('PossibleAnswers').removeChild(document.getElementById('PossibleAnswers').getElementsByTagName('div')[document.getElementById('PossibleAnswers').getElementsByTagName('div').length - 1]);
}
