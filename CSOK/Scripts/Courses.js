﻿function openCourse(id)
{
    var data = {};
    data["id"] = id;
    doAjaxHtml("Courses", "TeacherDiscipline", data, null, null, "#partialContent");
}
function noPageUp(e)
{
    var key = e.which;
    if ((key == "34") || (key == "33"))
        e.preventDefault();
}
function addSectionInit()
{
    showModal(".addSection", function()
    {
        $(".addSection input").val("");
    },
    function()
    {
        var data = {};
        data["name"] = $(".addSection input").val();
        data["disciplineId"] = disciplineId;
        doAjax("courses", "addSection", data, function(dataServ)
        {
            var addedName = dataServ["name"];
            var addedId = dataServ["id"];
            $("#teacherSectTable").append(
                '<div data-type="section" data-section-id="'+addedId+'" data-section-name="'+addedName+'">'+
                '<div class="sectionBlock">'+addedName+' <a onclick="deleteSection('+addedId+')">Удалить</a></div>'+
                '<ul data-type="themes"></ul></div>'
                );
        });
    }
    );
}
function deleteSection(id)
{
    var data = {};
    data["id"] = id;
    doAjax("courses", "RemoveSection", data, null);
    $('div[data-section-id="' + id + '"]').remove();
}
function addThemeInit()
{
    showModal(".addTheme", function()
        {
            $("#sectionsList").html("");
            $('div[data-type="section"]').each(function (elem, value)
            {
                $("#sectionsList").append('<option value="' + $(value).attr('data-section-id')
                    + '">' + $(value).attr('data-section-name') + '</option>');
            });
            $("#addedThemeName").val("");
        },
        function()
        {
            var data = {};
            data["themeName"] = $("#addedThemeName").val();
            data["sectionId"] = $("#sectionsList").val();
            doAjax("courses", "addTheme", data, function(dataServ)
            {
                var sectId = $("#sectionsList").val();
                var themeId = dataServ["id"];
                var themeName = dataServ["name"];
                $('div[data-section-id="' + sectId + '"] ul').append(
                    '<li data-type="theme" data-theme-id="' + themeId + '">' + themeName
                    + ' <a onclick="deleteTheme('+themeId+')">Удалить</a></li>'
                    );
            });
            console.log(data);
        });
}
function deleteTheme(id)
{
    var data = {};
    data["id"] = id;
    doAjax("courses", "RemoveTheme", data, null);
    $('li[data-theme-id="'+id+'"]').remove();
}
function showModal(elemId, initFunc, succsessFunc)
{
    $("html").css("overflow", "hidden");
    $("body").css("overflow", "hidden");
    var maskHeight = $(document).height();
    var maskWidth = $(window).width();
    var modal = $(elemId);
    var getCss = function (css) { return parseInt(modal.css(css)); }
    scrTop = $("body").scrollTop();
    if (scrTop == 0)
        scrTop = $("html").scrollTop();
    $('.mask').css("top", scrTop + "px");
    modal.css("top", (parseInt($(window).height() / 2) + parseInt(scrTop)) + "px");
    console.log(getCss('padding-left'));
    var l = (getCss('padding-left') + getCss('padding-right') + getCss('height'))/2;
    modal.css('margin-top', '-' + l + 'px');
    var r = (getCss('padding-top') + getCss('padding-bottom') + getCss('width'))/2;
    modal.css('margin-left', '-' + r+'px');
    $('.mask').fadeTo(200, 0.8);
    $('.mask').hide().show(); //Дикий, ужасный костыль. Подпирает хром 33 версии.
    var winH = $(window).height();
    var winW = $(window).width();
    $('body').keydown(noPageUp);
    modal.show("normal");
    initFunc();
    $(elemId + " .saveMe").unbind("click");
    $(elemId+" .saveMe").bind("click",function ()
    {
        closeMask();
        $('.commentSave').unbind('click');
        succsessFunc();
    });
}
function closeMask()
{

    $("html").css("overflow", "auto");
    $("body").css("overflow", "auto");
    $(".mask").hide();
    $('.mymodal').hide();
    $('body').unbind('keydown');
};