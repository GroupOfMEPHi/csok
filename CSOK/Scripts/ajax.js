﻿
function doAjax(controller, action, post_data, successFunc, dataType)
{
    if (dataType == undefined)
        dataType = "json";
    $.ajax({
        dataType: dataType,
        url: '/' + controller + '/' + action,
        data: post_data,
        type: "POST",
        traditional: true,
        success: function (data)
        {
            if (successFunc) successFunc(data);
            //скрываем индикатор загрузки
            //hideLoadingReport();
        },
        error: function (err)
        {
            console.log(err);

            var errText = '';
            if ('status' in err) errText += 'status: ' + err.status + '\n\n';
            if ('responseText' in err)
            {
                var d = $(document.createElement('div'));
                d.html(err.responseText);
                errText += d.find('title').html();
            }

            alert('Произошла ошибка при получении или отправке данных. ' + errText);

            //скрываем индикатор загрузки
            //hideLoadingReport();
        }
    });

}
function doAjaxHtml(controller, action, post_data, successFunc, errorFunc, resultPlaceholderSelector)
{
    var Url = '/' + controller + '/' + action;
    $.ajax({
        dataType: "html",
        url: Url,
        data: post_data,
        type: "POST",
        traditional: true,
        success: function (data, status, xhr)
        {

            if (xhr.getResponseHeader("AdditionalState") == 'Unauthorized')
                location.href = '/Account/Login';

            if (successFunc) successFunc(data);

            if (resultPlaceholderSelector != undefined)
                $(resultPlaceholderSelector).html(data);
            if (successFunc) successFunc(data);

            //скрываем индикатор загрузки
            //hideLoadingReport();
        },
        error: function (err)
        {
            if (errorFunc)
                errorFunc(err);
        }
    });

}