﻿
$(function () {
    $(".sortable").sortable();
    $(".sortable").disableSelection();
});

//function UpdateItemIdsForPost() {
//    $("#sortable1 li").each(function (idx, li) {
//        var idInputs = $(li).children(".id");
//        var nameInputs = $(li).children(".name");//if an ItemName input is used
//        $(idInputs).attr("id", "Questions_0__Blocks_" + idx + "__ID");
//        //$(idInputs).attr("id", "Items_" + idx + "__ItemId");
//        $(idInputs).attr("name", "Questions[0].Blocks[" + idx + "].ID");
//        //if an ItemName input is used
//        $(nameInputs).attr("id", "Questions_0__Blocks_" + idx + "__Body");
//        $(nameInputs).attr("name", "Questions[0].Blocks[" + idx + "].Body");
//    });
//}

$(function () {
    $(".sortable li, .sortable1 li").each(function (index, element) {
        // find the hidden input in each <li> and set it to its current index, according to the DOM
        var hiddenInput = $(element).find(".order");
        hiddenInput.val(index);
    });
    $(".sortable, .sortable1").sortable({
        stop: function (event, ui) {
            $(".sortable li, .sortable1 li").each(function (index, element) {
                // find the hidden input in each <li> and set it to its current index, according to the DOM
                var hiddenInput = $(element).find(".order");
                hiddenInput.val(index);
            });
        },
        receive: function (event, ui) {
            if ($(ui.sender).hasClass('droptrue')) {
                $(".sortable li, .sortable1 li").each(function (index, element) {
                    // find the hidden input in each <li> and set it to its current index, according to the DOM
                    var hiddenInput = $(element).find(".order");
                    hiddenInput.val(index);
                });
            }
        },
        revert: 300
    });
    $('.sortable2').sortable({
        receive: function (event, ui) {
            $('.sortable2 li').each(function (index, element) {
                // find the hidden input in each <li> and set it to its current index, according to the DOM
                var hiddenInput = $(element).find(".order");
                hiddenInput.val(index + 100500);
            });
        }
    });
});
    




$.fn.randomize = function (selector) {
    var $elems = selector ? $(this).find(selector) : $(this).children(),
        $parents = $elems.parent();

    $parents.each(function () {
        $(this).children(selector).sort(function () {
            return Math.round(Math.random()) - 0.5;
        }).remove().appendTo(this);
    });

    return this;
};

$('.sortable, .sortable1').randomize();