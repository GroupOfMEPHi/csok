﻿function add_input() {
    $('#for_block').fadeOut(400);
    $('#addinput, #delinput').removeClass('button_error');
    if ($("#delete_blocks").is(':checked')) {
        var nam = [document.getElementById('PossibleAnswers').getElementsByClassName('possible').length];
        var check = '<input type="checkbox" name="right_block" class="r_bl" value="' + nam + '">' + 'Правильный блок';
    }

    if ($("#simple_blocks").is(':checked')) {
        var nam = [document.getElementById('PossibleAnswers').getElementsByClassName('possible').length];
        var check = '';
    }


    // Создаем новый div-элемент
    var new_input = document.createElement('div');
    new_input.className = "possible"

    // Тут мы уже можем воспользоваться innerHTML потому, что изменения не коснуться всего остального докуметна
    new_input.innerHTML = '<br>Блок №' + [document.getElementById('PossibleAnswers').getElementsByClassName('possible').length + 1] + '   ' + '<input type="text" name="block" class="block" />' + check;
    // Добавляем только что созданный div на страницу
    document.getElementById('PossibleAnswers').appendChild(new_input);
    $('.r_bl').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });
}

function del_input() {
    //  удаляем последний div в нутри id=inputi
    $("#PossibleAnswers .possible").last().remove();
    //document.getElementById('PossibleAnswers').removeChild(document.getElementById('PossibleAnswers').getElementsByTagName('div')[document.getElementById('PossibleAnswers').getElementsByTagName('div').length - 1]);
}