﻿$(function () {
    $("#Timeslider").slider({
        range: "min",
        value: 15,
        min: 1,
        max: 180,
        step: 1,
        slide: function (event, ui) {
            $("#amount").val(ui.value);
        }
    });
    $("#amount").val($("#Timeslider").slider("value"));
    $("#amount").keyup(function () {
        $("#Timeslider").slider("value", $(this).val());
    });
    $("#Attempslider").slider({
        range: "min",
        value: 3,
        min: 1,
        max: 30,
        step: 1,
        slide: function (event, ui) {
            $("#attempt").val(ui.value);
        }
    });
    $("#attempt").val($("#Attempslider").slider("value"));
    $("#attempt").keyup(function () {
        $("#Attempslider").slider("value", $(this).val());
    });
});