﻿$(document).ready(function () {
    var themes = {};
    //var items2 = '<option value="" disabled>Выберите раздел</option>';
    //$('#struct_id').prepend(items2);

    $.ajax({
        type: "GET",
        url: "/Questions/GetThemes",
        contentType: "application/json; charset=utf-8",
        //data: { a: "events" },
        dataType: "json",
        success: function (data) {


            $.each(data, function (index, item) {
                if (!themes[index]) {
                    themes[index] = [];
                }
                themes[index].push(item);
                //console.log(themes);
            })

        },
        error: function () {
            alert('internal error');
        }
    });
    $('#struct_id').fancySelect().on('change.fs', function () {
        if ($(this).val() == 0) {
            $(this).next('.trigger').html("Выберите раздел");
        }
        else {
            $('#theme_id').fancySelect();

            var ul = $('#themes ul');
            if ($('#themes').is(":visible")) {

                $('#themes').effect("drop", 400);
                //var text = $('#themes').find('span').find('span').html().replace(new RegExp('^\.*\$', 'gi'), "Выберите тему вопроса");
                //text = "Выберите тему вопроса";
                //console.log( text);
                ul.empty();
            }
            var items = '<option value="">Выберите тему вопроса</option>';
            var options = {};
            $.each(themes, function (index, th) {
                if (themes[index][0].StructureId == parseInt($('#struct_id').val())) {
                    options[index] = [];
                    options[index].push(th);

                }

            });
            $.each(options, function (index, opt) {
                items += "<option value='" + opt[0][0].Id + "'>" + opt[0][0].Name + "</option>";
                ul.append("<li data-raw-value=\"" + (opt[0][0].Id) + "\">" + opt[0][0].Name + "</li>");
            });
            if (ul.text() === "") {

                ul.append("<li>В данном разделе не найдено ни одной темы!</li>");
            }

            $('#theme_id').html(items);
            $('#themes .trigger').html("Выберите тему вопроса");
            $('#themes').effect("slide", 400);
        }

    });
    $('#theme_id').fancySelect().on('change.fs', function () {
        if ($(this).val() == 0) {
            $(this).closest('select').next().next().find('li').removeClass('selected');
        }
    });
    //$('#struct_id').fancySelect();
    //$('#structures').find('span').find('span').html("Выберите раздел");
});