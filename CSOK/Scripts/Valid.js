﻿$(document).ready(function () {
    $("#editform").validate({
        ignore: ".ignored",
        invalidHandler: function (e, validator) { },
        errorPlacement: function (error, element) {
            if ($(element).attr("id") == "Body" || "Question_Body") {
                error.insertAfter($(element).closest(".jqte"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("jqte_error");
            if ($(element).attr("id") == "Body" || "Question_Body") {
                $(element).closest(".jqte").addClass("jqte_error");
            }
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("jqte_error");
            if ($(element).attr("id") == "Body" || "Question_Body") {
                $(element).closest(".jqte").removeClass("jqte_error");
            }
        },
        rules: {
            Title: "required",
            Body: "required",
            'CorrectAnswer.Body': "required",
            block: "required",
            difficulty: {
                required: true,
                min: 1, max: 5
            },
            'theme_id': 'required',
            'struct_id': 'required'
        },
        messages: {
            Title:  "Введите тему вопроса",
            //minlength: "Введите не менее, чем 2 символа."
            
            Body: "Введите вопрос",
            'CorrectAnswer.Body': "Введите правильный ответ",
            block: "Введите тело блока",
            difficulty: {
                required: "Укажите сложность вопроса",
                min: "Не меньше 1",
                max: "Не больше 5"
            },
            'theme_id':
                {
                    required: "Выберите тему вопроса"
                },
            'struct_id':
                {
                    required: "Выберите раздел для вопроса"
                }
        },

    });
    $("#Body, #Question_Body").closest(".jqte").find(".jqte_editor").keyup(function () {
        if (!$.isEmptyObject(refValid.submitted)) {
            
            refValid.element('#Body, #Question_Body');
        }
    });
    var refValid = $("#question_createform").validate({
        ignore: ".ignored",
        invalidHandler: function (e, validator) { },
        errorPlacement: function (error, element) {
            if ($(element).attr("id") == "Body" || "Question_Body") {
                error.insertAfter($(element).closest(".jqte"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("jqte_error");
            if ($(element).attr("id") == "Body" || "Question_Body") {
                $(element).closest(".jqte").addClass("jqte_error");
            }
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("jqte_error");
            if ($(element).attr("id") == "Body" || "Question_Body") {
                $(element).closest(".jqte").removeClass("jqte_error");
            }
        },
        rules: {
            'Question.Title': "required",
            Title: "required",
            Body: "required",
            'Question.Body': "required",
            'CorrectAnswer.Body': {
                required: "#qtype3:checked",
            },
            'Question.CorrectAnswer.Body': {
                required: "#qtype3:checked",
            },
            'CheckBoxAnswer': {
                required: "#qtype1:checked",
            },
            'RadioAnswer': {
        required: "#qtype1:checked",
            },
            files: {
                required: "#qtype2:checked",
                extension: "jpe?g|png|gif"
            },
            'CorrAnsw': {
                required: true,
            },
            'HasPossibleAnswer': {
                required: true,
            },
            'Question.HasPossibleAnswer': {
        required: true,
            },
            'Question.CheckBoxAnswer': {
                required: "#qtype1:checked",
            },
            'Question.RadioAnswer': {
                required: "#qtype1:checked",
            },
            'Question.MatrixAnswer': {
                required: true,
            },
            nRows: {
                required: true,
                min: 1,
                max: 5
            },
            nCols: {
                required: true,
                min: 1,
                max: 5
            },
            difficulty: {
                required: true,
                min:1, max:5
            },
            'Question.difficulty': {
                required: true,
                min: 1, max: 5
            },
            'simple_blocks':
                {
                    required: true
                },
            'HasExcessBlocks' :
                {
                    required: true
                },
            'theme_id': 'required',
            'struct_id': 'required'

            //CorrAnsw: {

            //},
            //HasPossibleAnswer: {
            //}
        },
        
        messages: {
            'Question.Title': { 
                required: "Выберите тему вопроса"
            },
            Title: {
                required: "Выберите тему вопроса"
            },
            Body: {
                required: "Введите вопрос"
            },
            "Question.Body": {
                required: "Введите вопрос"
            },
            'CorrectAnswer.Body': {
        required: "Ведите правильный ответ свободной формы ответа <br/>"
            },
            'Question.CorrectAnswer.Body': {
        required: "Ведите правильный ответ свободной формы ответа <br/>"
    },
            'CheckBoxAnswer': {
                required: "Укажите кол-во вариантов",
            },
            'RadioAnswer': {
                required: "Укажите кол-во вариантов",
            },
            'CorrAnsw': {
                required: "Выберите форму ответа",
            },
            'HasPossibleAnswer': {
                required: "Выберите форму ответа",
            },
            files: {
                required: "Прикрепите файл",
                extension: "Форматы .jpg, .png или .gif"
            },
            'Question.HasPossibleAnswer': {
        required: "Выберите форму ответа",
            },
            'Question.CheckBoxAnswer': {
                required: "Укажите кол-во вариантов",
            },
            'Question.RadioAnswer': {
                required: "Укажите кол-во вариантов",
            },
            'Question.MatrixAnswer': {
                required: "Выберите форму ответа",
            },
            nRows: {
                required: "Укажите число строк",
                min: "Число строк не должно быть меньше 1",
                max: "Число строк не должно быть больше 5"
            },
            nCols: {
                required: "Укажите число столбцов",
                min: "Число столбцов не должно быть меньше 1",
                max: "Число столбцов не должно быть больше 5"
            },
            difficulty: {
                required: "Укажите сложность вопроса",
                min: "Не меньше 1",
                max: "Не больше 5"
            },
            'Question.difficulty': {
                required: "Укажите сложность вопроса",
                min: "Не меньше 1",
                max: "Не больше 5"
            },
            'simple_blocks': {
                required: "Выберите вид блоков"
            },
            'HasExcessBlocks':
                {
                    required: "Выберите вид блоков"
                },
            'theme_id':
                {
                    required: "Выберите тему вопроса"
                },
            'struct_id':
                {
                    required: "Выберите раздел для вопроса"
                }
            //minlength: "Введите не менее, чем 2 символа."

            //Body: "Введите вопрос"
        }

    });

    $("#qtype1").change(function () {
        if ($(this).parents("#qtype11").next('.error').html() != "") {
            //$(this).parents("#qtype11").next('.error').hide();
            $(this).parents("#test_setting").find('.error').each(function (index, elem) {
                $(elem).html("");
            });
        }
    });
    $("#qtype3").change(function () {
        if ($(this).parents("#qtype31").next('.error').html() != "") {
            //$(this).parents("#qtype11").next('.error').hide();
            $(this).parents("#test_setting").find('.error').each(function (index, elem) {
                $(elem).html("");
            });
        }
    });
    $("#qtype7").change(function () {
        if ($(this).parents("#qtype71").next('.error').html() != "") {
            //$(this).parents("#qtype11").next('.error').hide();
            $(this).parents("#test_setting").find('.error').each(function (index, elem) {
                $(elem).html("");
            });
        }
    });
    $("#qtype5").change(function () {
        if ($(this).parents("#qtype51").next('.error').html() != "") {
            //$(this).parents("#qtype11").next('.error').hide();
            $(this).parents(".PossibleAnswer").find('.error').each(function (index, elem) {
                $(elem).html("");
            });
        }
    });
    $("#qtype6").change(function () {
        if ($(this).parents("#qtype61").next('.error').html() != "") {
            //$(this).parents("#qtype11").next('.error').hide();
            $(this).parents(".PossibleAnswer").find('.error').each(function (index, elem) {
                $(elem).html("");
            });
        }
    });
    $("#simple_blocks").change(function () {
        if ($(this).parents("#simple").next('.error').html() != "") {
            //$(this).parents("#qtype11").next('.error').hide();
            $(this).parents("#test_setting").find('.error').each(function (index, elem) {
                $(elem).html("");
            });
        }
    });
    $("#delete_blocks").change(function () {
        if ($(this).parents("#delete").next('.error').html() != "") {
            //$(this).parents("#qtype11").next('.error').hide();
            $(this).parents("#test_setting").find('.error').each(function (index, elem) {
                $(elem).html("");
            });
        }
    });
    $("#test_createform").validate({
        rules: {
            Subject: "required"
            //Body: "required"
        },
        messages: {
            Subject: "Укажите тему теста"
            //minlength: "Введите не менее, чем 2 символа."

            //Body: "Введите вопрос"
        }
    });
    $("#create_eve_form").validate({
        rules: {
            text: "required",
            start_date: "required",
            end_date: "required",
            choice: "required"
        },
        messages: {
            text: "Введите текст события",
            start_date: "Введите дату начала",
            end_date: "Введите дату окончания",
            choice: "Выберите тип!"
        }
    });
    $("#edit_eve_form").validate({
        rules: {
            text: "required",
            start_date: "required",
            end_date: "required",
            choice : "required"
        },
        messages: {
            text: "Введите текст события",
            start_date: "Введите дату начала",
            end_date: "Введите дату окончания",
            choice: "Выберите тип!"
        }
    });
    $("#create_news_form").validate({
        rules: {
            Title: "required",
            Content: "required",
            
        },
        messages: {
            Title: "Введите заголовок",
            Content: "Введите текст",
            
        }
    });
    jQuery.validator.addMethod(
    'regex',
    function (value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    },
    "Please check your input."
);
    $("#reg_form").validate({
        rules: {
            "Name": {
                required: true,
                minlength: 3,
                regex: /^[А-Я][а-я]+$/
            },
            "FirstName": {
                required: true,
                minlength: 3,
                regex: /^[А-Я][а-я]+$/
            },
            "LastName": {
                required: true,
                minlength: 3,
                regex: /^[А-Я][а-я]+$/
            },
            "Password": {
                required: true,
                minlength: 8,
            },
            "ConfirmPassword": {
                equalTo: "#Password"
            },
            "Email": {
                required: true,
                regex: /^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i
            },
            "Num_of_record_book": {
                required: true,
                regex: /\w{6,}/
            },
            "Num_of_sub_faculty": {
                required: true,
                regex: /\d{2,}/
            },
            "Num_of_group": {
                required: true,
                regex: /[КАИРУТФБ]\d-\d{2,}$/
            },
            "Year": {
                required: true,
                regex: /\d{4}/
            },
            "Rank": {
                required: true,
            },
            "Post": {
                required: true,
            },
            "YOB": {
                required: true,
                regex: /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/
            }
        },
        messages: {
            "Name": {
                required: "Введите Вашу фамилию",
                minlength: "Не менее 3 символов",
                regex: "Ваша фамилия должна начинаться с заглавной буквы"
            },
            "FirstName": {
                required: "Введите Ваше имя",
                minlength: "Не менее 3 символов",
                regex: "Ваше имя должно начинаться с заглавной буквы"
            },
            "LastName": {
                required: "Введите Ваше отчество",
                minlength: "Не менее 3 символов",
                regex: "Ваше отчество должно начинаться с заглавной буквы"
            },
            "Password": {
                required: "Введите пароль",
                minlength: "Не менее 8 символов",
            },
            "ConfirmPassword": {
                equalTo: "Пароли не совпадают"
            },
            "Email": {
                required: "Введите Вашу почту",
                regex: "E-mail некорректный"
            },
            "Num_of_group": {
                required: "Введите номер группы",
                regex: "Неверно введён номер группы : К7-223"
            },
            "Num_of_record_book": {
                required: "Введите номер вашей зачетки",
                regex: "Некорректный номер"
            },
            "Num_of_sub_faculty": {
                required: "Введите номер вашей кафедры",
                regex: "Некорректный номер"
            },
            "Year": {
                required: "Введите год поступления",
                regex: "Некорректный год"
            },
            "Rank": {
                required: "Введите Ваше звание",
            },
            "Post": {
                required: "Введите Вашу должность",
            },
            "YOB": {
                required: "Пожалуйста, введите дату рождения",
                regex: "Введите дату в формате дд/мм/гггг"
            }
        }
    });
    $('#form_tt').validate({
        rules: {
            'file': {
                required: true,
                extension: 'xls|xlsx|xml',
            }
        },
        messages: {
            'file': {
                required: "Прикрепите файл",
                extension: "Файл должен иметь расширение .xls, .xlsx или .xml"
            }
        }
    });
});