﻿$(document).ready(function () {
    $('input').on('ifCreated ifClicked ifChanged ifChecked ifUnchecked ifDisabled ifEnabled ifDestroyed ifToggled', function (event) {
        if (event.type === "ifClicked") {
            $(this).trigger('click');
            //$(this).iCheck('check');
        }

    }).iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue',
        increaseArea: '20%'
    });
    $("#nav").ferroMenu({
        position: "left-bottom",
        delay: 50,
        rotation: 720,
        margin: 20
    });
    $('#nav li a').tooltip({
        position: {
            my: "center bottom-10",
            at: "center top",
            using: function (position, feedback) {
                $(this).css(position);
                $("<div>")
                  .addClass("arrow")
                  .addClass(feedback.vertical)
                  .addClass(feedback.horizontal)
                  .appendTo(this);
            }
        }
    });
});