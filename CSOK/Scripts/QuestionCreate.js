﻿$(document).ready(function () {

    $("#qtype1").click(function () {
        if ($("#qtype1").is(':checked')) {
            $("#qtype3, #qtype7").addClass("ignored");
            $(".PossibleAnswer").effect("slide", "show", 400);
            $("#qtype31, #qtype71").fadeOut("slow");
        } else {

            $(".PossibleAnswer").slideToggle(400);
            $("#qtype31, #qtype71").fadeIn("slow");
            $("#qtype3, #qtype7").removeClass("ignored");
        };
    });
    $("#qtype2").click(function () {
        if ($("#qtype2").is(':checked')) {

            $(".Images").effect("slide", "show", 400);
        } else {

            $(".Images").slideToggle(400);
            $('#files').addClass("ignored");
            //var file1 = $("#files");
            
            //var emptyform = file1.wrap('<form>').parents('form').find('form');
            //console.log(emptyform);
            //$('.Images div input').val('');
            //emptyform[0].reset();
            
            //file1.unwrap();
        };
    });
    $("#qtype3").click(function () {
        if ($("#qtype3").is(':checked')) {
            $("#qtype1, #qtype7").addClass("ignored");
            $(".CorrectAnswer").effect("slide", "show", 400);
            $("#qtype11, #qtype71").fadeOut("slow");
        } else {

            $(".CorrectAnswer").slideToggle(400);
            $("#qtype11, #qtype71").fadeIn("slow");
            $("#qtype1, #qtype7").removeClass("ignored");
        };
    });
    $("#qtype5").click(function () {
        if ($("#qtype5").is(':checked')) {
            $("#qtype6").addClass("ignored");
            $("#qtype61").fadeOut("slow");
            document.getElementById("addinput").disabled = false;
            document.getElementById("delinput").disabled = false;
            $("#PossibleAnswers").fadeIn("slow");
        } else {
            $("#qtype6").removeClass("ignored");
            $("#qtype61").fadeIn("slow");
            $("#PossibleAnswers").fadeOut("slow");
            document.getElementById("addinput").disabled = true;
            document.getElementById("delinput").disabled = true;
            while (document.getElementById('PossibleAnswers').firstChild) {
                document.getElementById('PossibleAnswers').removeChild(document.getElementById('PossibleAnswers').getElementsByTagName('div')[document.getElementById('PossibleAnswers').getElementsByTagName('div').length - 1]);
            }
        };
    });
    $("#simple_blocks").click(function () {
        if ($("#simple_blocks").is(':checked')) {
            $("#delete_blocks").addClass("ignored");
            $("#delete").fadeOut("slow");
            document.getElementById("addinput").disabled = false;
            document.getElementById("delinput").disabled = false;
            $("#PossibleAnswers").fadeIn("slow");
        } else {
            $("#delete_blocks").removeClass("ignored");
            $("#delete").fadeIn("slow");
            $("#PossibleAnswers").fadeOut("slow");
            document.getElementById("addinput").disabled = true;
            document.getElementById("delinput").disabled = true;
            while (document.getElementById('PossibleAnswers').firstChild) {
                document.getElementById('PossibleAnswers').removeChild(document.getElementById('PossibleAnswers').getElementsByTagName('div')[document.getElementById('PossibleAnswers').getElementsByTagName('div').length - 1]);
            }
        };
    });
    $("#delete_blocks").click(function () {
        if ($("#delete_blocks").is(':checked')) {
            $("#simple_blocks").addClass("ignored");
            $("#simple").fadeOut("slow");
            document.getElementById("addinput").disabled = false;
            document.getElementById("delinput").disabled = false;
            $("#PossibleAnswers").fadeIn("slow");
        } else {
            $("#simple_blocks").removeClass("ignored");
            $("#simple").fadeIn("slow");
            $("#PossibleAnswers").fadeOut("slow");
            document.getElementById("addinput").disabled = true;
            document.getElementById("delinput").disabled = true;
            while (document.getElementById('PossibleAnswers').firstChild) {
                document.getElementById('PossibleAnswers').removeChild(document.getElementById('PossibleAnswers').getElementsByTagName('div')[document.getElementById('PossibleAnswers').getElementsByTagName('div').length - 1]);
            }

        };
    });

    $("#qtype6").click(function () {
        if ($("#qtype6").is(':checked')) {
            $("#qtype5").addClass("ignored");
            $("#qtype51").fadeOut("slow");
            document.getElementById("addinput").disabled = false;
            document.getElementById("delinput").disabled = false;
            $("#PossibleAnswers").fadeIn("slow");
        } else {
            $("#qtype5").removeClass("ignored");
            $("#qtype51").fadeIn("slow");
            $("#PossibleAnswers").fadeOut("slow");
            document.getElementById("addinput").disabled = true;
            document.getElementById("delinput").disabled = true;
            while (document.getElementById('PossibleAnswers').firstChild) {
                document.getElementById('PossibleAnswers').removeChild(document.getElementById('PossibleAnswers').getElementsByTagName('div')[document.getElementById('PossibleAnswers').getElementsByTagName('div').length - 1]);
            }

        };
    });
    
    $("#regtypest").click(function () {
        if ($("#regtypest").is(':checked')) {

            $(".Register_Student").show("slow");
            $("#Teacher").hide("slow");
        } else {

            $(".Register_Student").hide("slow");
            $("#Teacher").show("slow");
        };
    });
    $("#regtypepr").click(function () {
        if ($("#regtypepr").is(':checked')) {

            $(".Register_Teacher").show("slow");
            $("#Student").hide("slow");
        } else {

            $(".Register_Teacher").hide("slow");
            $("#Student").show("slow");
        };
    });


});
