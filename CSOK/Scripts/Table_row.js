﻿$(document).ready(function () {
    $(".color_table tr:not(#first_tr)").addClass("colors");

    $(".colors:last").css("background-color", "#90ee90");
    var tr_clr = $(".colors:last").css("background-color");
    $(".colors:last").css("background-color", "white");

    $(".colors").hover(function () {
        if ($(this).css("background-color") != tr_clr)
            $(this).css("background-color", "#e0f7ff")
    }, function () {
        if ($(this).css("background-color") != tr_clr)
            $(this).css("background-color", "white")
    });

    $("#check_all").click(function () {
        if ($('#check_all').is(':checked')) {
            $('.selector').iCheck('check');
            $('.selector').prop('checked', true);
            $('.colors').css("background-color", tr_clr);
            if ($('.selector').length != 0) {
                $('#delete_several').fadeIn(400);
            }
        }
        else {
            $('.selector').iCheck('uncheck');
            $('.selector').prop('checked', false);
            $('.colors').css("background-color", '#fff');
            $('#delete_several').fadeOut(400);
        }

    });

    $('.selector').on('ifChecked', function () {
        $('#delete_several').fadeIn(400);

        
        $(this).prop('checked', true);

        $(this).parent().parent().parent().css("background-color", tr_clr);
        
    });
    $('.selector').on('ifUnchecked', function () {
        $(this).prop('checked', false);
        $(this).parent().parent().parent().css("background-color", "#e0f7ff");
        var flag;
        var arr = $('.selector').toArray();
        for (var i = 0; i < arr.length; i++) {

            if (arr[i].checked == true) {
                flag = true;
                break;
            }
            if (arr[i].checked == false) {
                flag = false;
                continue;
            }
        }
        if (flag == false) {
            $('#delete_several').fadeOut(400);
        }
    });

    $(".colors").click(function () {


        if ($(this).css("background-color") != tr_clr) {
            $(this).css("background-color", tr_clr)
            var $closecheck = $(this).find('.selector');
            
            $closecheck.iCheck('check');

            $closecheck.prop('checked', true);
            $('#delete_several').fadeIn(400);
        }
        else {
            $(this).css("background-color", "#e0f7ff")
            var $closecheck = $(this).find('.selector');

            $closecheck.iCheck('uncheck');

            $closecheck.prop('checked', false);
            var flag;
            var arr = $('.selector').toArray();
            for (var i = 0; i < arr.length; i++) {
                
                if (arr[i].checked == true) {
                    flag = true;
                    break;
                }
                if (arr[i].checked == false) {
                    flag = false;
                    continue;
                }
            }
            if (flag == false) {
                $('#delete_several').fadeOut(400);
            }
        }

        if ($(this).css("background-color") = tr_clr)
            $(this).css("background-color", tr_clr)
        //var $closecheck = $(this).find('.selector');
        //$closecheck.attr('checked', true);

        $(this).css("background-color", "#e0f7ff")
        //var $closecheck = $(this).find('.selector');
        //$closecheck.attr('checked', false);


        //});
        // }
    });

    //$("#test_index table").selectable({
    //    filter: 'tbody tr',
    //    stop: function (event, ui) {
    //        $('#delete_several').fadeIn(400);
    //        $(".ui-selected", this).each(function () {
    //            if ($(this).length != 0) {
                        
    //            }
    //        });
    //    }
    //});
    

});
//$(document).ready(
//   function () {
//       //////////////////////////
//       $('#ttt input:checkbox').click(function () {
//           var $this = $(this);
//           var $tr = $this.closest('tr');
//           if ($this.attr('checked')) $tr.addClass('blue');
//           else $tr.removeClass('blue');
//       });
//   }// ready
// );