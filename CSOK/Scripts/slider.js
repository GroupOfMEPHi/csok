﻿$(function () {
    $("#slider-range-min").slider({
        range: "min",
        value: 3,
        min: 1,
        max: 5,
        step: 1,
        slide: function (event, ui) {
            $("#amount").val(ui.value);
        }
    });
    $("#amount").val($("#slider-range-min").slider("value"));
    $("#amount").keyup(function () {
        $("#slider-range-min").slider("value", $(this).val());
    });
});