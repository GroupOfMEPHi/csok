﻿function GetDate(date) {
    return new Date(date.match(/\d+/)[0] * 1);
}
function my_f() {
    //console.time('test');
    var cal = $('#calendarTable');


    $.ajax({
        type: "GET",
        url: "/Calendar/Get_Events",
        contentType: "application/json; charset=utf-8",
        //data: { a: "events" },
        dataType: "json",
        success: function (data) {
            var groups = {};
            $.each(data, function (index, item) {
                var day = GetDate(item.start_date).getDate();
                if (!groups[day]) {
                    groups[day] = [];
                }
                
                //console.log(item);
                groups[day].push({
                    id: item.id,
                    text: item.text,
                    end_date: item.end_date,
                    start_date: item.start_date,
                    type: item.type
                });
                //console.log(groups);
                //var date = GetDate(item.start_date).getDate();
                //alert(date);
                var txt = [];
                var hr = "<hr />";
                groups[day].forEach(function (value, i) {
                    var x = value.text;
                    var s_d = GetDate(value.start_date);
                    var e_d = GetDate(value.end_date);
                    var tp = value.type

                    //translate in eng for css
                    var new_type;
                    switch (value.type) {
                        case "Тест":
                            new_type = "Test";
                            break;
                        case "Экзамен":
                            new_type = "Exam";
                            break;
                        case "Зачет":
                            new_type = "Zachet";
                            break;
                        case "Проверочная работа":
                            new_type = "Checking_work";
                            break;
                        case "Лабораторная работа":
                            new_type = "Lab_work";
                            break;
                        case "Другое":
                            new_type = "Other";
                            break;
                        default:
                            new_type = "Default_type"
                    }
                    if (i == groups[day].length - 1) hr = "";
                    txt.push("<div class = " + new_type + ">" + "<div style='color:#00AAE7; font-weight:bold; text-align: center'>" + x + "</div>" +
                        "Тип:<br/>" + tp + "<br/>" +
                    "Время начала:<br/>" + s_d.getHours() + " : " + (s_d.getMinutes() < 10 ? '0' : '') + s_d.getMinutes() + "<br/>" +
                    "Время окончания:<br/>" + e_d.getHours() + " : " + (e_d.getMinutes() < 10 ? '0' : '') + e_d.getMinutes() +

                    "</div>" + hr

                    );
                });

                var div = $('<div data-dd= "' + GetDate(item.start_date).getDate() +
                    '" data-mm = "' + (GetDate(item.start_date).getMonth() + 1) +
                    '" data-yyyy= "' + GetDate(item.start_date).getFullYear() +
                    '" data-text= "' + txt.join('') +
                    '"  data-link = "/Event/Details/' + item.id + '" ></div>');
                cal.append(div);


            });
            //var res = [];
            //for (var x in groups) {
            //    if (groups.hasOwnProperty(x)) {
            //        var obj = {};
            //        obj[x] = groups[x];
            //        res.push(obj);
            //    }
            //}

        },
        error: function () {
            alert("error");
        }
    });

    //console.timeEnd('test');
}



//function calendarBig(year) {

//    for (var m = 0; m <= 11; m++) {
//        var D = new Date(year, [m], new Date().getDate()),
//            Dlast = new Date(D.getFullYear(), D.getMonth() + 1, 0).getDate(),
//            DNlast = new Date(D.getFullYear(), D.getMonth(), Dlast).getDay(),
//            DNfirst = new Date(D.getFullYear(), D.getMonth(), 1).getDay(),
//            calendar = '<tr>';
//        console.log(D);
//        if (DNfirst != 0) {
//            for (var i = 1; i < DNfirst; i++) calendar += '<td>';
//        } else {
//            for (var i = 0; i < 6; i++) calendar += '<td>';
//        }

//        for (var i = 1; i <= Dlast; i++) {
//            if (i == D.getDate() && D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth()) {
//                calendar += '<td class="today">' + i;
//            } else {
//                if (
//                    (i == 1 && D.getMonth() == 0 && ((D.getFullYear() > 1897 && D.getFullYear() < 1930) || D.getFullYear() > 1947)) ||
//                    (i == 2 && D.getMonth() == 0 && D.getFullYear() > 1992) ||
//                    ((i == 3 || i == 4 || i == 5 || i == 6 || i == 8) && D.getMonth() == 0 && D.getFullYear() > 2004) ||
//                    (i == 7 && D.getMonth() == 0 && D.getFullYear() > 1990) ||
//                    (i == 23 && D.getMonth() == 1 && D.getFullYear() > 2001) ||
//                    (i == 8 && D.getMonth() == 2 && D.getFullYear() > 1965) ||
//                    (i == 1 && D.getMonth() == 4 && D.getFullYear() > 1917) ||
//                    (i == 9 && D.getMonth() == 4 && D.getFullYear() > 1964) ||
//                    (i == 12 && D.getMonth() == 5 && D.getFullYear() > 1990) ||
//                    (i == 7 && D.getMonth() == 10 && (D.getFullYear() > 1926 && D.getFullYear() < 2005)) ||
//                    (i == 8 && D.getMonth() == 10 && (D.getFullYear() > 1926 && D.getFullYear() < 1992)) ||
//                    (i == 4 && D.getMonth() == 10 && D.getFullYear() > 2004)
//                   ) {
//                    calendar += '<td class="holiday">' + i;
//                } else {
//                    calendar += '<td>' + i;
//                }
//            }
//            if (new Date(D.getFullYear(), D.getMonth(), i).getDay() == 0) {
//                calendar += '<tr>';
//            }
//        }

//        if (DNlast != 0) {
//            for (var i = DNlast; i < 7; i++) calendar += '<td>';
//        }

//        document.querySelector('#calendarBig table[data-m="' + [m] + '"] tbody').innerHTML = calendar;
//        document.querySelector('#calendarBig > thead td:nth-child(2)').innerHTML = 'Календарь на ' + year + ' год';
//        document.querySelector('#calendarBig > thead td:nth-child(1)').innerHTML = 'Календарь на ' + parseFloat(parseFloat(year) - 1) + ' год';
//        document.querySelector('#calendarBig > thead td:nth-child(3)').innerHTML = 'Календарь на ' + parseFloat(parseFloat(year) + 1) + ' год';
//        //console.time('test2');
//        setTimeout(function () {
//            //console.timeEnd('test2');
//            // абзац создаёт сообщения
//            for (var k = 1; k <= document.querySelectorAll('#calendarTable div').length; k++) {
//                var myD = document.querySelectorAll('#calendarBig table td'),
//                    my = document.querySelector('#calendarTable div:nth-child(' + [k] + ')');
//                for (var i = 0; i < myD.length; i++) {
//                    if (my.dataset.yyyy) {
//                        if (myD[i].innerHTML == my.dataset.dd && myD[i].parentNode.parentNode.parentNode.dataset.m == (my.dataset.mm - 1) && year == my.dataset.yyyy) {
//                            myD[i].title = my.dataset.text;
//                            //if (my.dataset.link) {

//                            //    //myD[i].innerHTML = '<a class ="show_popup" data-ajax="true" data-ajax-method="GET" data-ajax-mode="replace" data-ajax-update="#details" href="' + my.dataset.link + '" rel="tabs_info"   >' + myD[i].innerHTML + '</a>';
//                            //}
//                        }
//                    } else {
//                        if (myD[i].innerHTML == my.dataset.dd && myD[i].parentNode.parentNode.parentNode.dataset.m == (my.dataset.mm - 1)) {
//                            myD[i].title = my.dataset.text;
//                            //if (my.dataset.link) {
//                            //   // myD[i].innerHTML = '<a class ="show_popup" data-ajax="true" data-ajax-method="GET" data-ajax-mode="replace" data-ajax-update="#details" href="' + my.dataset.link + '" rel="tabs_info"   >' + myD[i].innerHTML + '</a>';
//                            //}
//                        }
//                    }
//                }
//            }
//        }, 300);

//    }

//}


//my_f();
//calendarBig(new Date().getFullYear());

//document.querySelector('#calendarBig > thead td:nth-child(1)').onclick = calendarBigG;
//document.querySelector('#calendarBig > thead td:nth-child(3)').onclick = calendarBigG;

//function calendarBigG() { calendarBig(this.innerHTML.replace(/[^\d]/gi, '')); }

$(document).ready(function () {

    $('#calendarBig tr td').tooltip({
        close: function (event, ui) {
            ui.tooltip.hover(function () {
                $(this).stop(true).fadeTo("slow", 1);
            },
            function () {
                $(this).effect("drop", function () {
                    $(this).remove();
                });
            });
        },
        content: function () {
            return $(this).attr('title')
        },
        show:
            {
                effect: "slide",
            },
        hide:
            {
                effect: "drop",
                delay: 250
            },
        tooltipClass: "overflow"
    });
});



for (var m = 0 ; m < 12; m++) {
    var cal = new Calendar(m, new Date().getFullYear());
    cal.generateHTML();

    var td = document.createElement("td");
    var tr = document.createElement("tr");
    //tr.appendChild(td);
    if (m == 3 || m == 6 || m == 9) {
        document.getElementById("calendarBig").getElementsByTagName("tbody")[0].appendChild(tr).appendChild(td).innerHTML += cal.getHTML();
    }
    else {
        document.getElementById("calendarBig").getElementsByTagName("tbody")[0].lastElementChild.appendChild(td).innerHTML += cal.getHTML();
    }
}
my_f();
setTimeout(function () {
    var year = new Date().getFullYear();
    //console.timeEnd('test2');
    // абзац создаёт сообщения
    for (var k = 1; k <= document.querySelectorAll('#calendarTable div').length; k++) {
        var myD = document.querySelectorAll('#calendarBig table td'),
            my = document.querySelector('#calendarTable div:nth-child(' + [k] + ')');
        for (var i = 0; i < myD.length; i++) {
            if (my.dataset.yyyy) {
                if (myD[i].innerHTML == my.dataset.dd && myD[i].parentNode.parentNode.parentNode.dataset.m == (my.dataset.mm - 1) && year == my.dataset.yyyy) {
                    myD[i].title = my.dataset.text;
                    //if (my.dataset.link) {

                    //    //myD[i].innerHTML = '<a class ="show_popup" data-ajax="true" data-ajax-method="GET" data-ajax-mode="replace" data-ajax-update="#details" href="' + my.dataset.link + '" rel="tabs_info"   >' + myD[i].innerHTML + '</a>';
                    //}
                }
            } else {
                if (myD[i].innerHTML == my.dataset.dd && myD[i].parentNode.parentNode.parentNode.dataset.m == (my.dataset.mm - 1)) {
                    myD[i].title = my.dataset.text;
                    //if (my.dataset.link) {
                    //   // myD[i].innerHTML = '<a class ="show_popup" data-ajax="true" data-ajax-method="GET" data-ajax-mode="replace" data-ajax-update="#details" href="' + my.dataset.link + '" rel="tabs_info"   >' + myD[i].innerHTML + '</a>';
                    //}
                }
            }
        }
    }
}, 300);