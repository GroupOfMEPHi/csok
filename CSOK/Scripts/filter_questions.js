﻿function removeDups(array) {
    var index = {};
    // traverse array from end to start so removing the current item from the array
    // doesn't mess up the traversal
    for (var i = array.length - 1; i >= 0; i--) {
        if (array[i] in index) {
            // remove this item
            array.splice(i, 1);
        } else {
            // add this value index
            index[array[i]] = true;
        }
    }
    return array;
}

var $rows = $('#ttt tbody tr');
$('#search').keyup(function () {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

    $rows.show().filter(function () {
        var text = $(this).find('.title').text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
});
$(function () {
    $('#random_test').fadeIn(400);

    var availableTags = [];
    $('.title').each(function (index, title) {
        availableTags.push($(title).html());
    });
    $("#search").autocomplete({
        source: removeDups(availableTags),
        select: function (event, ui) {
            var origEvent = event;
            while (origEvent.originalEvent !== undefined) {
                origEvent = origEvent.originalEvent;
            }
            //console.info('Event type = ' + origEvent.type);
            //console.info('ui.item.value = ' + ui.item.value);
            if (origEvent.type == 'click' || origEvent.type == 'keydown') {
                

            }
        }
    });
});

//test_random creating

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function checkRandom(r, nodeList) {
    for (var i = 0; i < r.length; i++) {
        var c = nodeList.get(r[i]);
        c.checked = true;
        $(c).parent('.icheckbox_flat-blue').addClass('checked');
        $(c).parents('.colors').css('background-color', '#90ee90');
    }
}
function getRandomArrayElements(arr, count) {
    var randoms = [], clone = arr.slice(0);
    for (var i = 0, index; i < count; ++i) {
        index = Math.floor(Math.random() * clone.length);
        randoms.push(clone[index]);
        clone[index] = clone.pop();
    }
    return randoms;
}
$('#random_test').click(function () {
    if ($('#count_of_random').val() != '') {
        var count = $('#count_of_random').val();
        $('#delete_several').fadeIn(400);
        var chks = $('#ttt tbody input[type=checkbox]').not(':hidden');
        $.each(chks, function (index, elem) {
            $(elem).prop('checked', false);
            $(elem).parent('.icheckbox_flat-blue').removeClass('checked');
            $(elem).parents('.colors').css('background-color', '#fff');


        });
        var array = [];
        for (var i = 0; i < chks.length; i++) {
            array.push(i);
        }
        var r = getRandomArrayElements(array, count);
        checkRandom(r, chks);
        //$.each(chks, function (index, elem) {
        //    console.log($(elem).prop('checked'));
        //});
    }
});