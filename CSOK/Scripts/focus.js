﻿$(document).ready(function () {
    $("input[type=text], input[type=password]").focus(function () {
        $(this).addClass("focused");
    });
    $("input[type=text], input[type=password]").blur(function () {
        $(this).removeClass("focused");
    });
});