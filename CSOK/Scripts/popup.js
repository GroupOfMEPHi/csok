﻿$(function () {
    //script for popups
    $('a.show_popup').click(function (event) {
        event.preventDefault();

        $('html, body').animate({ scrollTop: 0 }, 300);
        $('div.' + $(this).attr("rel")).fadeIn(500);
        $("body").append("<div id='overlay'></div>");
        $('#overlay').show().css({ 'filter': 'alpha(opacity=80)' });
        return false;
    });

    /*$('a.close').click(function () {
        $(this).parent().fadeOut(100);
        $('#overlay').remove('#overlay');
        return false;
    });*/

    //script for tabs
//    jQuery.ajaxSetup({
//        beforeSend: function () {
//            $('.spinner').show();
//        },
//        complete: function () {
//            $('.spinner').hide();
//        }
//    });
});
$('.show_popup').click(function (index) {

    $.ajax({
        url: this.href,
        contentType: 'application/html; charset=utf-8',
        type: 'GET',
        success: function (result) {

            $('#details').html(result);
        }
    });
    return false;
});




