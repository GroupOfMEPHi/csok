﻿function fadeIn(el) {
    el.style.opacity = 0;

    var last = +new Date();
    var tick = function () {
        el.style.opacity = +el.style.opacity + (new Date() - last) / 400;
        last = +new Date();

        if (+el.style.opacity < 1) {
            (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16)
        }
    };

    tick();
}
function addRow(id) {
    $("#deny_button").fadeIn(400);
    var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR");
    row.className = "new";
    var td1 = document.createElement("TD");
    $(td1).html('<input type="text" name="column1" />');
    var td2 = document.createElement("TD")
    $(td2).html('<input type="text" name="column2" />');
    var td3 = document.createElement("TD")
    $(td3).html('<input type="text" name="column3" />');
    var td4 = document.createElement("TD")
    $(td4).html('<input type="text" name="column4" />');
    var td5 = document.createElement("TD")
    $(td5).html('<input type="text" name="column5" />');
    var td6 = document.createElement("TD")
    $(td6).html('<input type="text" name="column6" />');
    var td7 = document.createElement("TD")
    $(td7).html('<input type="text" name="column7" />');
    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
    row.appendChild(td4);
    row.appendChild(td5);
    row.appendChild(td6);
    row.appendChild(td7);
    tbody.appendChild(row);
    fadeIn(row);
}
$(document).ready(function () {
    $("#deny_button").click(function () {
        $(".timetable tbody tr.new").last().remove();
        if ($(".timetable tbody tr.new").length == 0) {
            $(this).fadeOut(400);
            }
    });
});