﻿var whereWe = "inbox";
var start = 0;
var count = 10;
var fullCount = 0;
var sortOrder = 2;
function addAttachment()
{
    $("#attachmentList").append('<div class="file-block"><input type="file"  class="btn btn-primary" name="AttachmentUrls"/>' +
        '<button type="button" class="btn btn-primary" onclick="$(this).parent().remove();">Удалить вложение</button><br/></div>');
}
function SendMail()
{
    var data = {};
    $($("#MailTo").val().split(";")).each(function (index, value)
    {
        data["To[" + index + "]"] = value;
    });
    data["Subject"] = $("#MailSubject").val();
    data["Text"] = $("#MailText").val();
    doAjaxHtml("Message", "SendMessage", data, function () { location.href = "/Message/SendSucceful"; },
        function () { showError(); }, $("#result"));
}
function checkValidNames()
{
    var data = {};
    data["names"] = $("#MailTo").val();
    doAjax("Message", "CheckValidName", data, function (dat)
    {
        console.log(dat["badNames"].length);
        if (dat["badNames"].length != 0)
        {
            $("#MailTo").parent().css('border-color', '#990000');
        }
    });
}
function submitForm()
{
    var data = {};
    data["names"] = $("#MailTo").val();
    doAjax("Message", "CheckValidName", data, function (dat)
    {
        if (dat["badNames"].length != 0)
        {
            $("#MailTo").parent().css('border-color', '#990000');
        }
        else
        {
            $("#sendMessageForm").submit();
        }
    });
}
function removeMessage(id)
{
    var data = {};
    data["id"] = id;
    doAjax("message", "delete", data, null);

    $("#message-" + id).remove();
}

function restoreMessage(id)
{
    var data = {};
    data["id"] = id;
    doAjax("message", "restore", data, null);

    $("#message-" + id).remove();
}
function doAjax(controller, action, post_data, successFunc, dataType)
{
    if (dataType == undefined)
        dataType = "json";
    $.ajax({
        dataType: dataType,
        url: '/' + controller + '/' + action,
        data: post_data,
        type: "POST",
        traditional: true,
        success: function (data)
        {
            if (successFunc) successFunc(data);
            //скрываем индикатор загрузки
            //hideLoadingReport();
        },
        error: function (err)
        {
            console.log(err);

            var errText = '';
            if ('status' in err) errText += 'status: ' + err.status + '\n\n';
            if ('responseText' in err)
            {
                var d = $(document.createElement('div'));
                d.html(err.responseText);
                errText += d.find('title').html();
            }

            alert('Произошла ошибка при получении или отправке данных. ' + errText);

            //скрываем индикатор загрузки
            //hideLoadingReport();
        }
    });

}
function doAjaxHtml(controller, action, post_data, successFunc, errorFunc, resultPlaceholderSelector)
{
    var Url = '/' + controller + '/' + action;
    $.ajax({
        dataType: "html",
        url: Url,
        data: post_data,
        type: "POST",
        traditional: true,
        success: function (data, status, xhr)
        {

            if (xhr.getResponseHeader("AdditionalState") == 'Unauthorized')
                location.href = '/Account/Login';

            if (successFunc) successFunc(data);

            if (resultPlaceholderSelector != undefined)
                $(resultPlaceholderSelector).html(data);
            if (successFunc) successFunc(data);

            //скрываем индикатор загрузки
            //hideLoadingReport();
        },
        error: function (err)
        {
            if (errorFunc)
                errorFunc(err);
        }
    });

}
//Хитрая штука с активной блокировкой не получилась...
function listSearch()
{
    tryAjaxGetList(function (html)
    {
        $(".message-list").html(html);
    });
}
function doMore()
{
    start += 10;
    tryAjaxGetList(function (html)
    {
        if (html.indexOf("div") == -1)
            $("#more").remove();
        $(".message-list").append(html);
    });
}
function tryAjaxGetList(succFunc)
{
    var data = {};
    data["searchString"] = $("#searchInput").val();
    data["start"] = start;
    data["count"] = count;
    data["type"] = whereWe;
    $("#sortLink").text(sortOrder==2 ? "Сначала ранние" : "Сначала поздние");
    data["sortOrder"] = sortOrder;
    doAjax("Message", "MessageListPartial", data, function (postData)
    {
        succFunc(postData);
    }, "html");
}