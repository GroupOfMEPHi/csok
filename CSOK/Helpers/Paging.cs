﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Text;
using System.Web.Mvc.Ajax;

namespace CSOK.Helpers
{
    public static class Paging
    {
        public static MvcHtmlString PagingNavigator(this HtmlHelper helper, int pageNum, int itemCount, int pageSize)
        {
            StringBuilder sb = new StringBuilder();
            if (pageNum > 0)
            {

                sb.Append(helper.ActionLink("<", "QuestionTest", new { pageNum = pageNum - 1 }));
            }
            else
            {
                sb.Append(HttpUtility.HtmlEncode("<"));
            }
            sb.Append(" ");

            int pagesCount = (int)Math.Ceiling( (double)itemCount / pageSize);
            if (pageNum < pagesCount - 1)
            {
                sb.Append(helper.ActionLink("<", "QuestionTest", new { pageNum = pageNum + 1 }));
            }
            else
            {
                sb.Append(HttpUtility.HtmlEncode(">"));
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString AjaxPagingNavigator(this AjaxHelper helper, int pageNum, int itemCount, int pageSize)
        {
            StringBuilder sb = new StringBuilder();
            AjaxOptions ao = new AjaxOptions();
            ao.UpdateTargetId = "QuestionDiv";
            if (pageNum > 0)
            {

                sb.Append(helper.ActionLink("<", "QuestionTest", new { pageNum = pageNum - 1 },ao));
            }
            else
            {
                sb.Append(HttpUtility.HtmlEncode("<"));
            }
            sb.Append(" ");

            int pagesCount = (int)Math.Ceiling((double)itemCount / pageSize);
            if (pageNum < pagesCount - 1)
            {
                sb.Append(helper.ActionLink("<", "QuestionTest", new { pageNum = pageNum + 1 }, ao));
            }
            else
            {
                sb.Append(HttpUtility.HtmlEncode(">"));
            }

            return MvcHtmlString.Create(sb.ToString());
        }
    }
}